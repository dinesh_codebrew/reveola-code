<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use Config;

class SetLocale {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (isset($request->lang) && !empty($request->lang)) {   
            $locale = $request->lang;
            if ($locale != 'ar' && $locale != 'en') {
                $locale = 'en';
            } 
            /*else if (Session::has('locale')) {
                $locale = Session::get('locale', Config::get('app.locale'));
            }*/
            App::setLocale($locale);
        } 
        return $next($request);
    }

}
