<?php

namespace App\Http\Middleware;

use Closure;

class verifyToken {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (isset($request->access_email) && !empty($request->access_email)) {
          $user=  \App\Models\User::where('email', $request->access_email)->first(); 
            if(isset($user->id) && !empty($user->id)) {
               $request->attributes->add(['user_id' => $user->id]);
            } else {
              $request->attributes->add(['user_id' => 0]);
            }
        }else
             $request->attributes->add(['user_id' => 0]);
        return $next($request);
    }

}
