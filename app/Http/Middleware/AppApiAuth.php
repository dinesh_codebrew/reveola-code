<?php

namespace App\Http\Middleware;

use Closure;

class AppApiAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $me = auth()->user();

        if ($me->status == 'block')
            return Response(array('success' => -2, 'statuscode' => 401, 'msg' => 'Sorry, your account is blocked by admin'), 401);

      /*  $time = new \DateTime('now', new \DateTimeZone(isset($me->timezone) ? $me->timezone : $me->timezone ));
        $me->timezonez = $time->format('P');*/

        $request['user_id'] = $me->id;
        return $next($request);
    }

}
