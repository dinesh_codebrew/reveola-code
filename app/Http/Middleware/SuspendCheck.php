<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class SuspendCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (optional(Auth::user())->suspended) {
            abort(500);
        };
        return $next($request);
    }
}
