<?php

namespace App\Http\Middleware;

use Closure;

use Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use View;
use DB;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Route;
use App\Models\User;

class BusinessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentPath = Route::currentRouteName(); 
        if (!in_array($currentPath, ['business_login_get', 'business_login_post'])) {

            if (!Request::cookie('businessid'))
                return Redirect::route('business_login_get')->withErrors('Please Login First');

            $admin = User::where('id', Request::cookie('businessid'))
                    ->select('*', DB::RAW("(CONCAT('" . env('IMAGE_URL') . "',profile_pic)) "
                            . "AS pic_url")); 
            
            
            $admin->with(['orders' => function($query) { 
                    $query->whereDate('end_date','>',date('Y-m-d')); 
                }]);
            $admin = $admin->first(); 
            if (!$admin)
                return Redirect::route('business_login_get')->withErrors('Please Login First');
            if (!in_array($currentPath, ['business_subscriptions','business_logout','business_payment_get','business_payment_post','business_subscriptions_post'])) {
             if(count($admin->orders)<1)
                return Redirect::route('business_subscriptions');
                //->withErrors('Please choose one subscription plan.');
            }
            $time = new \DateTime('now', new \DateTimeZone($admin->timezone));
            $request['timezonez'] = $time->format('P');

            \Cookie::queue('adminid', $admin->id, 60);
            \Cookie::queue('admintz', $request['timezonez'], 60);

            \App::instance('admin', $admin);

            $request['timezone'] = $admin->timezone; 
            View::share('admin', $admin);
        }
        return $next($request);
    }
}
