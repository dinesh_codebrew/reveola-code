<?php

namespace App\Http\Controllers\SocialAuth;

use App\Http\Controllers\Auth\LoginController;
use App\Services\SocialLinkedinAccountService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class LinkedinController extends Controller
{
        /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('linkedin')->redirect();
    }
    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(SocialGoogleAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('linkedin')->user());
        auth()->login($user, true);
        return redirect()->to('/home');
    }
}
