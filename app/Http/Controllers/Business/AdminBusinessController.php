<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use Illuminate\Validation\Rule;
use View;
use Redirect;
use Auth;
use Illuminate\Support\Facades\Input;
use DB;
use Mail;
use Hash;
use App\Mail\sharecredentials;
use App\Http\Controllers\CommonController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Service;
use App\Models\Business;
use App\Models\Gallery;
use App\Models\User;

class AdminBusinessController extends Controller {
 
    /////////////			All Business     ///////////////////////////////////// 
    public static function business_business_all(Request $request) {           
        try {
            $admin = \App('admin');
            $businesses = Business::where('user_id',$admin->id)->orderBy('id')->paginate(10);
            return View::make('Business.Business.allbusinesses')->with('businesses', $businesses);
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function business_business_add_get(Request $request) {
        try {
            $maincategories = Category::where('status', 'active')->get(); //
            return View::make('Business.Business.addbusiness')->with(compact('maincategories', 'users'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    ///////////////////////// 		Add Category Post     ///////// 
    public static function business_business_add_post(Request $request) {
        try {
            $rules = array(
                'b_name' => 'required|max:150',
                'b_pic' => 'image',
                'b_address' => 'required',
                'b_lat' => 'required',
                'b_lng' => 'required',
                'cat_id' => 'required'
            );
            $msg = array(
                'b_name.required' => 'Business name is required.',
                'b_address.required' => 'Business address is required.',
                'b_lat.required' => 'Please choose a valid address.',
                'b_lng.required' => 'Please choose a valid address.',
                'b_pic.image' => 'Business image should be a valid image.',
                'cat_id.required' => 'You have to choose one category.'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            if (!empty($request->b_pic)) {
                $filename = CommonController::image_uploader(Input::file('b_pic'));

                if (empty($filename))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");

                $data['b_pic'] = $filename;
            }

            /*             * If features added */
            if (count($request->features) > 0) {
                foreach ($request->features as $key => $val) {
                    foreach ($val as $key2 => $val2) {
                        if (!empty($val2))
                            $featuredata[$key2] = $val2;
                    }
                }
                $data['features'] = json_encode($featuredata);
            }
            $data['user_id']=Auth::user()->id;
            $data['status']='block';
            /** features added end */
            $business_id = Business::add_new_business($data); 
            
            return Redirect::route('business_business_edit',$business_id)->with('status', 'Business added successfully.Admin will notify soon after active your business.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function business_business_edit(Request $request, $id = null) {
        try {
            $maincategories = Category::where('status', 'active')->get(); //
            $business = Business::where('user_id',Auth::user()->id)->findorfail($id);
            //->where('status','active')
//          $check_newfeature=  DB::table('cat_features')->where('cat_id', $service->cat_id)->get();
              
            return View::make('Business.Business.addbusiness')->with(compact('business', 'maincategories'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function business_business_update(Request $request) {
        try {
            $rules = array(
                'b_name' => 'required|max:150',
                'b_pic' => 'image',
                'b_address' => 'required',
                'b_lat' => 'required',
                'b_lng' => 'required',
                'cat_id' => 'required'
            );
            $msg = array(
                'b_name.required' => 'Business name is required.',
                'b_address.required' => 'Business address is required.',
                'b_lat.required' => 'Please choose a valid address.',
                'b_lng.required' => 'Please choose a valid address.',
                'b_pic.image' => 'Business image should be a valid image.',
                'cat_id.required' => 'You have to choose one category.'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            $business = Business::where('user_id',Auth::user()->id)->findorfail($request->id);
            //->where('status','active')
            if (!empty($request['b_pic'])) {
                $data['b_pic'] = CommonController::image_uploader(Input::file('b_pic'));
                if (empty($request['b_pic']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            } else
                $data['b_pic'] = $business->b_pic;
            $data['status'] = $business->status;
            
            /*             * If features added */
            /*  if (count($request->features) > 0) {
              foreach ($request->features as $key=>$val) {
              foreach ($val as $key2=>$val2) {
              if (!empty($val2))
              $featuredata[$key2] = $val2;
              }
              }
              if (isset($featuredata) && count($featuredata) > 0)
              $data['features'] = json_encode($featuredata);
              else
              $data['features'] = '';
              } */
            /** features added end */
            $request['id'] = Business::update_business($data);
            return Redirect::back()->with('status', 'Business updated successfully.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

////////////////////Service Block Unblock 	////////////////////////////////////////////////////
    public static function business_business_block(Request $request, $id = null, $status = null) {
        try {
            $business = Business::findorfail($id);

            if ($status == 1) {

                if ($business->status == 'block')
                    return Redirect::back()->withErrors('This Business is already Blocked')->withInput();

                DB::Table('businesses')->where('id', $business->id)->update(array(
                    'status' => 'block',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Business Blocked Successfully');
            }
            else {
                if ($business->status == 'active')
                    return Redirect::back()->withErrors('This Business is already Unblocked')->withInput();

                DB::Table('businesses')->where('id', $business->id)->update(array(
                    'status' => 'active',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Business Unblocked Successfully');
            }
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function business_business_delete(Request $request, $id = null) {
        try {
            $admin = \App('admin'); 
            $business = Business::where('id',$id)->where('user_id',$admin->id)->first();
            if($business->user_id==Auth::user()->id)    
                $business->delete();
            else
             return Redirect::back()->witherrors('Sorry, you can\'t delete this business.');
        return Redirect::back()->with('status', 'Business Successfully Deleted.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function business_business_addGallery_post(Request $request, $id = null) {
        try {
            $rules = array(
                'name' => 'required|image',
                'business_id' => 'required'
            );
            $msg = array(
                'name.required' => 'Gallery Image is required.',
                'name.image' => 'Gallery Image should be a valid image.',
                'business_id.required' => 'Something wrong, please retry after page refresh!'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
        $business = Business::where('user_id',Auth::user()->id)->findorfail($request->business_id);
        //->where('status','active')
            if (!empty($request['name'])) {
                $data['name'] = CommonController::image_uploader(Input::file('name'));
                if (empty($request['name']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            }
            $request['id'] = Gallery::add_new_gallery_image($data);
            return Redirect::back()->with('status', 'Image added successfully.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function business_business_addGallery_get(Request $request, $id = null) {
        try {
            $business   =   Business::where('user_id',Auth::user()->id)->findorfail($id);
            //->where('status','active')
            $gallery    =   Gallery::where('business_id', $business->id)->paginate(5);
            return View::make('Business.Business.addgallery')->with(compact('gallery', 'business'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
     
    public static function business_bgallery_delete(Request $request, $id = null) {
        try {
            Business::where('user_id',Auth::user()->id)->first();
            $gallery = Gallery::findorfail($id);
            $gallery->delete();

            return Redirect::back()->with('status', 'Image Successfully Deleted.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
    public static function business_bgallery_block(Request $request, $id = null, $status = null) {
        try {
            Business::where('user_id',Auth::user()->id)->first();
            $gallery = Gallery::findorfail($id);

            if ($status == 1) {
                if ($gallery->status == 'block')
                return Redirect::back()->withErrors('This image is already Blocked')->withInput();
            DB::Table('gallery')->where('id', $gallery->id)->update(array(
                    'status' => 'block',
                    'updated_at' => new \DateTime
                ));
            return Redirect::back()->with('status', 'Image Blocked Successfully');
            }
            else {
                if ($gallery->status == 'active')
                return Redirect::back()->withErrors('This Image is already Unblocked')->withInput();
            DB::Table('gallery')->where('id', $gallery->id)->update(array(
                    'status' => 'active',
                    'updated_at' => new \DateTime
                ));

            return Redirect::back()->with('status', 'Image Unblocked Successfully');
            }
        } catch (\Exception $e) {
             return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }


}
