<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
//use Mail;
use Auth;
use View;
use Cookie;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Controllers\CommonController;
use Auth\AdminForgotPasswordController;
use Auth\AdminResetPasswordController; 
use App\Models\User; 
use App\Models\Subscription;
use App\Models\Order;
use App\Models\Product;
use App\Models\Service;


class BusinessController extends Controller
{
    public static function business_subscriptions_post(Request $request){
        try {
            
            $rules = array( 
                'id' => 'required'
            );
            
            $msg = array( 
                'id.required' => 'Please choose one plan.'
            );            
            $validator = Validator::make($request->all(), $rules,$msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())
                ->withInput();
            $data =Subscription::findorfail($request->id);  
            $orderdata['user_id']   =   Auth::user()->id;
            $orderdata['sub_id']    =   $data->id;
            $orderdata['sub_name']  =   $data->sub_name;
            $orderdata['sub_price'] =   $data->sub_price;
            $orderdata['validity']  =   $data->validity;
            $startTime              =   date("Y-m-d H:i:s");
            $orderdata['start_date']= $startTime;
            $orderdata['end_date']  = date('Y-m-d H:i:s',strtotime('+'.$data->validity.' days',strtotime($startTime)));
            /*(new DateTime('2014-07-10'))->modify('+1 day')->format('Y-m-d');*/
            $previous_order =   Order::where('sub_id',  $orderdata['sub_id'])->where('user_id',$orderdata['user_id'])->first();
            if(isset($previous_order->id) && !empty($previous_order->id))
                return Redirect::back()->witherrors('You already subscribed this plan.');
            $order   = Order::createOrder($orderdata);
                return Redirect::route('business_business_all')->with('status', 'Congratulations, you successfully subscribe the plan.'); 
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    public static function business_subscriptions(){
        $subscriptions   =   Subscription::where('status','active')->get();
         return View::make('Business.Subscribe.index')->with(compact('subscriptions'));
    }
    

    public static function business_payment_get() {        
     return View::make('Business.Subscribe.pay');
    }
    public static function business_payment_post(Request $request) {
       /*   [stripeToken] => tok_1Ccp6VFkVNOlLMbCxld17rTe
            [stripeTokenType] => card
            [stripeEmail] => test1@gmail.com
        */
        if(isset($request->stripeToken) && !empty($request->stripeToken)){
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
            // Token is created using Checkout or Elements!
            // Get the payment token ID submitted by the form:
            $token  =    $request->stripeToken;
            try{ 
            $data   = Subscription::findOrFail($request->id);
            $orderdata['user_id']   =   Auth::user()->id;
            $orderdata['sub_id']    =   $data->id;
            $orderdata['sub_name']  =   $data->sub_name;
            $orderdata['sub_price'] =   $data->sub_price;
            $orderdata['validity']  =   $data->validity;
            $startTime              =   date("Y-m-d H:i:s");
            $orderdata['start_date']= $startTime;
            $orderdata['end_date']  = date('Y-m-d H:i:s',strtotime('+'.$data->validity.' days',strtotime($startTime)));
           
            /******************////////// Payment Start ///////////*******************/
            $charge = \Stripe\Charge::create([
                'amount' => $data->sub_price*100,
                'currency' => 'usd',
                'description' => $data->sub_name,
                'source' => $token,
            ]);
              if($charge->status == "succeeded"){ 
                $orderdata['stripe_id']             =   $charge->id;
                $orderdata['balance_transaction']   =   $charge->balance_transaction;
                $orderdata['transaction_created']   =   $charge->created;
                
                $order   = Order::createOrder($orderdata);
               // return Redirect::back()->with('status', 'Congratulations, you successfully subscribe the '.$data->sub_name.' plan.'); 
                 return Redirect::route('business_business_all')->with('status', 'Congratulations, you successfully subscribe the '.$data->sub_name.' plan.');
                }else{
                    return Redirect::back()->witherrors('There are some issue with your card , please contact with site administrator.');
                }
            } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage());
        }
        }else{
            return Redirect::back()->witherrors('There are some error , please try after soemtime');
        }
    }
    public static function business_login_get() {
        $bdetails = array();
        if (Cookie::get('b_email') !== null) {
            $bdetails['b_email'] = Cookie::get('b_email');
            $bdetails['b_psw'] = Cookie::get('b_psw');
        }
        return View::make('Business.Other.login')->with(compact('bdetails'));
    }
    
    public static function business_login_post(Request $request) {

        $remember = $request->get('remember');
        if (!$remember) { 
            Cookie::queue(\Cookie::forget('b_email'));
        }
        try{ 
         if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'user_type' => 'business'])) { 
            
             \Cookie::queue('businessid', Auth::user()->id, 60);

         if ($remember) {
                Cookie::queue('b_email', $request['email'], time() + (10 * 365 * 24 * 60 * 60));
                Cookie::queue('b_psw', $request['password'], time() + (10 * 365 * 24 * 60 * 60));
            }
       // return Redirect::route('business_dashboard')->with('successMsg', 'Logged In Successfully');
        return Redirect::route('business_business_all')->with('successMsg', 'Logged In Successfully');
         }else{
             return Redirect::back()->witherrors('Wrong username / password!')->withInput();
         }
        }catch (\Exception $e) {
             return Redirect::back()->witherrors($e->getMessage())->withInput();
        } 
    }

    ///////////////         Logout 			////////////////////////////// 
    public static function business_logout() {
        try {
            Auth::logout();
            \Cookie::queue('businessid', 0, 0);
            return Redirect::route('business_login_get')->with('fresh', '1')->with('status', 'Logged Out Successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

///////////////////////////////////			Admin Dash Board 			////////////////////////////////////////

    public static function business_dashboard(Request $request) {
        $starting_dt    = Carbon::now(\Session::get('timezonez'))->subMonths(2)->format('d/m/Y');
        $ending_dt      = Carbon::now(\Session::get('timezonez'))->addday(1)->format('d/m/Y');
        /* $request['starting_dt'] = $request['starting_dt'] . ' 00:00:00';
          $request['ending_dt'] = $request['ending_dt'] . ' 23:59:59'; */
        $data['starting_dt'] = '1970-01-01 00:00:00';
        $data['ending_dt'] = date('Y-m-d h:i:s', time());
         
        return View::make('Business.dashboard.index')->with(compact('starting_dt', 'ending_dt'));
    }
    
    
    
//////////////////		Admin Profile Update Get ////////////////////////////////////////////////////////////

    public static function bprofile_update_get() { 
        try {
            
            $user   = User::findorfail(Auth::user()->id);  
            return View::make('Business.Other.profileupdate')->with(compact('user'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

//////////////		Admin Profile Update Post ////////////////////////////////////////////////////////////

    public static function bprofile_update_post(Request $request) {
        try { 
            $admin = \App('admin');
            $rules = array( 
                'name'          =>  'required|max:150',    
                'phone'         =>  'bail|nullable|numeric|unique:users,phone,' . $admin->id . ',id',  
                'profile_pic'   =>  'image',
                'latitude'      =>  'required',
                'longitude'     =>  'required',
                'address'       =>  'required'
            );
            $msg    =   array(
                'address.required'          =>  'Business address is required.',
                'latitude.required'         =>  'Please choose a valid address.',
                'longitude.required'        =>  'Please choose a valid address.',
                'profile_pic.image'         =>  'Profile picture should be a valid image.'
            );
            $validator = Validator::make($request->all(), $rules,$msg);
            if ($validator->fails())
            return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
 
            $data = $request->all();
            if (!empty($data['profile_pic'])) {
                $data['profile_pic'] = CommonController::image_uploader(Input::file('profile_pic'));
                if (empty($data['profile_pic']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            } else
                $data['profile_pic'] = $admin->profile_pic;
            $data['email']           =    $admin->email;
            $request['id'] = User::update_user_admin($data,$admin);  
              
            return Redirect::back()->with('status', 'Profile Updated Successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

/////////////		Admin Password Update Get  //////////////////////////////////////////////////////

    public static function bpassword_update_get() {
        try {
            return View::make('Business.Other.passwordupdate');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

//////////////		Admin Password Update post  //////////////////////////////////////////////////////

    public static function bpassword_update_post() {
        try {
            $data = Input::all();
            $rules = array('old_password' => 'required', 'password' => 'required|confirmed');

            $validator = Validator::make($data, $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first());

            $admin = \App('admin'); 
            if (!\Hash::check($data['old_password'], $admin->password))
                return Redirect::back()->withErrors('Please Enter the Correct Old Password');

            DB::Table('users')->where('id', $admin->id)->update(array(
                'password'      => \Hash::make($data['password']),
                'updated_at'    => new \DateTime
            ));

            return Redirect::Back()->with('status', 'Password Updated Successfully.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
     public static function business_cat_features(Request $request, $cat_id, $product_id) {
        $existing_key   =   []      ;
        $features       =   []      ;
        if ($product_id != 0 || !empty($product_id)) {
            $productDetails = Product::find($product_id);
            $old_features1 = $productDetails->features; 
             if (isset($old_features1) && !empty($old_features1)) {
                $featuresArr = json_decode($old_features1);
                foreach ($featuresArr as $key => $val) {
                    $existing_key[] = $key;
                    $features[$key] = $val;
                }
            }  
        } 
        $old_features = DB::table('cat_features')->where('cat_id', $cat_id)->get();
       
        if (count($old_features) > 0) {
            foreach ($old_features as $feature) { 
                if(in_array($feature->feature_name,$existing_key)===false)
                $features[$feature->feature_name] = ''; 
            }
        }
        if (count($features) > 0) {
            return response(['success' => 1, 'statuscode' => 200, 'msg' => '', 'result' => ['features' => $features]], 200);
        } else {
            return response(['success' => 1, 'statuscode' => 400, 'msg' => 'No features available.'], 200);
        }
    }

      public static function business_cat_features_service(Request $request, $cat_id, $service_id) { 
        $existing_key   =   []      ;
        $features       =   []      ;
        if ($service_id != 0 || !empty($service_id)) {
            $serviceDetails = Service::find($service_id);
            $old_features1 = $serviceDetails->features; 
             if (isset($old_features1) && !empty($old_features1)) {
                $featuresArr = json_decode($old_features1);
                foreach ($featuresArr as $key => $val) {
                    $existing_key[] = $key;
                    $features[$key] = $val;
                }
            }  
        } 
        $old_features = DB::table('cat_features')->where('cat_id', $cat_id)->get();
       
        if (count($old_features) > 0) {
            foreach ($old_features as $feature) { 
                if(in_array($feature->feature_name,$existing_key)===false)
                $features[$feature->feature_name] = ''; 
            }
        }
        if (count($features) > 0) {
            return response(['success' => 1, 'statuscode' => 200, 'msg' => '', 'result' => ['features' => $features]], 200);
        } else {
            return response(['success' => 1, 'statuscode' => 400, 'msg' => 'No features available.'], 200);
        }
    }
}
