<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use Illuminate\Validation\Rule;
use View;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Controllers\CommonController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Gallery;
use App\Models\Business;
use Intervention\Image\ImageManagerStatic as Image;

class AdminProductController extends Controller {

    ///////////////////////////////////////////// All Products     ///////////////////////////////////// 
    public static function admin_product_all(Request $request) {
    try {   
// ,DB::RAW('(select count(*) from users where users.id = products.user_id) as userID')
            $admin = \App('admin');
            $products = Product::select('*'
                   )->with('categories')
                   // ->where('status', 'active')
                    ->where('user_id', $admin->id)
                    
                  //  ->havingRaw('userID = ?',[$admin->id])
                    ->paginate(10);
            return View::make('Business.Product.allproducts')->with('products', $products);
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_product_add_get(Request $request) {  
        try { 
        $admin = \App('admin');
        $businesses = Business::where('user_id',$admin->id)
                //where('status','active')
                ->get(); 
            $maincategories = Category::where('status','active')->get();  
            return View::make('Business.Product.addproduct')->with(compact('maincategories','businesses'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    ///////////////////////// 		Add Category Post     ///////// 
    public static function admin_product_add_post(Request $request) {
        try {    
            $admin = \App('admin'); 
            $rules = array(
                'p_name' => 'required',
                'p_img' => 'image',
                'cat_id' => 'required'
            );
            $msg = array(
                'p_name.required' => 'Product name is required.',
                'p_img.image' => 'Product image should be a valid image.',
                'cat_id.required' => 'You have to choose one category.'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            if (!empty($request->p_img)) {
                $filename = CommonController::image_uploader(Input::file('p_img'));

            if (empty($filename))
                return Redirect::back()->witherrors("Image Not Uploaded please try again");
             $data['p_img'] = $filename;
            }
            $featuredata = [];
            /** If features added */
            if (count($request->features) > 0) {
                foreach ($request->features as $key => $val) {
                    foreach ($val as $key2 => $val2) {
                        if (!empty($val2))
                            $featuredata[$key2] = $val2;
                    }
                }
                $data['features'] = json_encode($featuredata);
            }
            /** features added end */ 
            $data['user_id']        =   $admin->id;
            $data['status']         =   'block';
            $product_id  =   Product::add_new_product($data);
            return Redirect::route('business_product_edit',$product_id)->with('status', 'Product added successfully.');
            //->with('status','Category added successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_product_edit(Request $request, $id = null) {
        try {
            $admin = \App('admin');
            $businesses = Business::where('user_id',$admin->id)->get(); 
            $maincategories = Category::where('status', 'active')->get(); //
            $product = Product::where('user_id',$admin->id)->where('id',$id)->first();
            $check_newfeature = DB::table('cat_features')->where('cat_id', $product->cat_id)->get();

            return View::make('Business.Product.addproduct')->with(compact('product', 'maincategories', 'check_newfeature','businesses'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_product_update(Request $request) {
        try {
            $rules = array(
                'p_name' => 'required',
                'p_img' => 'image',
                'cat_id' => 'required'
            );
            $msg = array(
                'p_name.required' => 'Product name is required.',
                'p_img.image' => 'Product image should be a valid image.',
                'cat_id.required' => 'You have to choose one category.'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            $product = Product::findorfail($request->id);

            if (!empty($request['p_img'])) {
                $data['p_img'] = CommonController::image_uploader(Input::file('p_img'));
                if (empty($request['p_img']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            } else
                $data['p_img'] = $product->p_img; 
            $data['status'] = $product->status; 
            /*             * If features added */
            if (count($request->features) > 0) {
                foreach ($request->features as $key => $val) {
                    foreach ($val as $key2 => $val2) {
                        if (!empty($val2))
                            $featuredata[$key2] = $val2;
                    }
                }
                 if (isset($featuredata) && count($featuredata) > 0)
                     $data['features'] = json_encode($featuredata);
                 else
                     $data['features'] = '';
            }
            /** features added end */
            $request['id'] = Product::update_product($data);
            return Redirect::back()->with('status', 'Product updated successfully.');
            //->with('status','Category added successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

////////////////////Product Block Unblock 	////////////////////////////////////////////////////
    public static function admin_product_block(Request $request, $id = null, $status = null) {
        try {
            $admin = \App('admin'); 
            //$product = Product::findorfail($id);
            $product = Product::where('user_id',$admin->id)->where('id',$id)->first();

            if ($status == 1) {

                if ($product->status == 'block')
                    return Redirect::back()->withErrors('This Product is already Blocked')->withInput();

                DB::Table('products')->where('id', $product->id)->update(array(
                    'status' => 'block',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Product Blocked Successfully');
            }
            else {
                if ($product->status == 'active')
                    return Redirect::back()->withErrors('This Product is already Unblocked')->withInput();

                DB::Table('products')->where('id', $product->id)->update(array(
                    'status' => 'active',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Product Unblocked Successfully');
            }
        } catch (\Exception $e) {
              return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

   
    public static function admin_product_delete(Request $request, $id = null) {
        try {
            $admin = \App('admin'); 
            $product = Product::where('user_id',$admin->id)->where('id',$id)->first();
            $product->delete();
             
            return Redirect::back()->with('status', 'Product Successfully Deleted.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_product_addGallery_post(Request $request, $id = null) {
    try {
            $rules = array(
                'name' => 'required|image',                 
                'product_id' => 'required'
            );
            $msg = array(
                'name.required' => 'Gallery Image is required.',
                'name.image' => 'Gallery Image should be a valid image.',
                'product_id.required' => 'Something wrong, please retry after page refresh!'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all(); 
            if (!empty($request['name'])) {
                $data['name'] = CommonController::image_uploader(Input::file('name'));
                if (empty($request['name']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            }            
            $request['id'] = Gallery::add_new_gallery_image($data);
            return Redirect::back()->with('status', 'Image added successfully.'); 
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    public static function admin_product_addGallery_get(Request $request, $id = null) {
        try {
            $admin = \App('admin'); 
            $product = Product::where('id',$id)->where('user_id',$admin->id)->first();  
            $gallery = Gallery::where('product_id',$product->id)->paginate(5);  
            return View::make('Business.Product.addgallery')->with(compact('gallery','product'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
     
    public static function admin_pgallery_delete(Request $request, $id = null) {
        try {
            $gallery = Gallery::findorfail($id);
            $gallery->delete();
            return Redirect::back()->with('status', 'Image Successfully Deleted.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
     public static function admin_pgallery_block(Request $request, $id = null, $status = null) {
        try {
            $gallery = Gallery::findorfail($id);

            if ($status == 1) {

                if ($gallery->status == 'block')
                    return Redirect::back()->withErrors('This image is already Blocked')->withInput();

                DB::Table('gallery')->where('id', $gallery->id)->update(array(
                    'status' => 'block',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Image Blocked Successfully');
            }
            else {
                if ($gallery->status == 'active')
                    return Redirect::back()->withErrors('This Image is already Unblocked')->withInput();

                DB::Table('gallery')->where('id', $gallery->id)->update(array(
                    'status' => 'active',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Image Unblocked Successfully');
            }
        } catch (\Exception $e) {
             return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    } 
}
