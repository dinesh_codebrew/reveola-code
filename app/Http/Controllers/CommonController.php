<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AWS;
use Carbon\Carbon;
use Image;

class CommonController extends Controller {

    ///////////		Multiple	Image Uploads 			//////////////////////////////

    public static function upload_multiple_images($images, $data) {

        $con = array();
        $s3 = \AWS::createClient('s3');

        foreach ($images as $key => $image) {

            $extention = $image->GetClientOriginalExtension();
            $filename = substr(sha1(time() . time()), 0, 25) . str_random(25) . ".{$extention}";

            $upload_success = $s3->putObject(array(
                'ACL' => 'public-read',
                'Bucket' => env('AWS_BUCKET'),
                'Key' => 'Files/' . $filename,
                'Body' => fopen($image->getPathname(), 'r'),
                'ContentType' => 'image/' . $extention,
            ));

            array_push($con, $filename);
        }

        if (count($con) == count($images))
            return $con;
        else
            return [];
    }

////////////////////////////////// Image Uploader ////////////////////////////////////////////////

    public static function image_uploader($image) {
        try { 
            
          //  $info = getimagesize($source);
            print_r('<pre>');
            print_r($image);
            foreach($_FILES as $data){
            print_r($data->size());
            
            }
                die(' here');
            
            $extention = $image->GetClientOriginalExtension();
            $filename = substr(sha1(time() . time()), 0, 30) . str_random(30) . ".{$extention}";
            $s3 = \AWS::createClient('s3');
            
            $image_resize = Image::make($image->getRealPath());              
            //$image_resize->resize(300, 300);
            $image_resize->save(public_path('images/ServiceImages/' .$filename),50);
              
            $upload_success = $s3->putObject(array(
                'ACL' => 'public-read',
                'Bucket' => env('AWS_BUCKET'),
                'Key' => 'Files/' . $filename,
               // 'Body' => fopen($image->getPathname(), 'r'),
                'Body' => fopen(public_path('images/ServiceImages/' .$filename), 'r'),
                'ContentType' => 'image/' . $extention,
            ));
            unlink(public_path('images/ServiceImages/' .$filename));
            return $upload_success ? $filename : '';
        } catch (\Exception $e) { 
            return '';
        }
    }

//////////////////////////////////////		Starting Ending DT  //////////////////////////////////////////////////

    public static function set_starting_ending($data) {
        $time = new \DateTime('now', new \DateTimeZone($data['timezone']));
        $data['timezonez'] = $time->format('P');

        if (!isset($data['daterange'])) {
            $data['starting_dt'] = Carbon::now()->subMonths(2)->format('Y-m-d');
            $data['ending_dt'] = Carbon::now()->addday(1)->format('Y-m-d');
        } else {
            $data['daterange'] = urldecode($data['daterange']);
            $temp_array = explode(' - ', $data['daterange']);
            $data['starting_dt'] = Carbon::CreateFromFormat('d/m/Y', $temp_array[0], $data['timezone'])->timezone('UTC')->format('Y-m-d');
            $data['ending_dt'] = Carbon::CreateFromFormat('d/m/Y', $temp_array[1], $data['timezone'])->timezone('UTC')->format('Y-m-d');
        }

        $data['starting_dt'] = $data['starting_dt'] . ' 00:00:00';
        $data['ending_dt'] = $data['ending_dt'] . ' 23:59:59';

        $data['fstarting_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $data['starting_dt'], 'UTC')->timezone($data['timezone'])->format('d/m/Y');
        $data['fending_dt'] = Carbon::CreateFromFormat('Y-m-d H:i:s', $data['ending_dt'])->timezone($data['timezone'])->format('d/m/Y');

        return $data;
    }

///////////////////
//
    
    
 
   public static function compress_image($source_url, $destination_url, $quality) {

      $info = getimagesize($source_url);

          if ($info['mime'] == 'image/jpeg')
          $image = imagecreatefromjpeg($source_url);

          elseif ($info['mime'] == 'image/gif')
          $image = imagecreatefromgif($source_url);

          elseif ($info['mime'] == 'image/png')
          $image = imagecreatefrompng($source_url);

          imagejpeg($image, $destination_url, $quality);
          return $destination_url;
        }
        //  $url = 'C:/Users/admin/Downloads/compressed.jpg';
        //  $filename = compress_image($_FILES["file"]["tmp_name"], $url, 80);
         
}
