<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,
    Hash,
    DateTime,
    Mail,
    DB;
use App\Models\User;
use Redirect;

class UserController extends Controller {

    
    
    public function user_login_get() {
        return view('User.login');
    }
    
    public function user_login_post() {
        return view('User.login');
    }
    public function usersignup_get() {
        return view('User.signup');
    }

    public function usersignup_post(Request $request) {
        try{
        $rules = array(
            'name' => 'bail|required',
            'email' => 'bail|email|required_without_all:facebook_id,gmail_id,linkedin_id,twitter_id,Instagram_id|unique:users',
            'phone' => 'bail|required|unique:users',
            'password' => 'bail|min:7|required_without_all:facebook_id,gmail_id,linkedin_id,twitter_id,Instagram_id'
        );
        $msg = array(
            'email.exists' => __('signup.email_exist'),
            'phone.exists' => __('signup.phone_exist')
        );

        $validator = Validator::make($request->all(), $rules, $msg);
        if ($validator->fails())
            return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();
           
        $user = User::create_new_user($request->all());   
        return Redirect::back()->with('status',__('signup.signup_success'));
        }
        catch(\Exception $e){
             return Redirect::back()->writeerrors($e->getMessage())->withInputs();
        }
        
    }

}
