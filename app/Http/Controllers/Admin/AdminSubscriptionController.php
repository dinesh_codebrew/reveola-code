<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use Illuminate\Validation\Rule;
use View;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Controllers\CommonController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Service;
use App\Models\User;
use App\Models\Order;
use App\Models\Subscription;

class AdminSubscriptionController extends Controller
{
    public static function admin_sales_all(Request $request) { 
        try {
            $admin = \App('admin');
 
            $orders = Order::paginate(10);//where('status', 'active')->
            return View::make('Admin.Subscription.allorders')->with('orders', $orders);
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
      public static function admin_sale_view(Request $request, $id = null) {
        try { 
            Order::with('user')->findorfail($id); 
            $order = Order::get_order_details($id);  
            return View::make('Admin.Subscription.orderview')->with(compact('order'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
     public static function admin_sale_delete(Request $request, $id = null) {
        try {
            $order = Order::findorfail($id);
            $order->delete();  
            return Redirect::back()->with('status', 'Order Successfully Deleted.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
    
    
    
    
//////////////// 		All Subcriiptions     /////////// ///// 
    public static function admin_subscription_all(Request $request) { 
        try {
            $admin = \App('admin');
 
            $subscriptions = Subscription::paginate(10);//where('status', 'active')->
            return View::make('Admin.Subscription.allsubscriptions')->with('subscriptions', $subscriptions);
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
   
      public static function admin_subscription_delete(Request $request, $id = null) {
        try {
            $category = Subscription::findorfail($id);
            $category->delete();
            DB::table('cat_features')->where('cat_id', $id)->delete();
            return Redirect::back()->with('status', 'Category Successfully Deleted.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
////////////////////Category Block Unblock 	////////////////////////////////////////////////////
    public static function admin_subscription_block(Request $request, $id = null, $status = null) {
        try {
            $subscription = Subscription::findorfail($id);

            if ($status == 1) {

                if ($subscription->status == 'block')
                    return Redirect::back()->withErrors('This Subscription is already Blocked')->withInput();

                DB::Table('subscriptions')->where('id', $subscription->id)->update(array(
                    'status' => 'block',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Subscription Blocked Successfully');
            }
            else {
                if ($subscription->status == 'active')
                    return Redirect::back()->withErrors('This Subscription is already Unblocked')->withInput();

                DB::Table('subscriptions')->where('id', $subscription->id)->update(array(
                    'status' => 'active',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Subscription Unblocked Successfully');
            }
        } catch (\Exception $e) {
           return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
    public static function admin_subscription_add_get(){}
    public static function admin_subscription_add_post(){}
    public static function admin_subscription_edit(){}
    public static function admin_subscription_update(){}

}
