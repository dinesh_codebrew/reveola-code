<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use Illuminate\Validation\Rule;
use View;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Excel;
use App\Http\Controllers\CommonController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Service;

class AdminCategoryController extends Controller {

     public static function admin_category_export(){
      $items = Category::all();
      Excel::create('items', function($excel) use($items) {
          $excel->sheet('ExportFile', function($sheet) use($items) {
              $sheet->fromArray($items);
          });
      })->export('xls');

    }
    public static function buildCategory($parent, $category) {
        $html = "";
        if (isset($category['parent_cats'][$parent])) {
            $html .= "<ul>\n";
            foreach ($category['parent_cats'][$parent] as $cat_id) {
                if (!isset($category['parent_id'][$cat_id])) {
                    $html .= "<li>" . $category['categories'][$cat_id]['id'] . "==" . $category['categories'][$cat_id]['name'] . "==" . $category['categories'][$cat_id]['sort_order'] . "</a>\n</li> \n";
                }
                if (isset($category['parent_cats'][$cat_id])) {
                    // $html .= "<li>" . $category['categories'][$cat_id]['name'] . "</a> \n";
                    $html .= AdminCategoryController::buildCategory($cat_id, $category);
                    $html .= "</li> \n";
                }
            }
            $html .= "</ul> \n";
        }
        return $html;
    }

/////////////////////////////////////////////			All Categories     ///////////////////////////////////// 
    public static function admin_category_all(Request $request) { 
        /*  $category1= Category::orderBy('sort_order','Asc')->get();

          $category = array(
          'categories' => array(),
          'parent_cats' => array()
          );
          foreach($category1 as $cat){
          $category['categories'][$cat['id']] = $cat;
          $category['parent_cats'][$cat['parent_id']][] = $cat['id'];
          }
          $data =  AdminCategoryController::buildCategory(0, $category);
          print_r('<pre>');
          print_r($data);
          die; */
        try {
            $admin = \App('admin');

            // $request['timezone'] = $admin->timezone;
            //  $data = CommonController::set_starting_ending($request->all());
            //     $categories = Category::where('parent_id', 0)->get();
            $categories = Category::where('parent_id', 0)->orderBy('sort_order')->paginate(10);
            return View::make('Admin.Category.allcategories')->with('categories', $categories);
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

/////////////////////////////////////////////			Add Category Get     ///////////////////////////////////////
    public static function admin_category_add_get(Request $request) {
        try {
            return View::make('Admin.Category.addcategory');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

///////////////////////// 		Add Category Post     ///////// 
    public static function admin_category_add_post(Request $request) {
        try {
            $rules = array(
                //'unique:items,provider_id,NULL,id,barcode,'.$request->barcode
                'name' => 'required|unique:categories,name|max:150',
                'cat_image' => 'image',
                'sort_order' => 'numeric'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            if (!empty($request->cat_image)) {
                $filename = CommonController::image_uploader(Input::file('cat_image'));

                if (empty($filename))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");

                $data['cat_image'] = $filename;
            }
            $request['category_id'] = Category::add_new_category($data);

            if (count($request->feature) > 0) {
                foreach ($request->feature as $featurename) {
                    if (!empty($featurename))
                        $featuredata[] = ['cat_id' => $request['category_id'], 'feature_name' => $featurename, 'feature_slug' => AdminCategoryController::slugify($featurename)];
                }

                //Model::insert($data); // Eloquent approach
                DB::table('cat_features')->insert($featuredata);
            }
            return Redirect::back()->with('status', 'Category added successfully');
            //->with('status','Category added successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

/////////////////////////////////////////////			Add Category Post     /////////////////////////////////////
    public static function admin_category_edit(Request $request, $id = null) {
        try {
            $category = Category::findorfail($id);
            $old_features = DB::table('cat_features')->where('cat_id', $id)->get();
            if (is_object($old_features)) {
                foreach ($old_features as $feature) {
                    $features[] = $feature->feature_name;
                }
            }
            return View::make('Admin.Category.addcategory')->with(compact(['category', 'features']));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_category_delete(Request $request, $id = null) {
        try {
            $category = Category::findorfail($id);
            $category->delete();
            DB::table('cat_features')->where('cat_id', $id)->delete();
            return Redirect::back()->with('status', 'Category Successfully Deleted.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_category_update(Request $request) {
        try {
            $rules = array(
                'id' => 'required',
                'name' => 'required||unique:categories,name,' . $request->id,
                'cat_image' => 'image|max:500000',
                'sort_order' => 'numeric'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            $category = Category::findorfail($request->id);

            if (!empty($request['cat_image'])) {
                $data['cat_image'] = CommonController::image_uploader(Input::file('cat_image'));
                if (empty($request['cat_image']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            } else
                $data['cat_image'] = $category->cat_image;

            $request['id'] = Category::update_category($data);

            /* First delete old records if added before */
            DB::table('cat_features')->where('cat_id', $request['id'])->delete();
            if (count($request->feature) > 0) {
                $data2 = [];
                foreach ($request->feature as $featurename) {
                    if (!empty($featurename))
                        $data2[] = ['cat_id' => $request['id'], 'feature_name' => $featurename, 'feature_slug' => AdminCategoryController::slugify($featurename)];
                }
                DB::table('cat_features')->insert($data2);
            }
            return Redirect::back()->with('status', 'Category updated successfully')->with(compact('category'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

////////////////////Category Block Unblock 	////////////////////////////////////////////////////
    public static function admin_category_block(Request $request, $id = null, $status = null) {
        try {
            $category = Category::findorfail($id);

            if ($status == 1) {

                if ($category->status == 'block')
                    return Redirect::back()->withErrors('This Category is already Blocked')->withInput();

                DB::Table('categories')->where('id', $category->id)->update(array(
                    'status' => 'block',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Category Blocked Successfully');
            }
            else {
                if ($category->status == 'active')
                    return Redirect::back()->withErrors('This Category is already Unblocked')->withInput();

                DB::Table('categories')->where('id', $category->id)->update(array(
                    'status' => 'active',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Category Unblocked Successfully');
            }
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_cat_features(Request $request, $cat_id, $product_id) {
        $existing_key   =   []      ;
        $features       =   []      ;
        if ($product_id != 0 || !empty($product_id)) {
            $productDetails = Product::find($product_id);
            $old_features1 = $productDetails->features; 
             if (isset($old_features1) && !empty($old_features1)) {
                $featuresArr = json_decode($old_features1);
                foreach ($featuresArr as $key => $val) {
                    $existing_key[] = $key;
                    $features[$key] = $val;
                }
            }  
        } 
        $old_features = DB::table('cat_features')->where('cat_id', $cat_id)->get();
       
        if (count($old_features) > 0) {
            foreach ($old_features as $feature) { 
                if(in_array($feature->feature_name,$existing_key)===false)
                $features[$feature->feature_name] = ''; 
            }
        }
        if (count($features) > 0) {
            return response(['success' => 1, 'statuscode' => 200, 'msg' => '', 'result' => ['features' => $features]], 200);
        } else {
            return response(['success' => 1, 'statuscode' => 400, 'msg' => 'No features available.'], 200);
        }
    }

      public static function admin_cat_features_service(Request $request, $cat_id, $service_id) { 
        $existing_key   =   []      ;
        $features       =   []      ;
        if ($service_id != 0 || !empty($service_id)) {
            $serviceDetails = Service::find($service_id);
            $old_features1 = $serviceDetails->features; 
             if (isset($old_features1) && !empty($old_features1)) {
                $featuresArr = json_decode($old_features1);
                foreach ($featuresArr as $key => $val) {
                    $existing_key[] = $key;
                    $features[$key] = $val;
                }
            }  
        } 
        $old_features = DB::table('cat_features')->where('cat_id', $cat_id)->get();
       
        if (count($old_features) > 0) {
            foreach ($old_features as $feature) { 
                if(in_array($feature->feature_name,$existing_key)===false)
                $features[$feature->feature_name] = ''; 
            }
        }
        if (count($features) > 0) {
            return response(['success' => 1, 'statuscode' => 200, 'msg' => '', 'result' => ['features' => $features]], 200);
        } else {
            return response(['success' => 1, 'statuscode' => 400, 'msg' => 'No features available.'], 200);
        }
    }
//////////////////////////////////////////////////////////////////////
//////////////////////			SubCategories Section 		///////////////////////////////
//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////			Add Categories     /////////////////////////////////////

    public static function admin_subcategory_all(Request $request) {
        try {
            $admin = \App('admin');

            // $request['timezone'] = $admin->timezone;
            //  $data = CommonController::set_starting_ending($request->all());

            $subcat = Category::where('parent_id', '!=', 0)->with('parent')->orderBy('id')->paginate(10);

            return View::make('Admin.Category.allsubcategories')->with('categories', $subcat);
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_subcat_add_get(Request $request) {
        try {
            $maincategories = Category::orderBy('name')->get();  
            return View::make('Admin.Category.addsubcategory')->with(compact('maincategories'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_subcat_add_post(Request $request) {
        try {
            $rules = array(                
                'name' => 'required|unique:categories,name|max:150',                
                'sort_order' => 'numeric',
                'cat_image' => 'image',
                'parent_id' => 'required'
            );
            $msg = array('parent_id.required' => 'parent category is required .');

            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

            $data = $request->all();
            if (!empty($request['cat_image'])) {
                $data['cat_image'] = CommonController::image_uploader(Input::file('cat_image'));
                if (empty($request['cat_image']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            }

            $category = Category::add_new_category($data);
            if (count($request->feature) > 0) {
                foreach ($request->feature as $featurename) {
                    if (!empty($featurename))
                        $data[] = ['cat_id' => $category, 'feature_name' => $featurename, 'feature_slug' => AdminCategoryController::slugify($featurename)];
                }

                //Model::insert($data); // Eloquent approach
                DB::table('cat_features')->insert($data);
            }
            return Redirect::back()->with('status', 'SubCategory added Successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

/////////////////////////////////////////////			Update Categories     /////////////////////////////////////
    public static function admin_subcat_edit(Request $request, $id = null) {
        try {
            $maincategories = Category::where('id', '!=', $request->id)->get(); //
            $category = Category::findorfail($id);
            $old_features = DB::table('cat_features')->where('cat_id', $id)->get();
            if (is_object($old_features)) {
                foreach ($old_features as $feature) {
                    $features[] = $feature->feature_name;
                }
            }
            return View::make('Admin.Category.addsubcategory')->with(compact('category', 'maincategories', 'features'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_subcat_update(Request $request) {
        try {

            $rules = array(
                'id' => 'required',
                'name' => 'required||unique:categories,name,' . $request->id,
                'cat_image' => 'image',
                'sort_order' => 'numeric',
                'parent_id' => 'required',
            );
            $msg = array('parent_id.required' => 'parent category is required .');

            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

            $category = Category::findorfail($request->id);
            $data = $request->all();
            if (!empty($request['cat_image'])) {
                $data['cat_image'] = CommonController::image_uploader(Input::file('cat_image'));
                if (empty($request['cat_image']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            } else
                $data['cat_image'] = $category->cat_image;

            $request['id'] = Category::update_category($data);
            /* First delete old records if added before */
            DB::table('cat_features')->where('cat_id', $request['id'])->delete();
            if (count($request->feature) > 0) {
                $data2 = [];
                foreach ($request->feature as $featurename) {
                    if (!empty($featurename))
                        $data2[] = ['cat_id' => $request['id'], 'feature_name' => $featurename, 'feature_slug' => AdminCategoryController::slugify($featurename)];
                }
                DB::table('cat_features')->insert($data2);
            }
            return Redirect::back()->with('status', 'SubCategory updated Successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

////////////////////		SubCategory Block Unblock 	////////////////////////////////////////////////////
    public static function admin_subcategory_block(Request $request) {
        try {

            $rules = array(
                'category_sub_id' => 'required|exists:category_subs,id',
                'block' => 'required|in:0,1'
            );
            $msg = array('category_sub_id.exists' => 'This Subcategory is currently not available');

            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput();

            $subcat = CategorySub::get_single_subcat($request->all());

            if ($request['block'] == 1) {

                if ($subcat->blocked == '1')
                    return Redirect::back()->withErrors('This SubCategory Is already Blocked')->withInput();

                DB::Table('category_subs')->where('id', $subcat->id)->update(array(
                    'blocked' => '1',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'SubCategory Blocked Successfully');
            }
            else {
                if ($subcat->blocked == '0')
                    return Redirect::back()->withErrors('This SubCategory Is already Unblocked')->withInput();

                DB::Table('category_subs')->where('id', $subcat->id)->update(array(
                    'blocked' => '0',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'SubCategory Unblocked Successfully');
            }
        } catch (\Exception $e) {
            return \Response(array('success' => 0, 'msg' => $e->getMessage()), 200);
        }
    }

//////////////////////////
    public static function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}
