<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
//use Mail;
use View;
use Cookie;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Controllers\CommonController;
use Auth\AdminForgotPasswordController;
use Auth\AdminResetPasswordController;
use App\Models\Admin; 

class AdminController extends Controller {

    public static function admin_chat() { 
      return View::make('Admin.chat');
    }
    public static function showResetForm() {

        return View::make('Admin.passwords.email');
    }

//////////////		Admin Login Get 	////////////////////////////////////////////////////////////

    public static function admin_login_get() {
        $details = array();
        if (Cookie::get('ad_email') !== null) {
            $details['ad_email'] = Cookie::get('ad_email');
            $details['ad_psw'] = Cookie::get('ad_psw');
        }
        return View::make('Admin.Other.login')->with(compact('details'));
    }

//////////////		Admin Login Post 	////////////////////////////////////////////////////////////

    public static function admin_login_post(Request $request) {

        $remember = $request->get('remember');
        if (!$remember) {
            // Cookie::forget('ad_email');
            //Cookie::forget('ad_psw');
            Cookie::queue(\Cookie::forget('ad_email'));
        }
        try {
            $rules = array(
                'email' => 'required|email|exists:admin,email',
                'password' => 'required',
                'timezone' => 'required|timezone');
            $msg = array(
                'email.exists' => 'Sorry, this email is not registered with us'
            );

            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();

            $request['dynamic_column'] = 'email';
            $request['dynamic_value'] = $request['email'];
            $admin = Admin::return_by_dynamic($request);

            if (!\Hash::Check($request['password'], $admin->password))
                return Redirect::back()->witherrors('Password is Incorrect')->withInput();

            $time = new \DateTime('now', new \DateTimeZone($request['timezone']));
            $request['timezonez'] = $time->format('P');

            \Cookie::queue('adminid', $admin->id, 60);

            Admin::admin_update($request, $admin);
            if ($remember) {
                Cookie::queue('ad_email', $request['email'], time() + (10 * 365 * 24 * 60 * 60));
                Cookie::queue('ad_psw', $request['password'], time() + (10 * 365 * 24 * 60 * 60));
            }
            return Redirect::route('admin_dashboard')->with('successMsg', 'Logged In Successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

///////////////////////////				Logout 			///////////////////////////////////////////////////////////

    public static function admin_logout() {
        try {

            \Cookie::queue('adminid', 0, 0);

            return Redirect::route('admin_login_get')->with('fresh', '1')->with('status', 'Logged Out Successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

///////////////////////////////////			Admin Dash Board 			////////////////////////////////////////

    public static function admin_dashboard(Request $request) { 
        $starting_dt    = Carbon::now(\Session::get('timezonez'))->subMonths(2)->format('d/m/Y');
        $ending_dt      = Carbon::now(\Session::get('timezonez'))->addday(1)->format('d/m/Y');
        /*  $request['starting_dt'] = $request['starting_dt'] . ' 00:00:00';
            $request['ending_dt'] = $request['ending_dt'] . ' 23:59:59';        */
        $data['starting_dt'] = '1970-01-01 00:00:00';
        $data['ending_dt'] = date('Y-m-d h:i:s', time());
        $stats_data = AdminController::stats_data($data);
        $data['user_id']='0'; 
        $reviews    =   \App\Models\Review::get_all_reviews($data);
        return View::make('Admin.dashboard.index')->with(compact('stats_data', 'starting_dt', 'ending_dt','reviews'));
    }

///////////////////////////////////			Dashboard Data 		////////////////////////////////////////////////

    public static function get_dashboard_data(Request $request) {
        try {
           /* $request['daterange'] = urldecode($request['daterange']);
            $temp_array = explode(' - ', $request['daterange']);

            $request['starting_dt'] = Carbon::CreateFromFormat('d/m/Y', $temp_array[0], \Session::get('timezonez'))->timezone('UTC')->format('Y-m-d');
            $request['ending_dt'] = Carbon::CreateFromFormat('d/m/Y', $temp_array[1], \Session::get('timezonez'))->timezone('UTC')->format('Y-m-d');

            $request['starting_dt'] = $request['starting_dt'] . ' 00:00:00';
            $request['ending_dt'] = $request['ending_dt'] . ' 23:59:59';

            $stats_data = AdminController::stats_data($request);*/
            $graph_data = AdminController::new_graph_data($request);

            return Response(array('success' => '1', 'msg' => 'Dashboard Data', 'graph_data' => $graph_data), 200);
        } catch (\Exception $e) {
            return Response(array('success' => '0', 'msg' => $e->getMessage()), 200);
        }
    }

//////////////		Admin DashBoard Data		///////////////////////////////////////////////

    public static function stats_data($data) { //`user_type`='user' and
        return DB::SELECT("SELECT
	(SELECT COUNT(*) FROM users WHERE  created_at BETWEEN :sdt1 AND :edt1 AND id != 0) as users_count,
	(SELECT COUNT(*) FROM categories WHERE `parent_id` = 0 and created_at BETWEEN :sdt2 AND :edt2) as cat_count,
	(SELECT COUNT(*) FROM categories WHERE `parent_id` != 0 and created_at BETWEEN :sdt4 AND :edt4) as subcat_count,
	(SELECT COUNT(*) FROM orders WHERE  created_at BETWEEN :sdt3 AND :edt3) as orders_count FROM admin LIMIT 0, 1 ", [ 'sdt1' => $data['starting_dt'], 'edt1' => $data['ending_dt'],
                    'sdt2' => $data['starting_dt'], 'edt2' => $data['ending_dt'],
                    'sdt3' => $data['starting_dt'], 'edt3' => $data['ending_dt'],
                    'sdt4' => $data['starting_dt'], 'edt4' => $data['ending_dt']
        ]);
    }

//////		Graph Data 		//////////////////////////////////////////////////////////////////////////

    public static function new_graph_data($data) {  
            if(!isset($data['yr_only']))
                $yr_only = Carbon::now(\Session::get('timezonez'))->format('Y');
            else
                $yr_only  = $data['yr_only'];
	$querys = DB::SELECT("SELECT months_tbl.id as id,
	CAST((SELECT COUNT(*) FROM users WHERE user_type='user' and YEAR(created_at) = :yr1 AND MONTH(created_at) = months_tbl.id AND id != 0) AS UNSIGNED) as users,
        CAST((SELECT COUNT(*) FROM users WHERE user_type='business' and YEAR(created_at) = :yr4 AND MONTH(created_at) = months_tbl.id AND id != 0) AS UNSIGNED) as busers,
	CAST((SELECT COUNT(*) FROM products WHERE YEAR(created_at) = :yr2 AND MONTH(created_at) = months_tbl.id) AS UNSIGNED) as products,
	CAST((SELECT COUNT(*) FROM orders WHERE YEAR(created_at) = :yr3 AND MONTH(created_at) = months_tbl.id) AS UNSIGNED) as orders  FROM months_tbl",['yr1'=>$yr_only, 'yr2'=>$yr_only, 'yr3'=>$yr_only, 'yr4'=>$yr_only]);
        /*
$graph_users = DB::SELECT("SELECT COUNT(*) as count , MONTH(`created_at`) as month,`sub_name`as Package FROM orders group by month, `Package` order by month ASC ");*/
                $users = array();
                $busers = array();
                $products = [];
                $orders = array(); 
                $orders_all = array(); 

                foreach($querys as $query)
                        {
                            array_push($users, $query->users);
                            array_push($busers, $query->busers);
                            array_push($products,$query->products);
                            array_push($orders, $query->orders);  
                        }

    		return array('users'=>$users, 'busers'=>$busers, 'products'=>$products, 'orders'=>$orders,'dt'=>$yr_only);
        
      /*$graph_users = DB::SELECT("SELECT COUNT(*) as count , MONTH(`created_at`) as month,`user_type`as type FROM users group by MONTH(`created_at`), `user_type` order by MONTH(`created_at`) ASC ");
       
        $graph_sales = DB::SELECT("SELECT COUNT(*) as count , MONTH(`created_at`) as month FROM sales group by MONTH(`created_at`) order by MONTH(`created_at`) ASC");
 
        return array('users' => $graph_users,  'sales' => $graph_sales );*/
    }

//////////////////		Admin Profile Update Get ////////////////////////////////////////////////////////////

    public static function aprofile_update_get() {
        try {

            return View::make('Admin.Other.profileupdate');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

//////////////		Admin Profile Update Post ////////////////////////////////////////////////////////////

    public static function aprofile_update_post() {
        try {

            $data = Input::all();
            $rules = array(
                'name' => 'required',
                'job' => 'required',
                'profile_pic' => 'image',
            );

            $validator = Validator::make($data, $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first());

            $admin = \App('admin');

            if (!empty($data['profile_pic'])) {
                $filename = CommonController::image_uploader(Input::file('profile_pic'));
                if (empty($filename))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            } else
                $filename = $admin->profile_pic;

            DB::table('admin')->where('id', 1)->update(array(
                'name' => $data['name'],
                'job' => $data['job'],
                'profile_pic' => $filename,
                'updated_at' => new \DateTime
            ));

            return Redirect::back()->with('status', 'Profile Updated Successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

/////////////		Admin Password Update Get  //////////////////////////////////////////////////////

    public static function apassword_update_get() {
        try {
            return View::make('Admin.Other.passwordupdate');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

//////////////		Admin Password Update post  //////////////////////////////////////////////////////

    public static function apassword_update_post() {
        try {
            $data = Input::all();
            $rules = array('old_password' => 'required', 'password' => 'required|confirmed');

            $validator = Validator::make($data, $rules);
            if ($validator->fails())
                return Redirect::back()->withErrors($validator->getMessageBag()->first());

            $admin = \App('admin');

            if (!\Hash::check($data['old_password'], $admin->password))
                return Redirect::back()->withErrors('Please Enter the Correct Old Password');

            DB::Table('admin')->where('id', $admin->id)->update(array(
                'password' => \Hash::make($data['password']),
                'updated_at' => new \DateTime
            ));

            return Redirect::Back()->with('status', 'Password Updated Successfully');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

}
