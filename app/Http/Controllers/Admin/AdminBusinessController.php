<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use Illuminate\Validation\Rule;
use View;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Mail;
use Hash;
use App\Mail\sharecredentials;
use App\Http\Controllers\CommonController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Service;
use App\Models\Business;
use App\Models\Gallery;
use App\Models\User;

class AdminBusinessController extends Controller {

     public static function export(){
      $items = Business::all();
      Excel::create('items', function($excel) use($items) {
          $excel->sheet('ExportFile', function($sheet) use($items) {
              $sheet->fromArray($items);
          });
      })->export('xls');

    }
    public static function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }
    // share Credentials //
    public function admin_share_credentials(Request $request, $id = null) {       
        if ($id != '') {  
            $business               =   Business::select('*')->findorfail($id); 
            $user                   =   $business->user;
            $data['user_id']        =   $user->id ;
            $random_pass            =   AdminBusinessController::random_password();
            $data['new_password']   =   Hash::make($random_pass);
            $user->new_password     =   $random_pass; 
            User::update_user_password($data);  
            Mail::to($user->email)->send(new sharecredentials($user, $file=null));
        return response(['success' => 1, 'statuscode' => 200, 'msg' => 'Mail sent successfully.', 'result' => []], 200);
        } else {
            return response(['success' => 1, 'statuscode' => 400, 'msg' => 'Mail not send !'], 200);
        }
    }

    /////////////			All Business     ///////////////////////////////////// 
    public static function admin_business_all(Request $request) {           
        try {
            $admin = \App('admin');
            $businesses = Business::with('user')->orderBy('id')->paginate(10);

            return View::make('Admin.Business.allbusinesses')->with('businesses', $businesses);
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_business_add_get(Request $request) {
        try {
            $maincategories = Category::where('status', 'active')->get(); //
            $users = User::select('*'
                            , DB::RAW('(select count(*) from businesses b where b.user_id =users.id) as business_count'))
                    ->havingRaw('business_count = ?', ['0'])
                    ->where('status', 'active')
                    ->where('user_type', 'business')
                    ->orderByRaw('business_count')
                    ->get();
            return View::make('Admin.Business.addbusiness')->with(compact('maincategories', 'users'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    ///////////////////////// 		Add Category Post     ///////// 
    public static function admin_business_add_post(Request $request) {
        try {
            $rules = array(
                'b_name' => 'required|max:150',
                'b_pic' => 'image',
                'b_address' => 'required',
                'b_lat' => 'required',
                'b_lng' => 'required',
                'cat_id' => 'required'
            );
            $msg = array(
                'b_name.required' => 'Business name is required.',
                'b_address.required' => 'Business address is required.',
                'b_lat.required' => 'Please choose a valid address.',
                'b_lng.required' => 'Please choose a valid address.',
                'b_pic.image' => 'Business image should be a valid image.',
                'cat_id.required' => 'You have to choose one category.'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            if (!empty($request->b_pic)) {
                $filename = CommonController::image_uploader(Input::file('b_pic'));

                if (empty($filename))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");

                $data['b_pic'] = $filename;
            }

            /*             * If features added */
            if (count($request->features) > 0) {
                foreach ($request->features as $key => $val) {
                    foreach ($val as $key2 => $val2) {
                        if (!empty($val2))
                            $featuredata[$key2] = $val2;
                    }
                }
                $data['features'] = json_encode($featuredata);
            }
            /** features added end */
            $request['business_id'] = Business::add_new_business($data);
            return Redirect::back()->with('status', 'Business added successfully.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_business_edit(Request $request, $id = null) {
        try {
            $maincategories = Category::where('status', 'active')->get(); //
            $business = Business::findorfail($id);
//          $check_newfeature=  DB::table('cat_features')->where('cat_id', $service->cat_id)->get();
            $users = User::select('*'
                            , DB::RAW('(select count(*) from businesses b where b.user_id =users.id) as business_count'))
                    ->havingRaw('business_count = ?', ['0'])
                    ->where('status', 'active')
                    ->where('user_type', 'business')
                    ->orderByRaw('business_count')
                    ->get();   
            return View::make('Admin.Business.addbusiness')->with(compact('business', 'maincategories','users'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_business_update(Request $request) {
        try {
            $rules = array(
                'b_name' => 'required|max:150',
                'b_pic' => 'image',
                'b_address' => 'required',
                'b_lat' => 'required',
                'b_lng' => 'required',
                'cat_id' => 'required'
            );
            $msg = array(
                'b_name.required' => 'Business name is required.',
                'b_address.required' => 'Business address is required.',
                'b_lat.required' => 'Please choose a valid address.',
                'b_lng.required' => 'Please choose a valid address.',
                'b_pic.image' => 'Business image should be a valid image.',
                'cat_id.required' => 'You have to choose one category.'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            $business = Business::findorfail($request->id);

            if (!empty($request['b_pic'])) {
                $data['b_pic'] = CommonController::image_uploader(Input::file('b_pic'));
                if (empty($request['b_pic']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            } else
                $data['b_pic'] = $business->b_pic;

            /*             * If features added */
            /*  if (count($request->features) > 0) {
              foreach ($request->features as $key=>$val) {
              foreach ($val as $key2=>$val2) {
              if (!empty($val2))
              $featuredata[$key2] = $val2;
              }
              }
              if (isset($featuredata) && count($featuredata) > 0)
              $data['features'] = json_encode($featuredata);
              else
              $data['features'] = '';
              } */
            /** features added end */
            $request['id'] = Business::update_business($data);
            return Redirect::back()->with('status', 'Business updated successfully.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

////////////////////Service Block Unblock 	////////////////////////////////////////////////////
    public static function admin_business_block(Request $request, $id = null, $status = null) {
        try {
            $business = Business::findorfail($id);

            if ($status == 1) {

                if ($business->status == 'block')
                    return Redirect::back()->withErrors('This Business is already Blocked')->withInput();

                DB::Table('businesses')->where('id', $business->id)->update(array(
                    'status' => 'block',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Business Blocked Successfully');
            }
            else {
                if ($business->status == 'active')
                    return Redirect::back()->withErrors('This Business is already Unblocked')->withInput();

                DB::Table('businesses')->where('id', $business->id)->update(array(
                    'status' => 'active',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Business Unblocked Successfully');
            }
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_business_delete(Request $request, $id = null) {
        try {
            $business = Business::findorfail($id);
            $business->delete();

            return Redirect::back()->with('status', 'Business Successfully Deleted.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_business_addGallery_post(Request $request, $id = null) {
        try {
            $rules = array(
                'name' => 'required|image',
                'business_id' => 'required'
            );
            $msg = array(
                'name.required' => 'Gallery Image is required.',
                'name.image' => 'Gallery Image should be a valid image.',
                'business_id.required' => 'Something wrong, please retry after page refresh!'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            if (!empty($request['name'])) {
                $data['name'] = CommonController::image_uploader(Input::file('name'));
                if (empty($request['name']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            }
            $request['id'] = Gallery::add_new_gallery_image($data);
            return Redirect::back()->with('status', 'Image added successfully.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

    public static function admin_business_addGallery_get(Request $request, $id = null) {
        try {
            $business = Business::findorfail($id);
            $gallery = Gallery::where('business_id', $business->id)->paginate(5);
            return View::make('Admin.Business.addgallery')->with(compact('gallery', 'business'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

}
