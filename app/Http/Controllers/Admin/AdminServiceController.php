<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
 
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use Illuminate\Validation\Rule;
use View;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Controllers\CommonController;
use App\Models\Category; 
use App\Models\Service; 
use App\Models\Gallery; 

class AdminServiceController extends Controller
{
  ///////////////////////////////////////////// All Services     ///////////////////////////////////// 
    public static function admin_service_all(Request $request) { 
        try {
            $admin = \App('admin'); 
            $services = Service::with('categories')->paginate(10); 
            return View::make('Admin.Service.allservices')->with('services', $services);
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
     public static function admin_service_add_get(Request $request) {
        try {
            $maincategories = Category::all(); //
            return View::make('Admin.Service.addservice')->with(compact('maincategories'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    ///////////////////////// 		Add Category Post     ///////// 
    public static function admin_service_add_post(Request $request) { 
        try {
            $rules = array( 
                's_name'    => 'required',
                's_img'     => 'image',
                'cat_id'    => 'required'
            );
            $msg=array(
                's_name.required'       => 'Service name is required.',
                's_img.image'           => 'Service image should be a valid image.',
                'cat_id.required'       => 'You have to choose one category.'
            );
            $validator = Validator::make($request->all(), $rules,$msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all();
            if (!empty($request->s_img)) {
                $filename = CommonController::image_uploader(Input::file('s_img'));

                if (empty($filename))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");

                $data['s_img'] = $filename;
            }
            
            /**If features added */
             if (count($request->features) > 0) {
                foreach ($request->features as $key=>$val) {
                foreach ($val as $key2=>$val2) {
                    if (!empty($val2))
                        $featuredata[$key2] = $val2;
                    }
                }
                 $data['features'] = json_encode($featuredata);
            }
            /** features added end*/             
            $request['service_id'] = Service::add_new_service($data); 
            return Redirect::back()->with('status', 'Service added successfully.');             
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
    
    public static function admin_service_edit(Request $request, $id = null) {
        try {
            $maincategories = Category::where('status', 'active')->get(); //
            $service = Service::findorfail($id);
            $check_newfeature=  DB::table('cat_features')->where('cat_id', $service->cat_id)->get();
          
            return View::make('Admin.Service.addservice')->with(compact('service', 'maincategories', 'check_newfeature'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
    
    public static function admin_service_update(Request $request){
         try {
              $rules = array( 
                's_name'    => 'required',
                's_img'     => 'image',
                'cat_id'    => 'required'
            );
            $msg=array(
                's_name.required'       => 'Service name is required.',
                's_img.image'           => 'Service image should be a valid image.',
                'cat_id.required'       => 'You have to choose one category.'
            );
            $validator = Validator::make($request->all(), $rules,$msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data       =   $request->all();
            $service    =   Service::findorfail($request->id);

            if (!empty($request['s_img'])) {
                $data['s_img'] = CommonController::image_uploader(Input::file('s_img'));
                if (empty($request['s_img']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            } else
                $data['s_img'] = $service->s_img;
            
            /**If features added */
             if (count($request->features) > 0) {
                foreach ($request->features as $key=>$val) {
                foreach ($val as $key2=>$val2) {
                    if (!empty($val2))
                        $featuredata[$key2] = $val2;
                    }
                }
                if (isset($featuredata) && count($featuredata) > 0)
                     $data['features'] = json_encode($featuredata);
                 else
                     $data['features'] = '';
            }
            /** features added end*/ 
            
            $request['id'] = Service::update_service($data);  
            return Redirect::back()->with('status', 'Service updated successfully.');
             
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
////////////////////Service Block Unblock 	////////////////////////////////////////////////////
    public static function admin_service_block(Request $request, $id = null, $status = null) {
        try {
            $service = Service::findorfail($id);

            if ($status == 1) {

                if ($service->status == 'block')
                    return Redirect::back()->withErrors('This Service is already Blocked')->withInput();

                DB::Table('services')->where('id', $service->id)->update(array(
                    'status' => 'block',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Service Blocked Successfully');
            }
            else {
                if ($service->status == 'active')
                    return Redirect::back()->withErrors('This Service is already Unblocked')->withInput();

                DB::Table('services')->where('id', $service->id)->update(array(
                    'status' => 'active',
                    'updated_at' => new \DateTime
                ));

                return Redirect::back()->with('status', 'Service Unblocked Successfully');
            }
        } catch (\Exception $e) {
              return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
    
    public static function admin_service_delete(Request $request, $id = null) {
        try {
            $service = Service::findorfail($id);
            $service->delete();
             
            return Redirect::back()->with('status', 'Service Successfully Deleted.');
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    
    
    
    public static function admin_service_addGallery_post(Request $request, $id = null) {
    try {
            $rules = array(
                'name' => 'required|image',                 
                'service_id' => 'required'
            );
            $msg = array(
                'name.required' => 'Gallery Image is required.',
                'name.image' => 'Gallery Image should be a valid image.',
                'service_id.required' => 'Something wrong, please retry after page refresh!'
            );
            $validator = Validator::make($request->all(), $rules, $msg);
            if ($validator->fails())
                return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();
            $data = $request->all(); 
            if (!empty($request['name'])) {
                $data['name'] = CommonController::image_uploader(Input::file('name'));
                if (empty($request['name']))
                    return Redirect::back()->witherrors("Image Not Uploaded please try again");
            }            
            $request['id'] = Gallery::add_new_gallery_image($data);
            return Redirect::back()->with('status', 'Image added successfully.'); 
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }
    public static function admin_service_addGallery_get(Request $request, $id = null) {
        try {
            $service = Service::findorfail($id);  
            $gallery = Gallery::where('service_id',$service->id)->paginate(5);  
            return View::make('Admin.Service.addgallery')->with(compact('gallery','service'));
        } catch (\Exception $e) {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    } 

}
