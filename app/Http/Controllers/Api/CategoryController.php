<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Controllers\Api\HomeController;
use Redirect;
use DB;

class CategoryController extends Controller
{
    
//////////////////////////////		Get Main Categories 	/////////// 
    /**
     * 
     *
     * 	 @SWG\Get(
     *     path="/get_main_cat",
     *     tags={"Category Mangement"},
     *     consumes={"multipart/form-data"},
     *     description="Main Categories",
     *     
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function get_main_cat(Request $request) {
        try {  
            $categories = Category::where('status','active')
                                    ->where('parent_id','0')
                                   // ->select('id','cat_image','name','name_ar','sort_order')
                                    ->select('id', 'name','name_ar','sort_order',DB::RAW("(COALESCE(cat_image,'')) as cat_image"), DB::RAW("(CASE WHEN cat_image IS NULL THEN '' WHEN cat_image LIKE 'http%' THEN cat_image ELSE CONCAT('" . env('IMAGE_URL') . "',cat_image) END) as cat_image_url"))
                                    ->orderBy('sort_order', 'asc') 
                                    ->orderBy('name', 'desc')
                                    ->get();
           
            $versions = \Config::get('app.app_versions'); 
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Categories lisitng.'), 'result' => ['categories' => $categories, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

}
