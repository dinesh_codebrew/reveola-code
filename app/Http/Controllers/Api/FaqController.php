<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\User;
use App\Http\Controllers\Api\HomeController;
use Redirect;

class FaqController extends Controller
{
    //////////////////////////////	All Faqs /////////////////////////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/get_all_faqs",
     *     tags={"All Faqs"},
     *     consumes={"multipart/form-data"},
     *     description="Review of the day",
     *     @SWG\Parameter(
     *     		name="b_id",
     *     		in="formData",
     *     		description="Business ID",
     *     		required=false,
     *     		type="string"
    *     ),
    *     @SWG\Parameter(
    *     		name="p_id",
    *     		in="formData",
    *     		description="Produnct ID",
    *     		required=false,
    *     		type="string"
    *     ), 
    *     @SWG\Parameter(
    *     		name="s_id",
    *     		in="formData",
    *     		description="Service ID",
    *     		required=false,
    *     		type="string"
    *   	),
     *    @SWG\Parameter(
     *     		name="pageno",
     *     		in="formData",
     *     		description="Page No.",
     *     		required=false,
     *     		type="string"
     *     ), 
     *    @SWG\Parameter(
     *     		name="pageoffset",
     *     		in="formData",
     *     		description="Offset",
     *     		required=false,
     *     		type="string"
     *      ), 
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
      public static function get_all_faqs(Request $request) {
       try { 
            $data['b_id'] = $request->b_id;
            $data['p_id'] = $request->p_id;
            $data['s_id'] = $request->s_id; 
            if (isset($request->pageno)) {
                $data['page'] = $request->pageno;
                $data['offset'] = $request->pageoffset;
            }
            $faqs = Faq::get_all_faq($data);

            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Al Faqs.'), 'result' => ['faqs' => $faqs, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }
     //////////////////////////////	Faq details  /////////////////////////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/faq_details",
     *     tags={"All Faqs"},
     *     consumes={"multipart/form-data"},
     *     description="Get Details of Faq",
     *     @SWG\Parameter(
     *     		name="faq_id",
     *     		in="formData",
     *     		description="FAQ ID",
     *     		required=true,
     *     		type="string"
    *     ) ,
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
      public static function faq_details(Request $request) {
       try {  
            $faq = Faq::faq_details($request->faq_id);
            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Faq details'), 'result' => ['faq' => $faq, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }
}
