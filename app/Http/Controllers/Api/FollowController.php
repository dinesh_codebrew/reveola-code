<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,
    Hash,
    DateTime,
    Mail,
    DB;
use Redirect;
//use App\Models\Admin; 
use App\Models\Follower;

class FollowController extends Controller {

    /////////////// /////	Follow Business,User, Product,Service	/////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/follow",
     *     tags={"Business APIs"},
     *     consumes={"multipart/form-data"},
     *     description="Follow/Unfollow", 
     *      @SWG\Parameter(
     *     		name="follow",
     *     		in="formData",
     *     		description="yes / no",
     *     		required=false,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="b_id",
     *     		in="formData",
     *     		description="Business ID",
     *     		required=false,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="p_id",
     *     		in="formData",
     *     		description="Product ID",
     *     		required=false,
     *     		type="string"
     *   	), 
     *   	@SWG\Parameter(
     *     		name="s_id",
     *     		in="formData",
     *     		description="Service ID",
     *     		required=false,
     *     		type="string"
     *   	),
     * *   	@SWG\Parameter(
     *     		name="u_id",
     *     		in="formData",
     *     		description="User ID",
     *     		required=false,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="access_email",
     *     		in="formData",
     *     		description="Access Email",
     *     		required=false,
     *     		type="string"
     *   	), 
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function get_follow(Request $request) {
        try {
            $data['b_id'] = $request->b_id;
            $data['p_id'] = $request->p_id;
            $data['s_id'] = $request->s_id;
            $data['u_id'] = $request->u_id;
            $data['user_id'] = \Request::get('user_id');
            if ($request->follow=='yes') {
							Follower::remove_follow($data);
                $follow = 	Follower::add_follow($data);
                $msg = __('Following Successfully.');
            } else {
                $follow = Follower::remove_follow($data);
                $msg = __('Un-Follow Successfully.');
            } 
            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => $msg, 'result' => ['versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

}
