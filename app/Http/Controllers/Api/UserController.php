<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator,
    Hash,
    DateTime,
    Mail,
    DB,
    Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\SocialAccount;
use App\Http\Controllers\Api\HomeController;
use Redirect;

class UserController extends Controller {

//////////////////////////////		User Register 		/////////////////////////////////////////////
    /**
     *
     * User Register
     *
     * 	 @SWG\Post(
     *     path="/app_register",
     *     tags={"User Register & Login Section"},
     *     consumes={"multipart/form-data"},
     *     description="User Registeration",
     *   	@SWG\Parameter(
     *     		name="name",
     *     		in="formData",
     *     		description="Full Name",
     *     		required=true,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="email",
     *     		in="formData",
     *     		description="Email",
     *     		required=true,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="password",
     *     		in="formData",
     *     		description="Password",
     *     		required=true,
     *     		type="string"
     *   	),
     *          @SWG\Parameter(
     *     		name="timezone",
     *     		in="formData",
     *     		description="TimeZone Eg - Asia/Kolkata",
     *     		required=true,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="latitude",
     *     		in="formData",
     *     		description="Latitude",
     *     		required=false,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="longitude",
     *     		in="formData",
     *     		description="Longitude",
     *     		required=false,
     *     		type="string"
     *   	),   	
     *   	@SWG\Parameter(
     *     		name="profile_pic",
     *     		in="formData",
     *     		description="Profile Picture",
     *     		required=false,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="fcm_id",
     *     		in="formData",
     *     		description="FCM ID for push notifications",
     *     		required=false,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="lang",
     *     		in="formData",
     *     		description="User Language Eg - en or ar",
     *     		required=false,
     *     		type="string"
     *   	),
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error")
     * )
     *
     * User SignUp
     *
     * @return \Illuminate\Http\Response
     */
    public static function app_register(Request $request) {
        try {

            $validation = Validator::make($request->all(), [
                        'name' => 'bail|required',
                        'email' => 'bail|required|email|unique:users,email',
                        //    'phone' => 'bail|required|unique:users',
                        'password' => 'bail|required|min:6',
                        'timezone' => 'bail|required|timezone'
                            ], [
                        'email.unique' => __('signup.email_exist')
                            ]
            );
            if ($validation->fails()) {
                return response(array('success' => 0, 'statuscode' => 400, 'msg' =>
                    $validation->getMessageBag()->first()), 400);
            }

            $data = $request->all();
            $data['password'] = trim($request['password'], '"');
            $id = User::create_new_user($data);
            $data['user_type'] = 'New';
            $data['user_id'] = $id;
            //$user = User::get_user_profile($request->all()); 
            //$userData = User::where('id', $id)->first(); 
            $userData = User::get_user_profile($data);
            $versions = \Config::get('app.app_versions');
            Auth::loginUsingId($userData->id);
            //$user = Auth::user();
            $userData->token = $userData->createToken('reveola')->accessToken;
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('signup.signup_success'), 'result' => ['user' => $userData, 'versions' => $versions]], 200);
        } catch (\Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

    //////////////////////////////      User Register       /////////////////////////////////////////////
    /**
     *
     * User Social Login
     *
     *   @SWG\Post(
     *     path="/app_social_login",
     *     tags={"User Social Login Section"},
     *     consumes={"multipart/form-data"},
     *     description="User Social Login",
     *      @SWG\Parameter(
     *          name="name",
     *          in="formData",
     *          description="Full Name",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="Email",
     *          required=false,
     *          type="string"
     *      ),  
      @SWG\Parameter(
     *          name="provider_user_id",
     *          in="formData",
     *          description="Socail Media Unique ID",
     *          required=true,
     *          type="string"
     *      ),
      @SWG\Parameter(
     *          name="provider",
     *          in="formData",
     *          description="Social Media Name like twitter, facebook",
     *          required=true,
     *          type="string"
     *      ),
      @SWG\Parameter(
     *          name="timezone",
     *          in="formData",
     *          description="TimeZone Eg - Asia/Kolkata",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="latitude",
     *          in="formData",
     *          description="Latitude",
     *          required=false,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="longitude",
     *          in="formData",
     *          description="Longitude",
     *          required=false,
     *          type="string"
     *      ),      
     *      @SWG\Parameter(
     *          name="profile_pic",
     *          in="formData",
     *          description="Profile Picture",
     *          required=false,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="fcm_id",
     *          in="formData",
     *          description="FCM ID for push notifications",
     *          required=false,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          in="formData",
     *          description="User Language Eg - en or ar",
     *          required=false,
     *          type="string"
     *      ),     
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error")
     * )
     *
     * User SignUp
     *
     * @return \Illuminate\Http\Response
     */
    public static function app_social_login(Request $request) {
        try {
            $versions = \Config::get('app.app_versions');
				/* Checking for previous records start */
            $extstingUser = $extstingSocial = [];
            if (isset($request->email) && !empty($request->email)) {
                $extstingUser = User::with('social_accounts')
                        ->where('email', $request->email)
                        ->get();
            } else {
                $extstingSocial = SocialAccount::with('user')
                        ->where('provider_user_id', $request->provider_user_id)
                        ->where('provider', $request->provider)
                        ->get();
            }

            if (count($extstingUser) > 0 && count($extstingUser[0]->social_accounts) > 0) {
                $social_accounts = $extstingUser[0]->social_accounts;
                SocialAccount::firstOrCreate(
                        ['provider_user_id' => $request->provider_user_id, 'provider' => $request->provider, 'user_id' => $extstingUser[0]->id]
                );
                //    $userData = User::where('id', $extstingUser[0]->id)->first();
                $data['user_id'] = $extstingUser[0]->id;
                $userData = User::get_user_profile($data);
                Auth::loginUsingId($userData->id);
                // $user = Auth::user();
                $userData->token = $userData->createToken('reveola')->accessToken;
                return response(['success' => 1, 'statuscode' => 200, 'msg' => __('signup.register_user_msg'), 'result' => ['user' => $userData, 'versions' => $versions]], 200);
            } else if (count($extstingSocial) > 0 && count($extstingSocial[0]->user) > 0) {
                // $userData = User::where('id', $extstingSocial[0]->user->id)->first();
                $data['user_id'] = $extstingSocial[0]->user->id;
                $userData = User::get_user_profile($data);
                Auth::loginUsingId($userData->id);
                // $user = Auth::user();
                $userData->token = $userData->createToken('reveola')->accessToken;
                return response(['success' => 1, 'statuscode' => 200, 'msg' => __('signup.register_user_msg'), 'result' => ['user' => $userData, 'versions' => $versions]], 200);
            }

            /* Checking for previous records end */


            $validation = Validator::make($request->all(), [
                        'name' => 'bail|required',
                        'provider_user_id' => 'bail|required|unique:social_accounts,provider_user_id',
                        'provider' => 'bail|required',
                        'email' => 'bail|email|unique:users,email',
                        'timezone' => 'bail|required|timezone'
                            ], [
                        'email.unique' => __('signup.email_exist'),
                        'provider.required' => __('Provider_required'),
                        'provider_user_id.required' => __('Provider_user_required'),
                        'provider_user_id.unique' => __('Provider_unique')
                            ]
            );
            if ($validation->fails()) {
                return response(array('success' => 0, 'statuscode' => 400, 'msg' =>
                    $validation->getMessageBag()->first()), 400);
            }

            $data = $request->all(); 
            $id = User::create_new_user($data);
            //$request['user_type'] = 'New';
            $data['user_id'] = $id;
            $socialData = SocialAccount::add_new_login_type($data);
            //$user = User::get_user_profile($request->all()); 
            //$userData = User::where('id', $id)->first(); 
            $userData = User::get_user_profile($data);
            Auth::loginUsingId($userData->id);
            //$user = Auth::user();

            $userData->token = $userData->createToken('reveola')->accessToken;
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('signup.signup_success'), 'result' => ['user' => $userData, 'versions' => $versions]], 200);
        } catch (\Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

    /**
     *
     * User Login
     *
     *   @SWG\Post(
     *     path="/app_login",
     *     tags={"User Register & Login Section"},
     *     consumes={"multipart/form-data"},
     *     description="User Login",
     *      @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="Email",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          description="Password",
     *          required=true,
     *          type="string"
     *      ), 
     *      @SWG\Parameter(
     *          name="fcm_id",
     *          in="formData",
     *          description="FCM ID for Push Notifications",
     *          required=false,
     *          type="string"
     *      ), @SWG\Parameter(
     *          name="timezone",
     *          in="formData",
     *          description="TimeZone Eg - Asia/Kolkata",
     *          required=true,
     *          type="string"
     *      ), @SWG\Parameter(
     *          name="lang",
     *          in="formData",
     *          description="User Language Eg - en or ar",
     *          required=false,
     *          type="string"
     *      ),     
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error")
     * )
     *
     * User SignUp
     *
     * @return \Illuminate\Http\Response
     */
    public function app_login() {
        $versions = \Config::get('app.app_versions');
        if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'user_type' => 'user'])) {
            //$user = Auth::user();
            $data['user_id'] = Auth::user()->id;
            $user = User::get_user_profile($data);
            if (!empty(request('timezone'))) {
                $data['fcm_id'] = !empty(request('fcm_id')) ? request('fcm_id') : '';
                $data['timezone'] = request('timezone');
                User::update_user_fcm($data, $user);
            }
            $versions = \Config::get('app.app_versions');
            $user->token = $user->createToken('reveola')->accessToken;
            return response(['success' => 1, 'statuscode' => 200,
                'msg' => __('signup.Login_success'), 'result' => ['user' => $user,
                    'versions' => $versions]], 200);
        } else {
            $getUser_withphone = User::where('phone', request('email'))
                    ->where('user_type', 'user')
                    ->first();
            if (count($getUser_withphone) > 0 && Hash::check(request('password'), $getUser_withphone->password)) {
                Auth::loginUsingId($getUser_withphone->id);
                //$user = Auth::user();
                $data['user_id'] = $getUser_withphone->id;
                $user = User::get_user_profile($data);
                if (!empty(request('timezone'))) {
                    $data['fcm_id'] = !empty(request('fcm_id')) ? request('fcm_id') : '';
                    $data['timezone'] = request('timezone');
                    User::update_user_fcm($data, $user);
                    Auth::user()->fcm_id = $data['fcm_id'];
                    Auth::user()->timezone = $data['timezone'];
                }

                $user->token = $user->createToken('reveola')->accessToken;
                return response(['success' => 1, 'statuscode' => 200,
                    'msg' => __('signup.Login_success'), 'result' => ['user' => $user,
                        'versions' => $versions]], 200);
            }
            return response(['success' => 0, 'statuscode' => 500, 'msg' => __('signup.Login_fail')], 500);
        }
    }

//////////////////////////////		Password Update 	////////////////////////////////////////////////
    /**
     *
     * User Password Update
     *
     * 	 @SWG\Post(
     *     path="/password_update",
     *     tags={"User Register & Login Section"},
     *     consumes={"multipart/form-data"},
     *     description="User Password Update",
     *     security={
     *         {
     *             "default": "Bearer "
     *         }
     *     },
     *   	@SWG\Parameter(
     *     		name="old_password",
     *     		in="formData",
     *     		description="Old Password",
     *     		required=true,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="new_password",
     *     		in="formData",
     *     		description="New Password",
     *     		required=true,
     *     		type="string"
     *   	),
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Password Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function password_update(Request $request) {
        try {

            $validation = Validator::make($request->all(), [
                        'old_password' => 'required',
                        'new_password' => 'required|min:6|different:old_password',
                            ]
            );
            if ($validation->fails())
                return Response(array('success' => 0, 'statuscode' => 400, 'msg' => $validation->getMessageBag()->first()), 400);

            $user = Auth::user();

            $request['old_password'] = trim($request['old_password'], '"');
            $request['new_password'] = trim($request['new_password'], '"');

            if (!Hash::check($request['old_password'], $user->password))
                return Response(array('success' => 0, 'statuscode' => 400, 'msg' => __('signup.oldPassword_notMatch')), 400);

            Auth::user()->password = $request['new_password'] = bcrypt($request['new_password']);

            User::update_user_password($request->all());

            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('signup.Password_update')], 200);
        } catch (\Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

//////////////////////////////		Edit Profile 	//////////////////////////////////////////////
    /**
     *
     * User Profile Update
     *
     * 	 @SWG\Post(
     *     path="/edit_profile",
     *     tags={"User Register & Login Section"},
     *     consumes={"multipart/form-data"},
     *     description="User Profile Update",
     *     security={
     *         {
     *             "default": "Bearer "
     *         }
     *     },
     *   	@SWG\Parameter(
     *     		name="name",
     *     		in="formData",
     *     		description="Full Name",
     *     		required=true,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="email",
     *     		in="formData",
     *     		description="Email",
     *     		required=false,
     *     		type="string"
     *   	),@SWG\Parameter(
     *     		name="phone",
     *     		in="formData",
     *     		description="Phone",
     *     		required=false,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="profile_pic",
     *     		in="formData",
     *     		description="Profile Pic",
     *     		required=false,
     *     		type="string"
     *   	),
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function edit_profile(Request $request) {
        try {
            $user = Auth::user();
            $validation = Validator::make($request->all(), [
                        'name' => 'bail|required',
                        'email' => 'bail|unique:users,email,' . $user->id . ',id',
                        'phone' => 'bail|numeric|unique:users,phone,' . $user->id . ',id',
                      //  'profile_pic' => 'image'
                            ]
            );
            if ($validation->fails())
                return Response(array('success' => 0, 'statuscode' => 400, 'msg' => $validation->getMessageBag()->first()), 400);


            $data['name'] = $request->name;
            $data['email'] = !empty($request->email) ? $request->email : $user->email;
            $data['phone'] = $request->phone;
			$data['profile_pic'] = !empty($request->profile_pic) ? $request->profile_pic : $user->profile_pic;

            User::update_profile($data, $user);

            $user = User::get_user_profile($request->all());

            Auth::user()->email = $data['email'];
            Auth::user()->name = $data['name'];
            Auth::user()->phone = $data['phone'];
            // $user = Auth::user();
            $versions = \Config::get('app.app_versions');
            $user->token = $user->createToken('reveola')->accessToken;
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('signup.Profile_update'), 'result' => ['user' => $user, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

//////////////////////////////		App Logout 		/////////////////////////////////////////////////
    /**
     *
     * User Logout
     *
     * 	 @SWG\Post(
     *     path="/app_logout",
     *     tags={"User Register & Login Section"},
     *     consumes={"multipart/form-data"},
     *     description="User Logout",
     *     security={
     *         {
     *             "default": "Bearer "
     *         }
     *     },
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Logout
     *
     * @return \Illuminate\Http\Response
     */
    public static function app_logout(Request $request) {
        try {

            $user = Auth::user();

            User::whereId($user->id)
                    ->limit(1)
                    ->update([
                        'fcm_id' => '',
                        'updated_at' => new \DateTime
            ]);

            Auth()->user()->token()->revoke();

            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('signup.logout_success')], 200);
        } catch (\Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

//////////////////////////////		Password Create 	////////////////////////////////////////////////
    /**
     *
     * User Password Create
     *
     * 	 @SWG\Post(
     *     path="/password_create",
     *     tags={"User Register & Login Section"},
     *     consumes={"multipart/form-data"},
     *     description="User Password Create",
     *     security={
     *         {
     *             "default": "Bearer "
     *         }
     *     }, 
     *   	@SWG\Parameter(
     *     		name="new_password",
     *     		in="formData",
     *     		description="New Password",
     *     		required=true,
     *     		type="string"
     *   	),
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Password Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function password_create(Request $request) {
        try {

            $validation = Validator::make($request->all(), [
                        'new_password' => 'required|min:6',
                            ]
            );
            if ($validation->fails())
                return Response(array('success' => 0, 'statuscode' => 400, 'msg' => $validation->getMessageBag()->first()), 400);

            $user = Auth::user();

            $data['user_id'] = $user->id;
            $data['new_password'] = trim($request['new_password'], '"');


            Auth::user()->password = $data['new_password'] = bcrypt($data['new_password']);

            User::create_user_password($data); 
			$userNew = User::get_user_profile($data);
			 $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('signup.password_created'), 'result' => ['user' => $userNew, 'versions' => $versions]], 200);
        } catch (\Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }
    
    
//////////////////////////////		My Profile 	//////////////////////////////////////////////
    /**
     *
     * User Profile Update
     *
     * 	 @SWG\Post(
     *     path="/my_profile",
     *     tags={"User Register & Login Section"},
     *     consumes={"multipart/form-data"},
     *     description="User Profile Update",
     *     security={
     *         {
     *             "default": "Bearer "
     *         }
     *     } ,
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function my_profile(Request $request) {
        try {
            $user = Auth::user(); 
            $user = User::get_user_profileFull($request->all()); 
            $versions = \Config::get('app.app_versions');
            $user->token = $user->createToken('reveola')->accessToken;
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('signup.Profile_update'), 'result' => ['user' => $user, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }


}
