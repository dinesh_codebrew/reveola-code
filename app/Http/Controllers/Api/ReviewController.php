<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Review;
use App\Models\User;
use App\Http\Controllers\Api\HomeController;
use Redirect;

class ReviewController extends Controller
{
   //////////////////////////////	Review of the day 	/////////// 
    /**
     * 
     *
     * 	 @SWG\Get(
     *     path="/get_review_day",
     *     tags={"Reviews"},
     *     consumes={"multipart/form-data"},
     *     description="Review of the day",
     *     
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function get_review_day(Request $request) {
        try {  
           /* $review = Review::where('status','active')
                            ->select('id','r_des','anonymous','user_id','created_at')   
                            ->first();
            $data['user_id']            =   $review->user_id;
            $user                       =   User::get_user_profile($data);*/
           // $user['followers_count']    =   $review->user->followers->count();
            //$user['reviews_count']      =   $review->user->reviews->count(); 
            $review = Review::review_of_theday();
            $versions = \Config::get('app.app_versions'); 
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Review of the day.'), 'result' => ['review' => $review, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }
    //////////////////////////////	All Review /////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/get_all_reviews",
     *     tags={"Reviews"},
     *     consumes={"multipart/form-data"},
     *     description="Review of the day",
     *     @SWG\Parameter(
     *     		name="b_id",
     *     		in="formData",
     *     		description="Business ID",
     *     		required=false,
     *     		type="string"
    *     ),
    *     @SWG\Parameter(
    *     		name="p_id",
    *     		in="formData",
    *     		description="Produnct ID",
    *     		required=false,
    *     		type="string"
    *     ), 
    *     @SWG\Parameter(
    *     		name="s_id",
    *     		in="formData",
    *     		description="Service ID",
    *     		required=false,
    *     		type="string"
    *   	), 
     *   @SWG\Parameter(
     *     		name="access_email",
     *     		in="formData",
     *     		description="Access Email",
     *     		required=false,
     *     		type="string"
     *   	), 
     *    @SWG\Parameter(
     *     		name="pageno",
     *     		in="formData",
     *     		description="Page No.",
     *     		required=false,
     *     		type="string"
     *     ), 
     *    @SWG\Parameter(
     *     		name="pageoffset",
     *     		in="formData",
     *     		description="Offset",
     *     		required=false,
     *     		type="string"
     *      ), 
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
      public static function get_all_reviews(Request $request) {
       try { 
            $data['b_id'] = $request->b_id;
            $data['p_id'] = $request->p_id;
            $data['s_id'] = $request->s_id;
            $data['user_id'] = \Request::get('user_id');
            if (isset($request->pageno)) {
                $data['page'] = $request->pageno;
                $data['offset'] = $request->pageoffset;
            }
            $reviews = Review::get_all_reviews($data);

            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Al Reviews.'), 'result' => ['reviews' => $reviews, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }
}
