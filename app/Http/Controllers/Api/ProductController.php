<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,
    Hash,
    DateTime,
    Mail,
    DB;
use App\Models\Business;
use App\Models\Product;
use App\Models\User;
use App\Http\Controllers\Api\HomeController;
use Redirect;
class ProductController extends Controller
{
   //////////////////////////////	get Product Details /////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/get_product_details",
     *     tags={"Product APIs"},
     *     consumes={"multipart/form-data"},
     *     description="Product details",
     * *     @SWG\Parameter(
     *     		name="product_id",
     *     		in="formData",
     *     		description="Product ID",
     *     		required=true,
     *     		type="number"
     *      ),
     *     @SWG\Parameter(
     *     		name="access_email",
     *     		in="formData",
     *     		description="Access Email",
     *     		required=false,
     *     		type="string"
     *      ),
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public static function get_product_details(Request $request) {
        try { 
            $data['p_id'] = $request->product_id;
            $data['user_id'] = \Request::get('user_id');
            $product = Product::get_product_details($data);
              
            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Product Details.'), 'result' => [ 'product' => $product, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    } 
    
    
    
}
