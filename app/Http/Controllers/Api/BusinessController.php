<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,
    Hash,
    DateTime,
    Mail,
    DB;
use App\Models\Business;
use App\Models\User;
use App\Http\Controllers\Api\HomeController;
use Redirect;

class BusinessController extends Controller {

    //////////////////////////////	Near By Business for Home	/////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/get_nearByBusiness_home",
     *     tags={"Business APIs"},
     *     consumes={"multipart/form-data"},
     *     description="Near By Business", 
     *   	@SWG\Parameter(
     *     		name="b_lat",
     *     		in="formData",
     *     		description="Latitude",
     *     		required=true,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="b_lng",
     *     		in="formData",
     *     		description="Longitude",
     *     		required=true,
     *     		type="string"
     *   	), 
     *   	@SWG\Parameter(
     *     		name="access_email",
     *     		in="formData",
     *     		description="Access Email",
     *     		required=false,
     *     		type="string"
     *   	), 
     * * @SWG\Parameter(
     *     		name="pageno",
     *     		in="formData",
     *     		description="Page No.",
     *     		required=false,
     *     		type="string"
     *      ), 
     * @SWG\Parameter(
     *     		name="pageoffset",
     *     		in="formData",
     *     		description="Offset",
     *     		required=false,
     *     		type="string"
     *      ), 
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function get_nearByBusiness_home(Request $request) {
        try {
            $validation = Validator::make($request->all(), [
                        'b_lat' => 'bail|required',
                        'b_lng' => 'bail|required'
                            ]
            );
            if ($validation->fails()) {
                return response(array('success' => 0, 'statuscode' => 400, 'msg' =>
                    $validation->getMessageBag()->first()), 400);
            }

            $data['circle_radius'] = 3959;
            $data['max_distance'] = 20;
            $data['lat'] = $request->b_lat;
            $data['lng'] = $request->b_lng;
            $data['user_id'] = \Request::get('user_id');
             if (isset($request->pageno)) {
                $data['page'] = $request->pageno;
                $data['offset'] = $request->pageoffset;
            }
            $business = Business::get_nearBusiness($data);

            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Near By business for home.'), 'result' => ['business' => $business, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

    //////////////////////////////	Near By Business for Home	/////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/get_nearByBusiness_home_search",
     *     tags={"Business APIs"},
     *     consumes={"multipart/form-data"},
     *     description="Search Near By Business", 
     *   	@SWG\Parameter(
     *     		name="b_lat",
     *     		in="formData",
     *     		description="Latitude",
     *     		required=true,
     *     		type="string"
     *   	),
     *   	@SWG\Parameter(
     *     		name="b_lng",
     *     		in="formData",
     *     		description="Longitude",
     *     		required=true,
     *     		type="string"
     *   	), 
     *          @SWG\Parameter(
     *     		name="b_search",
     *     		in="formData",
     *     		description="Search",
     *     		required=true,
     *     		type="string"
     *   	), 
     *   	@SWG\Parameter(
     *     		name="access_email",
     *     		in="formData",
     *     		description="Access Email",
     *     		required=false,
     *     		type="string"
     *   	), 
     * * @SWG\Parameter(
     *     		name="pageno",
     *     		in="formData",
     *     		description="Page No.",
     *     		required=false,
     *     		type="string"
     *      ), 
     * @SWG\Parameter(
     *     		name="pageoffset",
     *     		in="formData",
     *     		description="Offset",
     *     		required=false,
     *     		type="string"
     *      ), 
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function get_nearByBusiness_home_search(Request $request) {
        try {
            $validation = Validator::make($request->all(), [
                        'b_lat' => 'bail|required',
                        'b_lng' => 'bail|required',
                        'b_search' => 'bail|required',
                            ]
            );
            if ($validation->fails()) {
                return response(array('success' => 0, 'statuscode' => 400, 'msg' =>
                    $validation->getMessageBag()->first()), 400);
            }

            $data['circle_radius'] = 3959;
            $data['max_distance'] = 20;
            $data['lat'] = $request->b_lat;
            $data['lng'] = $request->b_lng;
            $data['search'] = $request->b_search;
            $data['user_id'] = \Request::get('user_id');
            
             if (isset($request->pageno)) {
                $data['page'] = $request->pageno;
                $data['offset'] = $request->pageoffset;
            }
            $business = Business::get_nearBusiness_search($data);

            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Near By business for home.'), 'result' => ['business' => $business, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

    
    //////////////////////////////	Home Most reviewed Businesses	/////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/most_reviewed_busineess",
     *     tags={"Business APIs"},
     *     consumes={"multipart/form-data"},
     *     description="Most Reviewed Business",
     *     @SWG\Parameter(
     *     		name="access_email",
     *     		in="formData",
     *     		description="Access Email",
     *     		required=false,
     *     		type="string"
     *      ), 
     *   * * @SWG\Parameter(
     *     		name="cat_id",
     *     		in="formData",
     *     		description="Category ID",
     *     		required=false,
     *     		type="string"
     *   	), 
     * @SWG\Parameter(
     *     		name="pageno",
     *     		in="formData",
     *     		description="Page No.",
     *     		required=false,
     *     		type="string"
     *      ), 
     * @SWG\Parameter(
     *     		name="pageoffset",
     *     		in="formData",
     *     		description="Offset",
     *     		required=false,
     *     		type="string"
     *      ), 
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function most_reviewed_busineess(Request $request) {
        try {
            if (isset($request->pageno)) {
                $data['page'] = $request->pageno;
                $data['offset'] = $request->pageoffset;
            }
            $data['cat_id'] = $request->cat_id;
            $data['user_id'] = \Request::get('user_id');
            $business = Business::get_reviewed_Business($data);
              
            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Most Reviewed Business.'), 'result' => [ 'business' => $business, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }
  //////////////////////////////	Home Most reviewed Businesses full Page	/////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/most_reviewed_busineess_search",
     *     tags={"Business APIs"},
     *     consumes={"multipart/form-data"},
     *     description="Search Most Reviewed Business",
     *     @SWG\Parameter(
     *     		name="access_email",
     *     		in="formData",
     *     		description="Access Email",
     *     		required=false,
     *     		type="string"
     *      ), 
     * @SWG\Parameter(
     *     		name="b_search",
     *     		in="formData",
     *     		description="Search",
     *     		required=false,
     *     		type="string"
     *   	), 
     * * @SWG\Parameter(
     *     		name="cat_id",
     *     		in="formData",
     *     		description="Category ID",
     *     		required=false,
     *     		type="string"
     *   	), 
     * @SWG\Parameter(
     *     		name="pageno",
     *     		in="formData",
     *     		description="Page No.",
     *     		required=false,
     *     		type="string"
     *      ), 
     * @SWG\Parameter(
     *     		name="pageoffset",
     *     		in="formData",
     *     		description="Offset",
     *     		required=false,
     *     		type="string"
     *      ), 
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function most_reviewed_busineess_search(Request $request) { 
        try {
            if (isset($request->pageno)) {
                $data['page'] = $request->pageno;
                $data['offset'] = $request->pageoffset;
            }
            $data['search'] = $request->b_search;
            $data['cat_id'] = $request->cat_id;
            $data['user_id'] = \Request::get('user_id');
            $business = Business::get_reviewed_Business_search($data); 
            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Most Reviewed Business.'), 'result' => [ 'business' => $business, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

    
    //////////////////////////////	get Businesses	Details /////////// 
    /**
     * 
     *
     * 	 @SWG\Post(
     *     path="/get_business_details",
     *     tags={"Business APIs"},
     *     consumes={"multipart/form-data"},
     *     description="Business details",
     * *     @SWG\Parameter(
     *     		name="business_id",
     *     		in="formData",
     *     		description="Business ID",
     *     		required=true,
     *     		type="number"
     *      ),
     *     @SWG\Parameter(
     *     		name="access_email",
     *     		in="formData",
     *     		description="Access Email",
     *     		required=false,
     *     		type="string"
     *      ),
     *     @SWG\Response(response=200, description="Success"),
     *     @SWG\Response(response=400, description="Validation Error"),
     *     @SWG\Response(response=500, description="Api Error"),
     *     @SWG\Response(response=401, description="Unauthorized")
     * )
     *
     * User Profile Update
     *
     * @return \Illuminate\Http\Response
     */
    public static function get_business_details(Request $request) {
        try { 
            $data['b_id'] = $request->business_id;
            $data['user_id'] = \Request::get('user_id');
            $business = Business::get_business_details($data);
              
            $versions = \Config::get('app.app_versions');
            return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Business Details.'), 'result' => [ 'business' => $business, 'versions' => $versions]], 200);
        } catch (Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    } 
    
    
    
}
 
