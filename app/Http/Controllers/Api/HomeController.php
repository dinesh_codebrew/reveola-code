<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator, DB  ; 
 

//use App\Models\Admin; 
use App\Models\User; 

class HomeController extends Controller
{

///////////////////////////		Api Versionng Function 		//////////////////////////////////////////////////

	public static function versioning_api(Request $request)
		{
		try{
				$rules = [
							'version'=> 'required'
						 ];

				$validation = Validator::make($request->all(),$rules);
					if($validation->fails())
						return Response(array('success'=>0, 'statuscode'=>400, 'msg'=>$validation->getMessageBag()->first()),400);

				$versioning = HomeController::check_versioning($request->all());

				return Response(array('success'=>1, 'statuscode'=>200, 'msg'=>trans('messages.Versioning complete'), 'versioning'=> $versioning),200);

			}
		catch(\Exception $e)
			{
				return \Response(array('success'=>0,'statuscode'=>500,'msg'=>$e->getMessage()),500);
			}
		}

/////////////////////////////			Api Versioning 	/////////////////////////////////////////////////////

	public static function check_versioning($data)
		{
                         return 1;

			/*$admin = Admin::select('*')->first();

			if((!empty($admin->ios_force_version))  && ($data['version'] < $admin->ios_force_version))
				{
					return 5;
				}
			elseif((!empty($admin->ios_normal_version))  && ($data['version'] < $admin->ios_normal_version))
				{
					return 4;
				}
			else
				{
					return 1;
				}*/

		}

/////////////////////			Filter Products 		/////////////////////////////////////////////////////

public static function filter_products(Request $request)
	{
	try{

			$rules = [
						'category_sub_sub_id'=> 'sometimes|exists:category_sub_subs,id',
						'page'=> 'required',
						'offset'=> 'required'
					];

			$validation = Validator::make($request->all(),$rules);
				if($validation->fails())
					return Response(array('success'=>0, 'statuscode'=>400, 'msg'=>$validation->getMessageBag()->first()),400);

			$user = \App('user');
////////////////////////////		Categories 	/////////////////////////////////////////////////
			if(!isset($request['categories']))
				$request['categories'] = [];
			else
				$request['categories'] = json_decode($request['categories'],true);
////////////////////////////		Categories 	/////////////////////////////////////////////////

////////////////////////////		Shipping Filter 	/////////////////////////////////////////
			// if(!isset($request['shipping_paid_by']) || $request['shipping_paid_by'] == 0)
			// 	$request['shipping_paid_by'] = ['Seller', 'Buyer'];
			// elseif($request['shipping_paid_by'] == 1)
			// 	$request['shipping_paid_by'] = ['Seller'];
			// else
			// 	$request['shipping_paid_by'] = ['Buyer'];
////////////////////////////		Shipping Filter 	/////////////////////////////////////////

////////////////////////////		Product Condition 	/////////////////////////////////////////
			if(!isset($request['pcondition']))
				$request['pcondition'] = [];
			else
				$request['pcondition'] = json_decode($request['pcondition'],true);
////////////////////////////		Product Condition 	/////////////////////////////////////////

////////////////////////////		Filters 	/////////////////////////////////////////////////
			if(!isset($request['filters']))
				$request['filters'] = [];
			else
				$request['filters'] = json_decode($request['filters'],true);

			$request['filters'] = HomeController::filter_where_creator($request->all());
////////////////////////////		Filters 	/////////////////////////////////////////////////


			if(!isset($request['min_price']))
				$request['min_price'] = 0;

			if(!isset($request['max_price']))
				$request['max_price'] = 99999999999;
// -Best match
// -Newest
// -Lowest Price
// -Highest Price
// -Most views
////////////////////////////////////			Sorting 			/////////////////////////////
			if(!isset($request['sort_by']) || $request['sort_by'] == 0 || $request['sort_by'] == 1)
				{//////////			Best Match 		///////////////
					$request['sort_column'] = 'id';
					$request['sort_order'] = 'DESC';
				}//////////			Best Match 		///////////////
			elseif($request['sort_by'] == 2)
				{//////////			Lowest Price First 		///////
					$request['sort_column'] = 'base_price';
					$request['sort_order'] = 'ASC';
				}//////////			Lowest Price First 		///////
			elseif($request['sort_by'] == 3)
				{//////////			Highest Price First 		///
					$request['sort_column'] = 'base_price';
					$request['sort_order'] = 'DESC';
				}//////////			Highest Price First 		///
			else
				{//////////			Most Views 				///////
					$request['sort_column'] = 'product_views';
					$request['sort_order'] = 'DESC';
				}//////////			Most Views 				///////
////////////////////////////////////			Sorting 			/////////////////////////////

			$products = Product::filter_product_listings($request->all());

			if(isset($request['search']) && $request['search'] != '')
				UserSearch::insert_new_search($request->all());

			$data = [
				'products'=> $products
					];

			return response(['success'=>1, 'statuscode'=>200, 'msg'=>trans('messages.Filtered Products'), 'data'=>$data,'request'=>$request->all()],200);

		}
	catch(\Exception $e)
		{
			return \Response(array('success'=>0,'statuscode'=>500,'msg'=>$e->getMessage()),500);
		}
	}

/////////////////////			Filter Creator 		/////////////////////////////////////////
public static function filter_where_creator($data)
	{
		$where = array();

		foreach($data['filters'] as $filter)
			{
				if( isset($filter['type']) && isset($filter['category_filter_id']) )
					{

					if($filter['type'] == 'List' && isset($filter['ids']))
						{//////////	List Type 	/////////////////

						$filter['ids'] = json_decode($filter['ids'], true);
							if(!empty($filter['ids']))
								{

									$where[] = '( (SELECT category_filter_values_id FROM product_filter_values as val WHERE val.product_id = products.id AND val.deleted="0" AND val.category_filter_id = "'.$filter['category_filter_id'].'" LIMIT 0,1) IN ("'.implode(',',$filter['ids']).'")  )';

								}

						}//////////	List Type 	/////////////////
					elseif( $filter['type'] == 'MinMax' && isset($filter['min_value']) && $filter['min_value'] != '' && isset($filter['max_value']) && $filter['max_value'] != '' )
						{//////////	MinMax 	/////////////////////

							$where[] = '( (SELECT value FROM product_filter_values as val WHERE val.product_id = products.id AND val.deleted="0" AND val.category_filter_id = "'.$filter['category_filter_id'].'" LIMIT 0,1) BETWEEN "'.($filter['min_value'] - 1).'" AND "'.($filter['max_value'] + 1).'" )';

						}//////////	MinMax 	/////////////////////
					elseif( $filter['type'] == "Value" && $filter['value'] != '' )
						{

	$where[] = '( (SELECT value FROM product_filter_values as val WHERE val.product_id = products.id AND val.deleted="0" AND val.category_filter_id = "'.$filter['category_filter_id'].'" LIMIT 0,1) LIKE "%'.$filter['value'].'%" )';

						}

					}
			}

		if(empty($where))
			return 'products.name != ""';
		else
			return '( '.implode(' OR ', $where).' )';

	}

 

}
