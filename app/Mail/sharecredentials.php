<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sharecredentials extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    /**
     * @var
     */
    protected $filename;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $filename
     */
    public function __construct($user, $filename)
    {
        $this->user = $user;
        $this->filename = $filename;
    }
     public function build()
    {
        return $this->view('Emails.APIs.share')
                    ->subject('Business Pannel Credentials')
            //      ->attach(storage_path('app/private/inbox/' . $this->filename))
                    ->with([
                        'user' => $this->user
                    ]);
    }
}