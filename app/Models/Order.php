<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Subscription;
use App\Models\User;
use DB;

class Order extends Model
{
    public function subscriptions() {
        return $this->hasMany(\App\Models\Subscription::class, 'sub_id');
    }
    public function user(){ 
       return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
    
    public static function createOrder($data){
            $order = new Order();
            $order->user_id         =   $data['user_id']; 
            $order->sub_id          =   $data['sub_id']; 
            $order->sub_name        =   $data['sub_name']; 
            $order->sub_price       =   $data['sub_price']; 
            $order->validity        =   $data['validity'];  
            $order->start_date      =   $data['start_date'];  
            $order->end_date        =   $data['end_date']; 
            
            $order->stripe_id                  =   isset($data['stripe_id'])?$data['stripe_id']:NULL;  
            $order->balance_transaction        =   isset($data['balance_transaction'])?$data['balance_transaction']:NULL;  
            $order->transaction_created        =   isset($data['transaction_created'])?$data['transaction_created']:NULL;  
            
            $order->created_at      =   new \DateTime;
            $order->updated_at      =   new \DateTime;
            $order->save();
            return $order->id;
    }
  
    public static function get_order_details($id){
        $query = Order::select('*')->where('id',$id);
        /***********Get user details start******/
            $query->with(['user'=>function($query){
                  $query->select('id','name','profile_pic','badges','email'
                , DB::RAW("(COALESCE(profile_pic,'')) as profile_pic")
                , DB::RAW("(CASE WHEN profile_pic = '' THEN ''  WHEN profile_pic IS NULL THEN '' WHEN profile_pic LIKE 'http%' THEN profile_pic ELSE CONCAT('" . env('IMAGE_URL') . "',profile_pic) END) as profile_pic_url")
                , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE user_id=users.id AND status='active') as user_total_reviews ")
                , DB::RAW("(SELECT COUNT(*) FROM followers WHERE u_id=users.id AND status='active') as user_total_followers")
                );
            }]);
        /***********Get user details end******/
        return $query->first();
    }
            
}
