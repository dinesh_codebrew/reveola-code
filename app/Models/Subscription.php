<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
     public function orders(){ 
       return $this->belongsTo(\App\Models\Order::class, 'sub_id');
    }
}
