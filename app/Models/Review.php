<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Review extends Model
{  
	public $timestamps = false;
    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
    public function business() {
        return $this->belongsTo(\App\Models\Business::class, 'b_id');
    }
    public function product() {
        return $this->belongsTo(\App\Models\Product::class, 'p_id');
    }
    public function service() {
        return $this->belongsTo(\App\Models\Service::class, 's_id');
    }
    public function gallery() {
        return $this->hasMany(\App\Models\Gallery::class, 'review_id');
    }
    public function comments() {
        return $this->hasMany(\App\Models\Comment::class, 'review_id');
    }
    public function votes() {
        return $this->hasMany(\App\Models\Vote::class, 'review_id');
    }

    ////////////////////////       Get all reviews ////////////////////////////

    public static function get_all_reviews($data) {
        $query = Review::select('id','b_id','r_des','rating','user_id','created_at'
                ,DB::Raw("(SELECT COUNT(*) FROM comments where review_id=reviews.id and status='active')as total_comments")
                ,DB::Raw("(SELECT COUNT(*) FROM votes where review_id=reviews.id and status='active')as total_votes")
                , DB::RAW("(SELECT COUNT(*) FROM votes left join users u on votes.user_id=u.id "
                        . "WHERE u.id=" . $data['user_id'] . " AND reviews.id=votes.review_id) as voted_yes")
                );  

        $query->with(['business'=>function($q){
            $q->select('id','b_name');
        }]); 
        $query->with(['product'=>function($q){
            $q->select('id','p_name');
        }]);  
        $query->with(['service'=>function($q){
            $q->select('id','s_name');
        }]);
     
        /***********Get user details start******/
        $query->with(['user'=>function($query){
                  $query->select('id','name','profile_pic','badges'
                , DB::RAW("(COALESCE(profile_pic,'')) as profile_pic")
                , DB::RAW("(CASE WHEN profile_pic IS NULL THEN '' WHEN profile_pic LIKE 'http%' THEN profile_pic ELSE CONCAT('" . env('IMAGE_URL') . "',profile_pic) END) as profile_pic_url")
                , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE user_id=users.id AND status='active') as user_total_reviews ")
                , DB::RAW("(SELECT COUNT(*) FROM followers WHERE u_id=users.id AND status='active') as user_total_followers")
                );
            $query->where('status','active');
        }]);
            /***********Get user details end******/
          /***********Get gallery images start******/
        $query->with(['gallery'=>function($query){
                  $query->select('id','name','review_id'
                , DB::RAW("(COALESCE(name,'')) as pic")
                , DB::RAW("(CASE WHEN name IS NULL THEN '' WHEN name LIKE 'http%' THEN name ELSE CONCAT('" . env('IMAGE_URL') . "',name) END) as pic_url")
                );
            $query->where('status','active');
        }]);
       /***********Get gallery images end******/    
        $query->where('status','active');

        if(isset($data['b_id']) && !empty($data['b_id']))       {
            $query->where('b_id',$data['b_id']);              
        }  
        else if(isset($data['p_id']) && !empty($data['p_id']))  {
            $query->where('p_id',$data['p_id']);               
        }
        else if(isset($data['s_id']) && !empty($data['s_id']))  {
            $query->where('s_id',$data['s_id']);              
        }
             
        $query->orderBy('id','desc'); 
        if (isset($data['page']))
            $query->skip(($data['page']-1) * $data['offset'])->take($data['offset']);
        else
            $query->skip(0)->take(4);
        return $query->get();
    }
    
     public static function review_of_theday() {
        $query = Review::select('id','b_id','r_des','rating','user_id','created_at');  

        $query->with(['business'=>function($q){
            $q->select('id','b_name');
        }]); 
        $query->with(['product'=>function($q){
            $q->select('id','p_name');
        }]);  
        $query->with(['service'=>function($q){
            $q->select('id','s_name');
        }]);
     
        /***********Get user details start******/
        $query->with(['user'=>function($query){
                  $query->select('id','name','profile_pic','badges'
                , DB::RAW("(COALESCE(profile_pic,'')) as profile_pic")
                , DB::RAW("(CASE WHEN profile_pic IS NULL THEN '' WHEN profile_pic LIKE 'http%' THEN profile_pic ELSE CONCAT('" . env('IMAGE_URL') . "',profile_pic) END) as profile_pic_url")
                , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE user_id=users.id AND status='active') as user_total_reviews ")
                , DB::RAW("(SELECT COUNT(*) FROM followers WHERE u_id=users.id AND status='active') as user_total_followers")
                );
        }]);
       /***********Get user details end******/     
        /***********Get gallery images start******/
        $query->with(['gallery'=>function($query){
                  $query->select('id','name','review_id'
                , DB::RAW("(COALESCE(name,'')) as pic")
                , DB::RAW("(CASE WHEN name IS NULL THEN '' WHEN name LIKE 'http%' THEN name ELSE CONCAT('" . env('IMAGE_URL') . "',name) END) as pic_url")
                );
        }]);
       /***********Get gallery images end******/     
        return $query->first();
    }
}
