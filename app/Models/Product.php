<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Reliese\Database\Eloquent\Model as Eloquent;
use Hash,
    DateTime,
    DB;
use Carbon\Carbon;

class Product extends Model {

    protected $fillable = ['p_name', 'p_description', 'cat_id', 'status'];

    public function categories() {
        return $this->belongsTo(\App\Models\Category::class, 'cat_id');
    }
    
    public function users() {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function gallery() {
        return $this->hasMany(\App\Models\Gallery::class, 'product_id');
    }

    public function reviews() {
        return $this->hasMany(\App\Models\Review::class, 'p_id');
    }

    public function followers() {
        return $this->hasMany(\App\Models\Follower::class, 'p_id');
    }
    
    public function faqs() {
        return $this->hasMany(\App\Models\Faq::class, 'p_id');
    }
    
    ////////////// 	Add New Product  //////////////////////////

    public static function add_new_product($data) {

        $product = new Product();
        $product->p_name = $data['p_name'];
        $product->p_description = isset($data['p_description']) ? $data['p_description'] : '';
        $product->p_img = isset($data['p_img']) ? $data['p_img'] : 'null';
        $product->cat_id = isset($data['cat_id']) ? $data['cat_id'] : '0';

        $product->features = isset($data['features']) ? $data['features'] : '';
       
        $product->user_id   = isset($data['user_id']) ? $data['user_id'] : '0';
        $product->b_id      = isset($data['b_id']) ? $data['b_id'] : NULL;
        $product->status    = isset($data['status']) ? $data['status'] : 'active';
        
        $product->created_at = new \DateTime;
        $product->updated_at = new \DateTime;

        $product->save();

        return $product->id;
    }

    //////////////////////////////		Update Product 		////////////////////////////////////////////////////////////

    public static function update_product($data) {
        Product::where('id', $data['id'])->update([
            'p_name' => $data['p_name'],
            'p_description' => isset($data['p_description']) ? $data['p_description'] : '',
            'p_img' => isset($data['p_img']) ? $data['p_img'] : 'null',
            'cat_id' => isset($data['cat_id']) ? $data['cat_id'] : '0',
            'features' => isset($data['features']) ? $data['features'] : '',
            'updated_at' => new \DateTime
        ]);
        return $data['id'];
    }
   ////////////////////////       Get business detail ////////////////////////////

    public static function get_product_details($data) {
        $product = Product::whereId($data['p_id']);
      
        $product->select('*'
                , DB::RAW("(COALESCE(p_img,'')) as p_img")
                , DB::RAW("(CASE WHEN p_img IS NULL THEN '' WHEN p_img LIKE 'http%' THEN p_img ELSE CONCAT('" . env('IMAGE_URL') . "',p_img) END) as p_img_url")
                 , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE p_id=products.id AND status='active') as total_reviews ")
                , DB::RAW("(SELECT ROUND(COALESCE(avg(rating),0),0)    FROM reviews WHERE p_id=products.id AND status='active') as smile ")
                , DB::RAW("(SELECT name FROM categories c WHERE c.id=products.cat_id AND status='active') as cat_name ")
                , DB::RAW("(SELECT count(*) FROM followers f left join users u on f.user_id=u.id WHERE u.id=" . $data['user_id'] . " AND products.id=f.p_id) as followed_yes")
                );
        
        $product->with(['followers' => function($query) {
                $query->select('id','p_id'); 
            }]);
        $product->with(['reviews' => function($query) {
                $query->select('id','p_id','r_des','rating','user_id','created_at');
                $query->where('status','active');
                /***********Get user details start******/
                    $query->with(['user'=>function($query){
                          $query->select('id','name','profile_pic','badges'
                        , DB::RAW("(COALESCE(profile_pic,'')) as profile_pic")
                        , DB::RAW("(CASE WHEN profile_pic IS NULL THEN '' WHEN profile_pic LIKE 'http%' THEN profile_pic ELSE CONCAT('" . env('IMAGE_URL') . "',profile_pic) END) as profile_pic_url")
                        , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE user_id=users.id AND status='active') as user_total_reviews ")
                        , DB::RAW("(SELECT COUNT(*) FROM followers WHERE u_id=users.id AND status='active') as user_total_followers")
                        );
                    }]);
                /***********Get user details end******/
                /***********Get review  gallery images start******/
                $query->with(['gallery'=>function($query){
                          $query->select('id','name','review_id'
                        , DB::RAW("(COALESCE(name,'')) as pic")
                        , DB::RAW("(CASE WHEN name IS NULL THEN '' WHEN name LIKE 'http%' THEN name ELSE CONCAT('" . env('IMAGE_URL') . "',name) END) as pic_url")
                        );
                    $query->where('status','active');
                }]);
               /***********Get review gallery images end******/   
                $query->orderBy('id','desc');
                $query->limit(2);
            }]); 
            
        $product->with(['faqs' => function($q) {
           $q->select('id','ques','ans','p_id');
           $q->where('status','active'); 
           $q->orderBy('id','desc');
           $q->limit(2);
       }]); 
        /***********Get Product  gallery images start******/
        $product->with(['gallery'=>function($query){
                  $query->select('id','name','product_id'
                , DB::RAW("(COALESCE(name,'')) as pic")
                , DB::RAW("(CASE WHEN name IS NULL THEN '' WHEN name LIKE 'http%' THEN name ELSE CONCAT('" . env('IMAGE_URL') . "',name) END) as pic_url")
                );
            $query->where('status','active');
        }]);
       /***********Get Product gallery images end******/       
        $product = $product->first();
        $product->followers_count =$product->followers->count();  
        return $product;
    }
}
