<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Hash,
    DateTime,
    DB;
use Carbon\Carbon;

class Service extends Model {

    protected $fillable = ['s_name', 's_des', 'cat_id', 'status'];

    public function categories() {
        return $this->belongsTo(\App\Models\Category::class, 'cat_id');
    }

    ////////////// 	Add New Service  //////////////////////////

    public static function add_new_service($data) {

        $service = new Service();
        $service->s_name = $data['s_name'];
        $service->s_des = isset($data['s_des']) ? $data['s_des'] : '';
        $service->s_img = isset($data['s_img']) ? $data['s_img'] : 'null';
        $service->cat_id = isset($data['cat_id']) ? $data['cat_id'] : '0';

        $service->s_timezone = isset($data['s_timezone']) ? $data['s_timezone'] : '';
        $service->s_lat = isset($data['s_lat']) ? $data['s_lat'] : null;
        $service->s_lng = isset($data['s_lng']) ? $data['s_lng'] : null;

        $service->user_id   = isset($data['user_id']) ? $data['user_id'] : '0';
        $service->b_id      = isset($data['b_id']) ? $data['b_id'] : NULL;
        $service->status    = isset($data['status']) ? $data['status'] : 'active';
        
        $service->features = isset($data['features']) ? $data['features'] : '';
        
        
        $service->created_at = new \DateTime;
        $service->updated_at = new \DateTime;

        $service->save();
        return $service->id;
    }

    /////////////// 	Update Service 		/////////////// 

    public static function update_service($data) {
        Service::where('id', $data['id'])->update([
            's_name' => $data['s_name'],
            's_des' => isset($data['s_des']) ? $data['s_des'] : '',
            's_img' => isset($data['s_img']) ? $data['s_img'] : 'null',
            'cat_id' => isset($data['cat_id']) ? $data['cat_id'] : '0',
            's_timezone' => isset($data['s_timezone']) ? $data['s_timezone'] : '',
            's_lat' => isset($data['s_lat']) ? $data['s_lat'] : null,
            's_lng' => isset($data['s_lng']) ? $data['s_lng'] : null,
             'features' => isset($data['features']) ? $data['features'] : '',
            'updated_at' => new \DateTime
        ]);
        return $data['id'];
    }

}
