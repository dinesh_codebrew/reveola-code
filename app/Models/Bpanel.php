<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\BpanelResetPasswordNotification;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as
CanResetPasswordContract;

class Bpanel extends Authenticatable implements CanResetPasswordContract {

    use Notifiable,
        CanResetPassword;

    protected $guard = 'bpanel';
    protected $table = 'users';
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $fillable = [
        'name',
        'email',
        'password'  
    ];

    public function sendPasswordResetNotification($token) {
        $this->notify(new BpanelResetPasswordNotification($token));
    } 
}
