<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Dec 2017 09:36:35 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Reliese\Database\Eloquent\Model as Eloquent;
use Hash,
    DateTime,
    DB;
use Carbon\Carbon;

/**
 * Class Category
 * 
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $category_filters
 * @property \Illuminate\Database\Eloquent\Collection $category_sub_subs
 * @property \Illuminate\Database\Eloquent\Collection $category_subs
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Category extends Model {

    // protected $table = 'categories';
    // protected $table = 'categories';
    protected $fillable = ['name', 'name_ar', 'slug', 'parent_id', 'sort_order', 'status'];

    public function children() {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    public function parent(){
        return $this->hasOne( 'App\Models\Category', 'id', 'parent_id' );
    }

    public function businesses() {
        return $this->hasMany(\App\Models\Business::class, 'cat_id');
    }
    public function products() {
        return $this->hasMany(\App\Models\Product::class, 'cat_id');
    }
    public function services() {
        return $this->hasMany(\App\Models\Service::class, 'cat_id');
    }

    public static function cat_name_like($data) {
        $cat_ids = Category::where('name', 'like', '%' . $data['cat_name'] . '%')->pluck('id')->toArray();
        return $cat_ids;
    }


    public static  function get_parent_ids($id) {
            $cat_Id = Category::select('id', 'parent_id')->where('id', $id)->first(); 
            $html = $cat_Id->id;
        if (!empty($cat_Id->parent_id)){
             $html.=",".Category::get_parent_ids($cat_Id->parent_id); 
        }
        return $html;
    }

//////////////////////////////			User get requests 		/////////// 

public static function user_get_categories($data) {

    return Category::where('blocked', '0')
                    ->select('id', 'name', 'image', DB::RAW("(CONCAT('" . env('IMAGE_URL') . "',image)) as image_url"), DB::Raw("(CASE WHEN categories.id=0 THEN 1 WHEN ((SELECT COUNT(*) FROM category_subs WHERE category_id=categories.id AND blocked='0') > 0) THEN 1 ELSE 0 END) as subcat_count"), DB::Raw("(CASE WHEN categories.id=0 THEN 1 WHEN ((SELECT COUNT(*) FROM category_sub_subs WHERE category_id=categories.id AND blocked='0') > 0) THEN 1 ELSE 0 END) as sub_subcat_count"), DB::Raw("(CASE WHEN categories.id=0 THEN 1 WHEN ((SELECT COUNT(*) FROM category_filters WHERE category_id=categories.id AND blocked='0') > 0) THEN 1 ELSE 0 END) as filter_count")
                    )
                    ->having('subcat_count', '!=', 0)
                    ->having('sub_subcat_count', '!=', 0)
                    ->having('filter_count', '!=', 0)
                    ->with(['category_subs' => function($q0) use ($data) {

                            $q0->select('id', 'category_id', 'name', DB::Raw("(CASE WHEN category_subs.id=0 THEN 1 WHEN ((SELECT COUNT(*) FROM category_sub_subs WHERE category_sub_id=category_subs.id AND blocked='0') > 0) THEN 1 ELSE 0 END) as sub_subcat_count")
                            );
                            $q0->having('sub_subcat_count', '!=', 0);
                            $q0->where('blocked', '0');

                            $q0->with(['category_sub_subs' => function($q1) use ($data) {

                                    $q1->select('id', 'category_id', 'category_sub_id', 'name', DB::Raw("(CASE WHEN category_sub_subs.id=0 THEN 1 WHEN ((SELECT COUNT(*) FROM category_filters WHERE category_sub_sub_id=category_sub_subs.id AND blocked='0') > 0) THEN 1 ELSE 0 END) as sub_subcat_filter_count")
                                    );
                                    $q1->where('blocked', '0');
                                    $q1->having('sub_subcat_filter_count', '!=', 0);

                                    $q1->with(['category_filters' => function($q2) use ($data) {

                                            $q2->select('id', 'category_id', 'category_sub_id', 'category_sub_sub_id', 'name', 'type', 'min_value', 'max_value', 'is_required', DB::RAW("(CASE WHEN category_filters.type != 'List' THEN '1' WHEN ((SELECT COUNT(*) FROM category_filter_values WHERE category_filter_id=category_filters.id AND deleted='0') > 0) THEN 1 ELSE 0 END) as filter_value_count")
                                            );
                                            $q2->having('filter_value_count', '!=', 0);
                                            $q2->where('blocked', '0');


                                            $q2->with(['category_filter_values' => function($q3) use ($data) {

                                                    $q3->select('id', 'category_filter_id', 'name', 'description');
                                                    //$q3->orderby('name');
                                                    $q3->where('deleted', '0');
                                                }]);
                                        }]);
                                        }]);
                                        }])
                                            ->get();
                        }

///////////////////////////////		Add New Category 		////////////////////////////////////////////////////////////

        public static function add_new_category($data) {

            $category = new Category();
            $category->name = $data['name'];
            $category->name_ar = isset($data['name_ar']) ? $data['name_ar'] : $data['name'];
            $category->cat_image = isset($data['cat_image']) ? $data['cat_image'] : 'null';
            $category->slug = \App\Http\Controllers\Admin\AdminCategoryController::slugify($data['name']);
            $category->sort_order = $data['sort_order'];
            $category->parent_id = isset($data['parent_id']) ? $data['parent_id'] : '0';

            $category->created_at = new \DateTime;
            $category->updated_at = new \DateTime;

            $category->save();

            return $category->id;
        }

///////////////////////////////		Update Category 		////////////////////////////////////////////////////////////

        public static function update_category($data) {
            Category::where('id', $data['id'])->update([
                'name' => $data['name'],
                'name_ar' => isset($data['name_ar']) ? $data['name_ar'] : $data['name'],
                'cat_image' => isset($data['cat_image']) ? $data['cat_image'] : 'null',
                'slug' => \App\Http\Controllers\Admin\AdminCategoryController::slugify($data['name']),
                'sort_order' => $data['sort_order'],
                'parent_id' => isset($data['parent_id']) ? $data['parent_id'] : '0',
                'updated_at' => new \DateTime
            ]);
            return $data['id'];
        }
        
        public static function get_product($data) {
        $products  = Category::all();
        if (isset($data['page']))
            $products->skip(($data['page']-1) * $data['offset'])->take($data['offset']);
        else
           $products->skip(0)->take(4);
        return $products->get();
            
        }
    /********************* Get Category relational data *******************/
    public static function get_cat_data($id) {
        $categories = Category::select('*'
                    , DB::RAW("(SELECT COUNT(*) FROM businesses WHERE id=businesses.cat_id AND status='active') as total_businesses ")
                    , DB::RAW("(SELECT COUNT(*) FROM products WHERE id=products.cat_id AND status='active') as total_products")
                    , DB::RAW("(SELECT COUNT(*) FROM services WHERE id=services.cat_id AND status='active') as total_services")
                    );
            
        $categories->where('id',$id)->where('status','active');
        $categories->with(['businesses' => function($query) {
                $query->select('id','b_name','cat_id', DB::RAW("(COALESCE(b_pic,'')) as b_pic"), DB::RAW("(CASE WHEN b_pic IS NULL THEN '' WHEN b_pic LIKE 'http%' THEN b_pic ELSE CONCAT('" . env('IMAGE_URL') . "',b_pic) END) as b_pic_url"));
                $query->where('status','active');
                $query->limit(4);
            }]);
        $categories->with(['products'=>function($query){
               $query->select('id','p_name','cat_id'
                        , DB::RAW("(COALESCE(p_img,'')) as p_img")
                        , DB::RAW("(CASE WHEN p_img IS NULL THEN '' WHEN p_img LIKE 'http%' THEN p_img ELSE CONCAT('" . env('IMAGE_URL') . "',p_img) END) as p_img_url") );
                $query->where('status','active');
                $query->limit(4); 
            }]);
        $categories->with(['services'=>function($query){
              $query->select('id','cat_id','s_name'
                        , DB::RAW("(COALESCE(s_img,'')) as s_img")
                        , DB::RAW("(CASE WHEN s_img IS NULL THEN '' WHEN s_img LIKE 'http%' THEN s_img ELSE CONCAT('" . env('IMAGE_URL') . "',s_img) END) as s_img_url") );
                $query->where('status','active');
                $query->limit(4); 
            }]); 
            
        return $categories->first();
            
        }
    

    }
