<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use DB;


class Follower extends Model {

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'u_id');
    }

    public function service() {
        return $this->belongsTo(\App\Models\Service::class, 's_id');
    }

    public function product() {
        return $this->belongsTo(\App\Models\Product::class, 'p_id');
    }

    public function business() {
        return $this->belongsTo(\App\Models\Business::class, 'b_id');
    }

    Public static function add_follow($data) {
        $follower           = new Follower(); 
        $follower->b_id     = isset($data['b_id']) ? $data['b_id']          : 0 ;
        $follower->p_id     = isset($data['P_id']) ? $data['p_id']          : 0 ;
        $follower->s_id     = isset($data['s_id']) ? $data['s_id']          : 0 ;
        $follower->u_id     = isset($data['u_id']) ? $data['u_id']          : 0 ;
        $follower->user_id  = isset($data['user_id']) ? $data['user_id']    : 0 ;
        

        $follower->created_at = new \DateTime;
        $follower->updated_at = new \DateTime;
        if($follower->user_id!=0)  
            $follower->save();
        return true;
    }

    Public static function remove_follow($data) {
        if(isset($data['b_id']))
            DB::table('followers')->where('b_id', $data['b_id'])->where('user_id', $data['user_id'])->delete();
        else if(isset($data['p_id']))
            DB::table('followers')->where('p_id', $data['p_id'])->where('user_id', $data['user_id'])->delete();
        else if(isset($data['s_id']))
            DB::table('followers')->where('s_id', $data['s_id'])->where('user_id', $data['user_id'])->delete();
        else if(isset($data['u_id']))
            DB::table('followers')->where('u_id', $data['u_id'])->where('user_id', $data['user_id'])->delete();
        return true;
    }

}
