<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Hash,
    DateTime,
    DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable; 
use Laravel\Passport\HasApiTokens;
use App\Models\SocialAccount;
use App\Models\Review;
use App\Models\Follower;

class User extends Authenticatable {

   use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'phone', 'latitude', 'longitude', 'timezone', 'fcm_id', 'lang'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', // 'password',
    ];

    public function business(){ 
       return $this->belongsTo(\App\Models\Business::class, 'user_id');
    }
    
    public function service(){ 
       return $this->hasMany(\App\Models\SocialAccount::class, 'user_id');
    }
    
    public function product(){ 
       return $this->hasMany(\App\Models\SocialAccount::class, 'user_id');
    }
    
    
    public function social_accounts() {
        return $this->hasMany(\App\Models\SocialAccount::class, 'user_id');
    }
    
    public function reviews() {
        return $this->hasMany(\App\Models\Review::class, 'user_id');
    }
    
     public function followers() {
        return $this->hasMany(\App\Models\Follower::class, 'u_id');
    }
    public function following() {
        return $this->hasMany(\App\Models\Follower::class, 'user_id');
    }
    
    public function orders() {
        return $this->hasMany(\App\Models\Order::class, 'user_id');
    }
    
    ///////////////////////////			Insert User 		///////////////////////////////////////////

    public static function create_new_user($data) {
       /* $id = DB::table('users')->insertGetId([
            'name' => $data['name'],
            'email' =>  $data['email'],
            'phone' =>  isset($data['phone']) ? $data['phone'] : null,
            'profile_pic' =>  isset($data['profile_pic']) ? $data['profile_pic'] : null,
            'password' =>  isset($data['password']) ? Hash::make($data['password']) : '',
            'timezone' =>  isset($data['timezone']) ? $data['timezone'] : 'null',
            'latitude' =>  isset($data['latitude']) ? $data['latitude'] : 'null',
            'longitude' =>  isset($data['longitude']) ? $data['longitude'] : 'null',
            'fcm_id' =>  isset($data['fcm_id']) ? $data['fcm_id'] : 'null',
            'lang' =>  isset($data['lang']) ? $data['lang'] : 'null',
            ]);*/
        $user           = new User();
        $user->name     = $data['name'];
        $user->email    = isset($data['email']) ? $data['email'] : null ;

        $user->phone    = isset($data['phone']) ? $data['phone'] : null;

        $user->profile_pic   = isset($data['profile_pic']) ? $data['profile_pic'] : null;
        

        $user->password = isset($data['password']) ? Hash::make($data['password']) : '';
        
        $user->flat_no  = isset($data['flat_no'])  ? $data['flat_no']     : 'null';
        $user->address  = isset($data['address'])  ? $data['address']     : 'null';
        $user->latitude = isset($data['latitude']) ? $data['latitude'] : 'null';
        $user->longitude= isset($data['longitude']) ? $data['longitude'] : 'null';
        $user->timezone = isset($data['timezone']) ? $data['timezone'] : 'null';
        $user->fcm_id   = isset($data['fcm_id']) ? $data['fcm_id'] : 'null';
        $user->lang     = isset($data['lang']) ? $data['lang'] : 'null';
        $user->badges   = isset($data['badges']) ? $data['badges'] : 0;


        $user->created_at = new \DateTime;
        $user->updated_at = new \DateTime;

        $user->save(); 
        return $user->id; 
    }

    /////////////////////////       Get User profile        ////////////////////////////

    public static function get_user_profile($data) {
        $user = User::whereId($data['user_id']);
      
        $user->select('*'
                , DB::RAW("(COALESCE(profile_pic,'')) as profile_pic")
                , DB::RAW("(CASE WHEN profile_pic IS NULL THEN '' WHEN profile_pic LIKE 'http%' THEN profile_pic ELSE CONCAT('" . env('IMAGE_URL') . "',profile_pic) END) as profile_pic_url")
                ,DB::Raw('(select count(*) from social_accounts sa where sa.user_id=users.id) as total_social'));
        
        $user->with(['followers' => function($query) {
                $query->select('id','u_id');
            }]);
        $user->with(['reviews' => function($query) {
                $query->select('id','user_id');
            }]);
	 $user->with(['following' => function($query) {
                $query->select('id','user_id');
            }]);
        $user = $user->first();
        $user->followers_count =$user->followers->count();
        $user->reviews_count =$user->reviews->count();
        $user->following_count =$user->following->count();
        return $user;
    } 
      public static function get_user_profileFull($data) {
        $user = User::whereId($data['user_id']);
      
        $user->select('*', DB::RAW("(COALESCE(profile_pic,'')) as profile_pic"), DB::RAW("(CASE WHEN profile_pic IS NULL THEN '' WHEN profile_pic LIKE 'http%' THEN profile_pic ELSE CONCAT('" . env('IMAGE_URL') . "',profile_pic) END) as profile_pic_url")
        );
        
        $user->with(['followers' => function($query) {
                $query->select('id','u_id');
            }]);
     /*   $user->with(['reviews' => function($query) {
                $query->select('*');
            }]);*/
	 $user->with(['following' => function($query) {
                $query->select('id','user_id');
            }]);
        $user = $user->first();
        $user->followers_count =$user->followers->count();
        $user->reviews_count =$user->reviews->count();
        $user->following_count =$user->following->count();
        return $user;
    }

/////////////////////////       Update User Password    /////////////////////////////

    public static function update_user_password($data)
        {
            User::whereId($data['user_id'])
            ->limit(1)
            ->update([
                'password' => $data['new_password'],
                'updated_at' => new \DateTime
            ]);

        }
/////////////////////////       Update User Password    /////////////////////////////

    public static function create_user_password($data)
        {
            User::whereId($data['user_id'])
            ->limit(1)
            ->update([
                'password' => $data['new_password'],
                'updated_at' => new \DateTime
            ]);

        }

/////////////////////////       Update Profile      //////////////////////////////////

    public static function update_profile($data, $user)
        {
            User::whereId($user->id)
            ->limit(1)
            ->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'phone' => isset($data['phone']) ? $data['phone'] : NULL,
                    'profile_pic' => isset($data['profile_pic']) ? $data['profile_pic'] : $user->profile_pic,
                    'updated_at' => new \DateTime
            ]);
             
        }

/////////////////////////       Update User Fcm         ///////////////////////////////

    public static function update_user_fcm($data, $user)
        {
                User::whereId($user->id)
                ->limit(1)
                ->update([
                        'fcm_id' => isset($data['fcm_id']) ? $data['fcm_id'] : $user->fcm_id,
                        'timezone' => $data['timezone'],
                        'updated_at' => new \DateTime
                ]);        
        }

/////////////////////////
         public static function update_user_admin($data, $user)
        {
            User::whereId($user->id)
            ->limit(1)
            ->update([
                    'name' => $data['name'],
                    'email' => isset($data['email']) ? $data['email'] : NULL,
                    'password'=> isset($data['password']) ? Hash::make($data['password']) : $user->password,
                    'phone' => isset($data['phone']) ? $data['phone'] : NULL,
                    'badges' => isset($data['badges']) ? $data['badges'] : 0,
                    'flat_no' => isset($data['flat_no']) ? $data['flat_no'] : NULL,
                    'address' => isset($data['address']) ? $data['address'] : NULL,
                    'latitude' => isset($data['latitude']) ? $data['latitude'] : NULL,
                    'longitude' => isset($data['longitude']) ? $data['longitude'] : NULL,
                    'profile_pic' => isset($data['profile_pic']) ? $data['profile_pic'] : $user->profile_pic,
                    'updated_at' => new \DateTime
            ]);
             
        }
     
}
