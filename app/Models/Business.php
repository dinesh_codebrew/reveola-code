<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB; 

class Business extends Model {

    public function category() {
        return $this->belongsTo(\App\Models\Category::class, 'cat_id');
    }
    public function user(){
      return $this->hasOne( \App\Models\User::class, 'id', 'user_id' );
    }
    public function gallery() {
        return $this->hasMany(\App\Models\Gallery::class, 'business_id');
    }

    public function reviews() {
        return $this->hasMany(\App\Models\Review::class, 'b_id');
    }

    public function followers() {
        return $this->hasMany(\App\Models\Follower::class, 'b_id');
    }
    
    public function faqs() {
        return $this->hasMany(\App\Models\Faq::class, 'b_id');
    }

    public static function get_nearBusiness($data) {
        $business = Business::select('*'
						,DB::RAW("(COALESCE(b_pic,'')) as b_pic"), DB::RAW("(CASE WHEN b_pic IS NULL THEN '' WHEN b_pic LIKE 'http%' THEN b_pic ELSE CONCAT('" . env('IMAGE_URL') . "',b_pic) END) as b_pic_url")	
                        , DB::RAW("(" . $data['circle_radius'] . " * acos(cos(radians(" . $data['lat'] . ")) * cos(radians(b_lat)) *  cos(radians(b_lng) - radians(" . $data['lng'] . ")) + sin(radians(" . $data['lat'] . ")) * sin(radians(b_lat)))) AS distance")
                        , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE b_id=businesses.id AND status='active') as total_reviews ")
                        , DB::RAW("(SELECT ROUND(COALESCE(avg(rating),0),0)    FROM reviews WHERE b_id=businesses.id AND status='active') as smile ")
                        , DB::RAW("(SELECT name    FROM categories c WHERE c.id=businesses.cat_id AND status='active') as cat_name ")
                        , DB::RAW("(SELECT count(*) FROM followers f left join users u on f.user_id=u.id WHERE u.id=" . $data['user_id'] . " AND businesses.id=f.b_id) as followed_yes"))
                ->havingRaw('distance < ?', [$data['max_distance']])
                ->where('status', 'active')
                ->orderByRaw('distance');
        
         
        if (isset($data['page']))
            $business->skip(($data['page']-1) * $data['offset'])->take($data['offset']);
        else
           $business->skip(0)->take(4);
        return $business->get();
    }
    
 public static function get_nearBusiness_search($data) {
        $business = Business::select('*'
                        ,DB::RAW("(COALESCE(b_pic,'')) as b_pic"), DB::RAW("(CASE WHEN b_pic IS NULL THEN '' WHEN b_pic LIKE 'http%' THEN b_pic ELSE CONCAT('" . env('IMAGE_URL') . "',b_pic) END) as b_pic_url")
                        , DB::RAW("(" . $data['circle_radius'] . " * acos(cos(radians(" . $data['lat'] . ")) * cos(radians(b_lat)) *  cos(radians(b_lng) - radians(" . $data['lng'] . ")) + sin(radians(" . $data['lat'] . ")) * sin(radians(b_lat)))) AS distance")
                        , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE b_id=businesses.id AND status='active') as total_reviews ")
                        , DB::RAW("(SELECT ROUND(COALESCE(avg(rating),0),0)    FROM reviews WHERE b_id=businesses.id AND status='active') as smile ")
                        , DB::RAW("(SELECT name  FROM categories c WHERE c.id=businesses.cat_id AND status='active') as cat_name ")
                        , DB::RAW("(SELECT count(*) FROM followers f left join users u on f.user_id=u.id WHERE u.id=" . $data['user_id'] . " AND businesses.id=f.b_id) as followed_yes"))
                ->havingRaw('distance < ?', [$data['max_distance']])
                ->where('status', 'active')
                ->orderByRaw('distance');
        
        if (isset($data['search'])){         
           $business->Where('b_name', 'like', '%' . $data['search'] . '%');
           $business->orWhere('b_des', 'like', '%' . $data['search'] . '%');
           $business->orWhereHas('category', function ($query) use ($data) {
               
                     $query->where('name', 'like', '%'.$data['search'].'%');
                
                  $query->orWhereHas('parent', function ($query) use ($data) {
                        
                       $query->where('name', 'like', '%'.$data['search'].'%');
                       
                      }); 
            });
         
        }
        
        if (isset($data['page']))
            $business->skip(($data['page']-1) * $data['offset'])->take($data['offset']);
        else
           $business->skip(0)->take(4);
        return $business->get();
    }
    
    public static function get_reviewed_Business($data) {
        $business = Business::select('*'
						,DB::RAW("(COALESCE(b_pic,'')) as b_pic"), DB::RAW("(CASE WHEN b_pic IS NULL THEN '' WHEN b_pic LIKE 'http%' THEN b_pic ELSE CONCAT('" . env('IMAGE_URL') . "',b_pic) END) as b_pic_url")
                        , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE b_id=businesses.id AND status='active') as total_reviews ")
                        , DB::RAW("(SELECT ROUND(COALESCE(avg(rating),0),0)    FROM reviews WHERE b_id=businesses.id AND status='active') as smile ")
                        , DB::RAW("(SELECT name    FROM categories c WHERE c.id=businesses.cat_id AND status='active') as cat_name ")
                        , DB::RAW("(SELECT count(*) FROM followers f left join users u on f.user_id=u.id WHERE u.id=" . $data['user_id'] . " AND businesses.id=f.b_id) as followed_yes"));
        
        $business->orderByRaw('total_reviews desc'); 
        $business->where('status', 'active');
        if (isset($data['cat_id']) && !empty($data['cat_id'])){          
           $business->WhereHas('category', function ($query) use ($data) {
               
                    $query->where('id',$data['cat_id']);              
                    
                    $query->orWhereHas('children', function ($query) use ($data) {                    
                       $query->where('id', $data['cat_id']);   
                    });  
                    /* $query->orWhereHas('parent', function ($query) use ($data) {                     
                       $query->where('id', $data['cat_id']);                       
                    }); */
            });
         
        }
        if (isset($data['page']))
            $business->skip(($data['page']-1) * $data['offset'])->take($data['offset']);
        else
           $business->skip(0)->take(4);
        return $business->get();
    } 
    
    public static function get_reviewed_Business_search($data) {
        $business = Business::select('*'
                ,DB::RAW("(COALESCE(b_pic,'')) as b_pic"), DB::RAW("(CASE WHEN b_pic IS NULL THEN '' WHEN b_pic LIKE 'http%' THEN b_pic ELSE CONCAT('" . env('IMAGE_URL') . "',b_pic) END) as b_pic_url")
                        , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE b_id=businesses.id AND status='active') as total_reviews ")
                        , DB::RAW("(SELECT ROUND(COALESCE(avg(rating),0),0)    FROM reviews WHERE b_id=businesses.id AND status='active') as smile ")
                        , DB::RAW("(SELECT name    FROM categories c WHERE c.id=businesses.cat_id AND status='active') as cat_name ")
                        , DB::RAW("(SELECT count(*) FROM followers f left join users u on f.user_id=u.id WHERE u.id=" . $data['user_id'] . " AND businesses.id=f.b_id) as followed_yes"));
        
        $business->orderByRaw('total_reviews desc');
        $business->where('status', 'active');
        if (isset($data['cat_id']) && !empty($data['cat_id'])){          
           $business->WhereHas('category', function ($query) use ($data) {
               
                    $query->where('id',$data['cat_id']);              
                    
                    $query->orWhereHas('children', function ($query) use ($data) {                    
                       $query->where('id', $data['cat_id']);   
                    });  
                    /* $query->orWhereHas('parent', function ($query) use ($data) {                     
                       $query->where('id', $data['cat_id']);                       
                    }); */
            });
         
        }
        if (isset($data['search'])&& !empty($data['search'])){         
           $business->Where('b_name', 'like', '%' . $data['search'] . '%');
           $business->orWhere('b_des', 'like', '%' . $data['search'] . '%');
           $business->orWhereHas('category', function ($query) use ($data) {
               
                     $query->where('name', 'like', '%'.$data['search'].'%');
                
                  $query->orWhereHas('parent', function ($query) use ($data) {
                        
                       $query->where('name', 'like', '%'.$data['search'].'%');
                       
                      }); 
            });
         
        }
        if (isset($data['page']))
            $business->skip(($data['page']-1) * $data['offset'])->take($data['offset']);
        else
            $business->skip(0)->take(4);
        return $business->get();
    }

    
    
    ///////////////// 	Add New Business  ///////////////////////// 

                public static function add_new_business($data) {

                    $business = new Business();
                    $business->b_name       =   $data['b_name'];
                    $business->cat_id       =   $data['cat_id'];
                    $business->b_lat        =   $data['b_lat'];
                    $business->b_lng        =   $data['b_lng'];
                    
        $business->b_des        =   isset($data['b_des']) ? $data['b_des'] : '';
        $business->b_flat_no    =   isset($data['b_flat_no']) ? $data['b_flat_no'] : '';
        
                    $business->b_address    =   $data['b_address'];
                    $business->b_pic    = isset($data['b_pic']) ? $data['b_pic'] : '';
                    $business->weburl   = isset($data['weburl']) ? $data['weburl'] : '';
                    $business->price    = isset($data['price']) ? $data['price'] : '';
                    $business->phone    = isset($data['phone']) ? $data['phone'] : '';
                    $business->email    = isset($data['email']) ? $data['email'] : '';
                    $business->offer    = isset($data['offer']) ? $data['offer'] : '';
                    
        $business->user_id  = isset($data['user_id']) ? $data['user_id'] : '0'; 
        $business->status   = isset($data['status']) ? $data['status'] : 'active';
                    
                    $business->created_at = new \DateTime;
                    $business->updated_at = new \DateTime;
                    $business->save();
                return $business->id;
            }

////////////////	Update Category 	/////////////////////////////////
                public static function update_business($data) {
                    Business::where('id', $data['id'])->update([
                    'b_name'    => $data['b_name'],
                    'cat_id'    => $data['cat_id'],
                    'b_address' => $data['b_address'],
                    'b_lat'     => $data['b_lat'],
                    'b_lng'     => $data['b_lng'],
                    'b_flat_no' => isset($data['b_flat_no']) ? $data['b_flat_no'] : '',
                    'b_des'     => isset($data['b_des']) ? $data['b_des'] : '',
                    'b_pic'     => isset($data['b_pic']) ? $data['b_pic'] : '',
                    'weburl'    => isset($data['weburl']) ? $data['weburl'] : '',
                    'price'     => isset($data['price']) ? $data['price'] : '',
                    'phone'     => isset($data['phone']) ? $data['phone'] : '',
                    'email'     => isset($data['email']) ? $data['email'] : '',
                    'offer'     => isset($data['offer']) ? $data['offer'] : '',
                    'updated_at' => new \DateTime
                    ]);
                    return $data['id'];
                }
                
        ////////////////////////       Get business detail ////////////////////////////

    public static function get_business_details($data) {
        $business = Business::whereId($data['b_id']);
      
        $business->select('*'
                , DB::RAW("(COALESCE(b_pic,'')) as b_pic")
                , DB::RAW("(CASE WHEN b_pic IS NULL THEN '' WHEN b_pic LIKE 'http%' THEN b_pic ELSE CONCAT('" . env('IMAGE_URL') . "',b_pic) END) as b_pic_url")
                 , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE b_id=businesses.id AND status='active') as total_reviews ")
                , DB::RAW("(SELECT ROUND(COALESCE(avg(rating),0),0)    FROM reviews WHERE b_id=businesses.id AND status='active') as smile ")
                , DB::RAW("(SELECT name FROM categories c WHERE c.id=businesses.cat_id AND status='active') as cat_name ")
                , DB::RAW("(SELECT count(*) FROM followers f left join users u on f.user_id=u.id WHERE u.id=" . $data['user_id'] . " AND businesses.id=f.b_id) as followed_yes")
                );
        
        $business->with(['followers' => function($query) {
                $query->select('id','b_id'); 
            }]);
        $business->with(['reviews' => function($query) {
                $query->select('id','b_id','r_des','rating','user_id','created_at');
                $query->where('status','active');
                /***********Get user details start******/
                    $query->with(['user'=>function($query){
                          $query->select('id','name','profile_pic','badges'
                        , DB::RAW("(COALESCE(profile_pic,'')) as profile_pic")
                        , DB::RAW("(CASE WHEN profile_pic IS NULL THEN '' WHEN profile_pic LIKE 'http%' THEN profile_pic ELSE CONCAT('" . env('IMAGE_URL') . "',profile_pic) END) as profile_pic_url")
                        , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE user_id=users.id AND status='active') as user_total_reviews ")
                        , DB::RAW("(SELECT COUNT(*) FROM followers WHERE u_id=users.id AND status='active') as user_total_followers")
                        );
                    }]);
                /***********Get user details end******/
                $query->orderBy('id','desc');
                $query->limit(2);
            }]); 
            
        $business->with(['faqs' => function($q) {
           $q->select('id','ques','ans','b_id');
           $q->where('status','active'); 
           $q->orderBy('id','desc');
           $q->limit(2);
       }]); 
        /***********Get Product  gallery images start******/
        $business->with(['gallery'=>function($query){
                  $query->select('id','name','business_id'
                , DB::RAW("(COALESCE(name,'')) as pic")
                , DB::RAW("(CASE WHEN name IS NULL THEN '' WHEN name LIKE 'http%' THEN name ELSE CONCAT('" . env('IMAGE_URL') . "',name) END) as pic_url")
                );
            $query->where('status','active');
        }]);
       /***********Get Product gallery images end******/       
        $business = $business->first();
        $business->followers_count =$business->followers->count();  
        return $business;
    }
}
