<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Faq extends Model
{
    public function business(){ 
       return $this->belongsTo(\App\Models\Business::class, 'b_id');
    }    
    public function comments() {
        return $this->hasMany(\App\Models\Comment::class, 'faq_id');
    }
     ////////////////////////       Get all reviews ////////////////////////////

    public static function get_all_faq($data) {
        $faqs = Faq::select('id','ques');  
     
        $faqs->with(['comments' => function($query) {
                $query->select('id','faq_id','c_des','user_id','created_at');
                $query->where('status','active');
                /***********Get user details start******/
                $query->with(['user'=>function($query){
                          $query->select('id','name','profile_pic','badges'
                        , DB::RAW("(COALESCE(profile_pic,'')) as profile_pic")
                        , DB::RAW("(CASE WHEN profile_pic IS NULL THEN '' WHEN profile_pic LIKE 'http%' THEN profile_pic ELSE CONCAT('" . env('IMAGE_URL') . "',profile_pic) END) as profile_pic_url")
                        , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE user_id=users.id AND status='active') as user_total_reviews ")
                        , DB::RAW("(SELECT COUNT(*) FROM followers WHERE u_id=users.id AND status='active') as user_total_followers")
                        );
                }]);
            /***********Get user details end******/
        }]);
        $faqs->where('status','active');

        if(isset($data['b_id']) && !empty($data['b_id']))       {
            $faqs->where('b_id',$data['b_id']);              
        }  
        else if(isset($data['p_id']) && !empty($data['p_id']))  {
            $faqs->where('p_id',$data['p_id']);               
        }
        else if(isset($data['s_id']) && !empty($data['s_id']))  {
            $faqs->where('s_id',$data['s_id']);              
        }
             
        $faqs->orderBy('id','desc'); 
        if (isset($data['page']))
            $faqs->skip(($data['page']-1) * $data['offset'])->take($data['offset']);
        else
            $faqs->skip(0)->take(4);
        return $faqs->get();
    }
    
    public static function faq_details($id) {
        $faqs = Faq::select('id','ques');       
        $faqs->with(['comments' => function($query) {
                $query->select('id','faq_id','c_des','user_id','created_at');
                $query->where('status','active');
                /***********Get user details start******/
                $query->with(['user'=>function($query){
                          $query->select('id','name','profile_pic','badges'
                        , DB::RAW("(COALESCE(profile_pic,'')) as profile_pic")
                        , DB::RAW("(CASE WHEN profile_pic IS NULL THEN '' WHEN profile_pic LIKE 'http%' THEN profile_pic ELSE CONCAT('" . env('IMAGE_URL') . "',profile_pic) END) as profile_pic_url")
                        , DB::RAW("(SELECT COUNT(*) FROM reviews WHERE user_id=users.id AND status='active') as user_total_reviews ")
                        , DB::RAW("(SELECT COUNT(*) FROM followers WHERE u_id=users.id AND status='active') as user_total_followers")
                        );
                }]);
            /***********Get user details end******/
        }]);
        $faqs->where('status','active');
        $faqs->where('id',$id); 
        return $faqs->first();
    }
    
}
