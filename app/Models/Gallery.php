<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected  $table='gallery';
    public function Product() {   return $this->belongsTo(\App\Models\Product::class,'product_id');}
    public function Service() {   return $this->belongsTo(\App\Models\Service::class,'service_id');}

    ////////////// 	Add New Image //////////////////////////

    public static function add_new_gallery_image($data) {

        $gallery = new Gallery();
        $gallery->name = $data['name'];  
        $gallery->product_id = isset($data['product_id']) ? $data['product_id'] : NULL;
        $gallery->service_id = isset($data['service_id']) ? $data['service_id'] : NULL;
        $gallery->business_id = isset($data['business_id']) ? $data['business_id'] : NULL;
 
        $gallery->created_at = new \DateTime;
        $gallery->updated_at = new \DateTime;

        $gallery->save();

        return $gallery->id;
    }
}
