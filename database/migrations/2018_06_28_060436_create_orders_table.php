<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('user_id');
			$table->bigInteger('sub_id');
			$table->string('sub_name', 150);
			$table->float('sub_price', 10, 4);
			$table->string('validity', 50);
			$table->dateTime('start_date');
			$table->dateTime('end_date');
			$table->string('stripe_id', 200)->nullable();
			$table->string('balance_transaction', 200)->nullable();
			$table->string('transaction_created', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
