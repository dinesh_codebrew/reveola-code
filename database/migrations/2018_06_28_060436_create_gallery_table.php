<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gallery', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('product_id')->nullable();
			$table->bigInteger('service_id')->nullable();
			$table->bigInteger('business_id')->nullable();
			$table->bigInteger('review_id')->nullable();
			$table->string('name', 150);
			$table->enum('status', array('active','block'))->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gallery');
	}

}
