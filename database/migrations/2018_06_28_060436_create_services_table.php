<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('services', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('cat_id');
			$table->bigInteger('b_id')->nullable();
			$table->bigInteger('user_id')->nullable();
			$table->string('s_name', 100);
			$table->string('s_des', 250)->nullable();
			$table->string('s_img', 150)->nullable();
			$table->enum('status', array('active','block'))->default('active');
			$table->text('features', 65535)->nullable();
			$table->string('s_timezone', 100)->nullable();
			$table->float('s_lat', 10, 6)->nullable();
			$table->float('s_lng', 10, 6)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('services');
	}

}
