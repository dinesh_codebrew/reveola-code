<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('cat_id');
			$table->bigInteger('b_id')->nullable();
			$table->bigInteger('user_id')->nullable();
			$table->string('p_name', 100);
			$table->string('p_description', 250)->nullable();
			$table->string('price', 50)->nullable();
			$table->string('p_img', 150);
			$table->enum('status', array('active','block'))->default('active');
			$table->text('features', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
