<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reviews', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->text('r_des', 65535);
			$table->integer('rating')->default(0);
			$table->integer('user_id')->nullable()->index('user_id');
			$table->enum('anonymous', array('true','false'))->default('false');
			$table->enum('contact_me', array('true','false'))->default('false');
			$table->bigInteger('b_id')->index('b_id');
			$table->bigInteger('p_id')->index('p_id');
			$table->bigInteger('s_id')->index('s_id');
			$table->enum('status', array('active','block'))->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reviews');
	}

}
