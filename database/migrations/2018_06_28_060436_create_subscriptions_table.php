<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subscriptions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('sub_name', 150);
			$table->text('sub_desc', 65535)->nullable();
			$table->float('sub_price', 10, 4)->default(0.0000);
			$table->string('sub_tag_line', 150)->nullable();
			$table->string('validity', 100);
			$table->enum('status', array('active','block'))->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subscriptions');
	}

}
