<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('businesses', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('user_id')->nullable()->default(0);
			$table->string('b_name', 200);
			$table->string('b_pic', 150)->nullable();
			$table->string('b_address', 250);
			$table->string('b_flat_no', 150)->nullable();
			$table->bigInteger('cat_id');
			$table->text('features', 65535)->nullable();
			$table->string('timezone', 100)->nullable();
			$table->float('b_lat', 10, 6);
			$table->float('b_lng', 10, 6);
			$table->string('weburl', 100)->nullable();
			$table->string('price', 150)->nullable();
			$table->string('phone', 100)->nullable();
			$table->string('email', 100)->nullable();
			$table->string('offer', 200)->nullable();
			$table->text('b_des', 65535)->nullable();
			$table->bigInteger('views')->nullable()->default(0);
			$table->enum('status', array('active','block'))->default('active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('businesses');
	}

}
