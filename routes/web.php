<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/chat', 'Admin\AdminController@admin_chat')->name('admin_chat');


Route::get('Api/V1/RESIZE/{id}', function($id) {

    $img = Image::make(env('FILE_URL') . $id)->resize(function($constraint) {
        $constraint->aspectRatio();
    });

    return $img->response();
});
Route::get('Api/V1/RESIZE_BU/{id}/{width?}/{height?}', function($id, $width = null, $height = null) {
    $img = Image::make(env('FILE_URL') . $id)->resize($width, $height);
    return $img->response();
});
Route::get('Api/V1/RESIZE/{id}/{width?}/{height?}', function($id, $width = null, $height = null) {

    $img = Image::make(env('FILE_URL') . $id)->resize($width, $height, function($constraint) {
        $constraint->aspectRatio();
    });

    return $img->response();
});
Route::get('Api/V1/RESIZE_ASPECT/{id}/{width?}/{height?}', function($id, $width = null, $height = null) {

    $img = Image::make(env('FILE_URL') . $id)->resize($width, $height, function($constraint) {
        $constraint->aspectRatio();
    });

    return $img->response();
}); 

Route::get('/administrator', 'Admin\AdminController@admin_login_get')->name('admin_login_get');

//admin password reset routes
Route::post('/admin/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('/admin/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('/admin/password/reset', 'Auth\AdminResetPasswordController@reset');
Route::get('/admin/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

//admin password reset routes
Route::post('/bpanel/password/email', 'Auth\BusinessForgotPasswordController@sendResetLinkEmail')->name('bpanel.password.email');
Route::get('/bpanel/password/reset', 'Auth\BusinessForgotPasswordController@showLinkRequestForm')->name('bpanel.password.request');
Route::post('/bpanel/password/reset', 'Auth\BusinessResetPasswordController@reset');
Route::get('/bpanel/password/reset/{token}', 'Auth\BusinessResetPasswordController@showResetForm')->name('bpanel.password.reset');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin_middleware'], function() {
    Route::get('/', 'AdminController@admin_dashboard')->name('admin.dashboard');

    Route::get('login', ['as' => 'admin_login_get', 'uses' => 'AdminController@admin_login_get']);
    Route::post('login', ['as' => 'admin_login_post', 'uses' => 'AdminController@admin_login_post']);

    Route::get('logout', ['as' => 'admin_logout', 'uses' => 'AdminController@admin_logout']);

    Route::get('dashboard', ['as' => 'admin_dashboard', 'uses' => 'AdminController@admin_dashboard']);

    Route::get('dashboard_data', ['as' => 'get_dashboard_data', 'uses' => 'AdminController@get_dashboard_data']);


    Route::get('profile/update', ['as' => 'aprofile_update_get', 'uses' => 'AdminController@aprofile_update_get']);
    Route::post('profile/update', ['as' => 'aprofile_update_post', 'uses' => 'AdminController@aprofile_update_post']);

    Route::get('password/update', ['as' => 'apassword_update_get', 'uses' => 'AdminController@apassword_update_get']);
    Route::post('password/update', ['as' => 'apassword_update_post', 'uses' => 'AdminController@apassword_update_post']);

//////////////////////////		Categories  		//////////////////////////
    Route::group(['prefix' => 'category'], function() {
        
        Route::get('export', ['as' => 'admin_category_export', 'uses' => 'AdminCategoryController@admin_category_export']);
        
        Route::get('all', ['as' => 'admin_category_all', 'uses' => 'AdminCategoryController@admin_category_all']);

        Route::get('add', ['as' => 'admin_category_add_get', 'uses' => 'AdminCategoryController@admin_category_add_get']);
        Route::post('add', ['as' => 'admin_category_add_post', 'uses' => 'AdminCategoryController@admin_category_add_post']);

        /* Below two routes are common for both categories and sub-categories */
        Route::get('delete/{id}', ['as' => 'admin_category_delete', 'uses' => 'AdminCategoryController@admin_category_delete']);
        Route::get('block/{id}/{id2}', ['as' => 'admin_category_block', 'uses' => 'AdminCategoryController@admin_category_block']);

        Route::get('edit/{id}', ['as' => 'admin_category_edit', 'uses' => 'AdminCategoryController@admin_category_edit']);
        Route::post('update', ['as' => 'admin_category_update', 'uses' => 'AdminCategoryController@admin_category_update']);

        ///////////////////// get categories features ///////////////////////
        Route::get('features/{cat_id}/{product_id}', ['as' => 'admin_cat_features', 'uses' => 'AdminCategoryController@admin_cat_features']);
        Route::get('servicefeatures/{cat_id}/{service_id}', ['as' => 'admin_cat_features_service', 'uses' => 'AdminCategoryController@admin_cat_features_service']);
    });
    /////////////////////////////////////     Categories      ///////////////////////
    //////////////    Sub Cats Routes     /////////////////////////// 
    Route::group(['prefix' => 'subcat'], function() {

        Route::get('all', ['as' => 'admin_subcategory_all', 'uses' => 'AdminCategoryController@admin_subcategory_all']);

        Route::get('add', ['as' => 'admin_subcat_add_get', 'uses' => 'AdminCategoryController@admin_subcat_add_get']);
        Route::post('add', ['as' => 'admin_subcat_add_post', 'uses' => 'AdminCategoryController@admin_subcat_add_post']);
        Route::get('edit/{id}', ['as' => 'admin_subcat_edit', 'uses' => 'AdminCategoryController@admin_subcat_edit']);
        Route::post('update', ['as' => 'admin_subcat_update', 'uses' => 'AdminCategoryController@admin_subcat_update']);
    }); //////////////		Sub Cats Routes 		///////////////////////////
    //////////////   Products Routes     ///////////////////////////

    Route::group(['prefix' => 'product'], function() {

        Route::get('all', ['as' => 'admin_product_all', 'uses' => 'AdminProductController@admin_product_all']);
        Route::get('add', ['as' => 'admin_product_add_get', 'uses' => 'AdminProductController@admin_product_add_get']);
        Route::post('add', ['as' => 'admin_product_add_post', 'uses' => 'AdminProductController@admin_product_add_post']);
        Route::get('edit/{id}', ['as' => 'admin_product_edit', 'uses' => 'AdminProductController@admin_product_edit']);
        Route::post('update', ['as' => 'admin_product_update', 'uses' => 'AdminProductController@admin_product_update']);

        Route::get('delete/{id}', ['as' => 'admin_product_delete', 'uses' => 'AdminProductController@admin_product_delete']);
        Route::get('block/{id}/{id2}', ['as' => 'admin_product_block', 'uses' => 'AdminProductController@admin_product_block']);

    /* Manage Gallery */
        Route::get('addGallery/{id}', ['as' => 'admin_product_addGallery_get', 'uses' => 'AdminProductController@admin_product_addGallery_get']);
        Route::post('addGallery', ['as' => 'admin_product_addGallery_post', 'uses' => 'AdminProductController@admin_product_addGallery_post']);

        Route::get('gdelete/{id}', ['as' => 'admin_pgallery_delete', 'uses' => 'AdminProductController@admin_pgallery_delete']);
        Route::get('gblock/{id}/{id2}', ['as' => 'admin_pgallery_block', 'uses' => 'AdminProductController@admin_pgallery_block']);
    });
//////////////   Products Routes     ///////////////////////////
    //////////////   Services Routes     ///////////////////////////

    Route::group(['prefix' => 'service'], function() {

        Route::get('all', ['as' => 'admin_service_all', 'uses' => 'AdminServiceController@admin_service_all']);
        Route::get('add', ['as' => 'admin_service_add_get', 'uses' => 'AdminServiceController@admin_service_add_get']);
        Route::post('add', ['as' => 'admin_service_add_post', 'uses' => 'AdminServiceController@admin_service_add_post']);
        Route::get('edit/{id}', ['as' => 'admin_service_edit', 'uses' => 'AdminServiceController@admin_service_edit']);
        Route::post('update', ['as' => 'admin_service_update', 'uses' => 'AdminServiceController@admin_service_update']);

        Route::get('delete/{id}', ['as' => 'admin_service_delete', 'uses' => 'AdminServiceController@admin_service_delete']);
        Route::get('block/{id}/{id2}', ['as' => 'admin_service_block', 'uses' => 'AdminServiceController@admin_service_block']);

        /* Manage Gallery */
        Route::get('addGallery/{id}', ['as' => 'admin_service_addGallery_get', 'uses' => 'AdminServiceController@admin_service_addGallery_get']);
        Route::post('addGallery', ['as' => 'admin_service_addGallery_post', 'uses' => 'AdminServiceController@admin_service_addGallery_post']);
    });
//////////////   Services Routes     ///////////////////////////
    //////////////   Business Routes     ///////////////////////////

    Route::group(['prefix' => 'business'], function() {
        
        Route::get('share_credentials/{id}', ['as' => 'admin_share_credentials', 'uses' => 'AdminBusinessController@admin_share_credentials']); 
         
        Route::get('all', ['as' => 'admin_business_all', 'uses' => 'AdminBusinessController@admin_business_all']);
        Route::get('add', ['as' => 'admin_business_add_get', 'uses' => 'AdminBusinessController@admin_business_add_get']);
        Route::post('add', ['as' => 'admin_business_add_post', 'uses' => 'AdminBusinessController@admin_business_add_post']);
        Route::get('edit/{id}', ['as' => 'admin_business_edit', 'uses' => 'AdminBusinessController@admin_business_edit']);
        Route::post('update', ['as' => 'admin_business_update', 'uses' => 'AdminBusinessController@admin_business_update']);

        Route::get('delete/{id}', ['as' => 'admin_business_delete', 'uses' => 'AdminBusinessController@admin_business_delete']);
        Route::get('block/{id}/{id2}', ['as' => 'admin_business_block', 'uses' => 'AdminBusinessController@admin_business_block']);

        /* Manage Gallery */
        Route::get('addGallery/{id}', ['as' => 'admin_business_addGallery_get', 'uses' => 'AdminBusinessController@admin_business_addGallery_get']);
        Route::post('addGallery', ['as' => 'admin_business_addGallery_post', 'uses' => 'AdminBusinessController@admin_business_addGallery_post']);
    });
//////////////   Business Routes     ///////////////////////////
     
    Route::group(['prefix' => 'user'], function() {
        Route::get('all', ['as' => 'admin_user_all', 'uses' => 'AdminUserController@admin_user_all']);
        
        Route::get('add', ['as' => 'admin_user_add_get', 'uses' => 'AdminUserController@admin_user_add_get']);
        Route::post('add', ['as' => 'admin_user_add_post', 'uses' => 'AdminUserController@admin_user_add_post']);
        Route::get('edit/{id}', ['as' => 'admin_user_edit', 'uses' => 'AdminUserController@admin_user_edit']);
        Route::get('view/{id}', ['as' => 'admin_user_view', 'uses' => 'AdminUserController@admin_user_view']);
        
        Route::post('update', ['as' => 'admin_user_update', 'uses' => 'AdminUserController@admin_user_update']);

        Route::get('delete/{id}', ['as' => 'admin_user_delete', 'uses' => 'AdminUserController@admin_user_delete']);
        Route::get('block/{id}/{id2}', ['as' => 'admin_user_block', 'uses' => 'AdminUserController@admin_user_block']);
    });
    Route::group(['prefix' => 'businessuser'], function() {
    Route::get('all', ['as' => 'admin_b_user_all', 'uses' => 'AdminUserController@admin_b_user_all']);
        
    Route::get('add', ['as' => 'admin_b_user_add_get', 'uses' => 'AdminUserController@admin_b_user_add_get']);
    Route::post('add', ['as' => 'admin_b_user_add_post', 'uses' => 'AdminUserController@admin_b_user_add_post']);
    Route::get('edit/{id}', ['as' => 'admin_b_user_edit', 'uses' => 'AdminUserController@admin_b_user_edit']);
    Route::get('view/{id}', ['as' => 'admin_b_user_view', 'uses' => 'AdminUserController@admin_b_user_view']);        
    Route::post('update', ['as' => 'admin_b_user_update', 'uses' => 'AdminUserController@admin_b_user_update']);
        
    });
    Route::group(['prefix' => 'subscription'], function() {

       
        
        /* Subscriptions Purchases URLs start*/
        Route::get('allsales', ['as' => 'admin_sales_all', 'uses' => 'AdminSubscriptionController@admin_sales_all']);
        Route::get('saledelete/{id}', ['as' => 'admin_sale_delete', 'uses' => 'AdminSubscriptionController@admin_sale_delete']);
        Route::get('saleview/{id}', ['as' => 'admin_sale_view', 'uses' => 'AdminSubscriptionController@admin_sale_view']);
        /* Subscriptions Purchases URLs end*/
        
        Route::get('all', ['as' => 'admin_subscription_all', 'uses' => 'AdminSubscriptionController@admin_subscription_all']);
        Route::get('delete/{id}', ['as' => 'admin_subscription_delete', 'uses' => 'AdminSubscriptionController@admin_subscription_delete']);
        Route::get('block/{id}/{id2}', ['as' => 'admin_subscription_block', 'uses' => 'AdminSubscriptionController@admin_subscription_block']);


        Route::get('add', ['as' => 'admin_subscription_add_get', 'uses' => 'AdminSubscriptionController@admin_subscription_add_get']);
        Route::post('add', ['as' => 'admin_subscription_add_post', 'uses' => 'AdminSubscriptionController@admin_subscription_add_post']);
        Route::get('edit/{id}', ['as' => 'admin_subscription_edit', 'uses' => 'AdminSubscriptionController@admin_subscription_edit']);
        Route::post('update', ['as' => 'admin_subscription_update', 'uses' => 'AdminSubscriptionController@admin_subscription_update']);
    });

    /* Route::get('push', ['as' => 'push_notifications_get', 'uses' => 'AdminOtherController@push_notifications_get']);
      Route::post('push', ['as' => 'push_notifications_post', 'uses' => 'AdminOtherController@push_notifications_post']); */
});

/********************       Business Panel Routes          ********************************/
Route::group(['prefix' => 'bpanel', 'namespace' => 'Business', 'middleware' => 'business_middleware'], function() {
    Route::get('/', 'BusinessController@business_dashboard')->name('business_dashboard');
    
    Route::get('login', ['as' => 'business_login_get', 'uses' => 'BusinessController@business_login_get']);
    Route::post('login', ['as' => 'business_login_post', 'uses' => 'BusinessController@business_login_post']);
    Route::get('logout', ['as' => 'business_logout', 'uses' => 'BusinessController@business_logout']);
    
    /*Route::get('dashboard', ['as' => 'business_dashboard', 'uses' => 'BusinessController@business_dashboard']);*/
    Route::get('dashboard', ['as' => 'business_dashboard', 'uses' => 'AdminBusinessController@business_business_all']);
    
    
    
 
    Route::get('profile/update', ['as' => 'bprofile_update_get', 'uses' => 'BusinessController@bprofile_update_get']);
    Route::post('profile/update', ['as' => 'bprofile_update_post', 'uses' => 'BusinessController@bprofile_update_post']);

    Route::get('password/update', ['as' => 'bpassword_update_get', 'uses' => 'BusinessController@bpassword_update_get']);
    Route::post('password/update', ['as' => 'bpassword_update_post', 'uses' => 'BusinessController@bpassword_update_post']);
    
    Route::get('subscribe', ['as' => 'business_subscriptions', 'uses' => 'BusinessController@business_subscriptions']);
    Route::post('subscribe', ['as' => 'business_subscriptions_post', 'uses' => 'BusinessController@business_subscriptions_post']);
   
    Route::post('payment', ['as' => 'business_payment_post', 'uses' => 'BusinessController@business_payment_post']);
    
    
    ///////////////// Business Panel get categories features ///////////////////////
    Route::get('features/{cat_id}/{product_id}', ['as' => 'business_cat_features', 'uses' => 'BusinessController@business_cat_features']);
    Route::get('servicefeatures/{cat_id}/{service_id}', ['as' => 'business_cat_features_service', 'uses' => 'BusinessController@business_cat_features_service']); 
     
    //////////////   Products Routes     ///////////////////////////

    Route::group(['prefix' => 'product'], function() {

        Route::get('all', ['as' => 'business_product_all', 'uses' => 'AdminProductController@admin_product_all']);
        Route::get('add', ['as' => 'business_product_add_get', 'uses' => 'AdminProductController@admin_product_add_get']);
        Route::post('add', ['as' => 'business_product_add_post', 'uses' => 'AdminProductController@admin_product_add_post']);
        Route::get('edit/{id}', ['as' => 'business_product_edit', 'uses' => 'AdminProductController@admin_product_edit']);
        Route::post('update', ['as' => 'business_product_update', 'uses' => 'AdminProductController@admin_product_update']);

        Route::get('delete/{id}', ['as' => 'business_product_delete', 'uses' => 'AdminProductController@admin_product_delete']);
        Route::get('block/{id}/{id2}', ['as' => 'business_product_block', 'uses' => 'AdminProductController@admin_product_block']);

    /* Manage Gallery */
        Route::get('addGallery/{id}', ['as' => 'business_product_addGallery_get', 'uses' => 'AdminProductController@admin_product_addGallery_get']);
        Route::post('addGallery', ['as' => 'business_product_addGallery_post', 'uses' => 'AdminProductController@admin_product_addGallery_post']);

        Route::get('gdelete/{id}', ['as' => 'business_pgallery_delete', 'uses' => 'AdminProductController@admin_pgallery_delete']);
        Route::get('gblock/{id}/{id2}', ['as' => 'business_pgallery_block', 'uses' => 'AdminProductController@admin_pgallery_block']);
    });
//////////////   Products Routes     ///////////////////////////
    //////////////   Services Routes     ///////////////////////////

    Route::group(['prefix' => 'service'], function() {

        Route::get('all', ['as' => 'business_service_all', 'uses' => 'AdminServiceController@admin_service_all']);
        Route::get('add', ['as' => 'business_service_add_get', 'uses' => 'AdminServiceController@admin_service_add_get']);
        Route::post('add', ['as' => 'business_service_add_post', 'uses' => 'AdminServiceController@admin_service_add_post']);
        Route::get('edit/{id}', ['as' => 'business_service_edit', 'uses' => 'AdminServiceController@admin_service_edit']);
        Route::post('update', ['as' => 'business_service_update', 'uses' => 'AdminServiceController@admin_service_update']);

        Route::get('delete/{id}', ['as' => 'business_service_delete', 'uses' => 'AdminServiceController@admin_service_delete']);
        Route::get('block/{id}/{id2}', ['as' => 'business_service_block', 'uses' => 'AdminServiceController@admin_service_block']);

        /* Manage Gallery */
        Route::get('addGallery/{id}', ['as' => 'business_service_addGallery_get', 'uses' => 'AdminServiceController@admin_service_addGallery_get']);
        Route::post('addGallery', ['as' => 'business_service_addGallery_post', 'uses' => 'AdminServiceController@admin_service_addGallery_post']);
    });
//////////////   Services Routes     ///////////////////////////
    //////////////   Business Routes     ///////////////////////////

    Route::group(['prefix' => 'business'], function() {        
        Route::get('all', ['as' => 'business_business_all', 'uses' => 'AdminBusinessController@business_business_all']);
        Route::get('add', ['as' => 'business_business_add_get', 'uses' => 'AdminBusinessController@business_business_add_get']);
        Route::post('add', ['as' => 'business_business_add_post', 'uses' => 'AdminBusinessController@business_business_add_post']);
        Route::get('edit/{id}', ['as' => 'business_business_edit', 'uses' => 'AdminBusinessController@business_business_edit']);
        Route::post('update', ['as' => 'business_business_update', 'uses' => 'AdminBusinessController@business_business_update']);
        Route::get('delete/{id}', ['as' => 'business_business_delete', 'uses' => 'AdminBusinessController@business_business_delete']);
        Route::get('block/{id}/{id2}', ['as' => 'business_business_block', 'uses' => 'AdminBusinessController@business_business_block']);
        /* Manage Gallery */
        Route::get('addGallery/{id}', ['as' => 'business_business_addGallery_get', 'uses' => 'AdminBusinessController@business_business_addGallery_get']);
        Route::post('addGallery', ['as' => 'business_business_addGallery_post', 'uses' => 'AdminBusinessController@business_business_addGallery_post']);
        Route::get('gdelete/{id}', ['as' => 'business_bgallery_delete', 'uses' => 'AdminBusinessController@business_bgallery_delete']);
        Route::get('gblock/{id}/{id2}', ['as' => 'business_bgallery_block', 'uses' => 'AdminBusinessController@business_bgallery_block']);
    });
//////////////   Business Routes     ///////////////////////////
    
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/auth/{provider}', 'Auth\AuthController@redirectToProvider');
//Route::get('/auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
Route::prefix('facebook')->group(function() {
    Route::get('/redirect', 'SocialAuth\FacebookController@redirect');
    Route::get('/callback', 'SocialAuth\FacebookController@callback');
});

Route::prefix('google')->group(function() {
    Route::get('/redirect', 'SocialAuth\GooglePlusController@redirect');
    Route::get('/callback', 'SocialAuth\GooglePlusController@callback');
});
Route::prefix('twitter')->group(function() {
    Route::get('/redirect', 'SocialAuth\TwitterController@redirect');
    Route::get('/callback', 'SocialAuth\TwitterController@callback');
});
Route::prefix('linkedin')->group(function() {
    Route::get('/redirect', 'SocialAuth\LinkedinController@redirect');
    Route::get('/callback', 'SocialAuth\LinkedinController@callback');
});

