<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */



/*
  |--------------------------------------------------------------------------
  | Mobile Application
  |--------------------------------------------------------------------------
 */
/* Route::post('auth/register', 'Api\AuthController@register');
  Route::post('auth/login', 'Api\AuthController@login');
  Route::post('auth/refresh', 'Api\AuthController@refreshToken'); */
Route::post('auth/refresh', 'Api\AuthController@refreshToken');
Route::post('auth/login', 'Api\AuthController@login');
// 'middleware' => ['swfix', 'log_middleware']
Route::group(['prefix' => 'v1', 'namespace' => 'Api', 'middleware' => ['lang', 'log_middleware']], function() {

    Route::post('app_test_api/', ['middleware' => 'auth:api', 'as' => 'app_test_api', 'uses' => 'CommonController@app_test_api']);

    Route::post('app_social_login', ['as' => 'app_social_login', 'uses' => 'UserController@app_social_login']);

    Route::post('app_register', ['as' => 'app_register', 'uses' => 'UserController@app_register']);

    Route::post('app_login', ['as' => 'app_login', 'uses' => 'UserController@app_login']);
//

    Route::group(['middleware' => ['swfix', 'auth:api', 'app_auth']], function() {
        Route::post('app_data', ['as' => 'app_data', 'uses' => 'HomeController@app_data']);


        Route::post('notification_update', ['as' => 'notification_update', 'uses' => 'UserController@notification_update']);

        Route::post('password_update', ['as' => 'password_update', 'uses' => 'UserController@password_update']);
        Route::post('password_create', ['as' => 'password_create', 'uses' => 'UserController@password_create']);
        Route::post('my_profile', ['as' => 'my_profile', 'uses' => 'UserController@my_profile']);
        Route::post('edit_profile', ['as' => 'edit_profile', 'uses' => 'UserController@edit_profile']);

        Route::post('app_logout', ['as' => 'app_logout', 'uses' => 'UserController@app_logout']);
    });

    /* Indivisual URL's Start here */
    Route::get('get_main_cat', ['as' => 'get_main_cat', 'uses' => 'CategoryController@get_main_cat']);
    Route::get('get_review_day', ['as' => 'get_review_day', 'uses' => 'ReviewController@get_review_day']);
    Route::get('most_reviewed_busineess', ['as' => 'most_reviewed_busineess', 'uses' => 'BusinessController@most_reviewed_busineess']);

    Route::group(['middleware' => ['vToken']], function() {
        Route::post('get_nearByBusiness_home', ['as' => 'get_nearByBusiness_home', 'uses' => 'BusinessController@get_nearByBusiness_home']);
        
          Route::post('get_nearByBusiness_home_search', ['as' => 'get_nearByBusiness_home_search', 'uses' => 'BusinessController@get_nearByBusiness_home_search']);
        
         Route::post('most_reviewed_busineess', ['as' => 'most_reviewed_busineess', 'uses' => 'BusinessController@most_reviewed_busineess']);
         
         Route::post('most_reviewed_busineess_search', ['as' => 'most_reviewed_busineess_search', 'uses' => 'BusinessController@most_reviewed_busineess_search']); 
         
        Route::post('follow', ['as' => 'follow', 'uses' => 'FollowController@get_follow']);
        Route::post('get_business_details', ['as' => 'get_business_details', 'uses' => 'BusinessController@get_business_details']);
        
         Route::post('get_product_details', ['as' => 'get_product_details', 'uses' => 'ProductController@get_product_details']);
        
        Route::post('get_all_reviews', ['as' => 'get_all_reviews', 'uses' => 'ReviewController@get_all_reviews']);
        Route::post('get_all_faqs', ['as' => 'get_all_faqs', 'uses' => 'FaqController@get_all_faqs']);
        Route::post('faq_details', ['as' => 'faq_details', 'uses' => 'FaqController@faq_details']);
        
    });
         

    /*  Route::post('get_nearByBusiness_home',['as'=>'get_nearByBusiness_home','uses'=>'BusinessController@get_nearByBusiness_home']); */

    /* Indivisual URL's End here */
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
