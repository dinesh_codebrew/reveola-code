<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Authentication Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used during authentication for various
      | messages that we need to display to the user. You are free to modify
      | these language lines according to your application's requirements.
      |
     */

    'email_exist' => 'Sorry, this email is already registered with us. Please login to continue.',
    'phone_exist' => 'This phone number is already used.',
    'signup_success' => 'Sign up successfully',
   
    'Name' => 'Name',
    'E-Mail Address' => 'E-Mail Address',
    'Password' => 'Password',
    'Phone' => 'Phone',
    'Confirm Password' => 'Confirm Password',
    'Register' => 'Register',
    'Login' => 'Login',
    'Logout' => 'Logout',
    'Forgot Your Password?' => 'Forgot Your Password?',
    'Remember Me' => 'Remember Me',
    
    'Login_success' => 'Login successfully.',
    'Login_fail' => 'Wrong username/password combination.',
    
    'Provider_required' => 'Please choose one of social media for login.',
    'Provider_unique' => 'Please use different credentials.',
    'Provider_user_required' => 'There are something gone wrong, please try again.',
    'register_user_msg' => 'Already Registered User.',
   
    'oldPassword_notMatch'=>'Old password do not match. Please enter correct password.',
    'Password_update'=>'Password updated successfully.',
    'logout_success'=>'Logged out successfully.',
    'password_created'=>'Password created successfully.', 
    'Profile_update'=>'Profile updated successfully.',    
    /*above done in both lang files*/
];
