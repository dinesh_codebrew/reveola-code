<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'email_exist' => 'هذا البريد الإلكتروني مستخدم بالفعل.',
    'phone_exist' => 'رقم الهاتف هذا مستخدم بالفعل.',
    'signup_success'=> 'اشترك بنجاح',
    'Phone'=> 'هاتف',
    'Name'=> 'اسم',
    'E-Mail Address'=> 'عنوان البريد الإلكتروني',
    'Password'=> 'كلمه السر', 
    'Confirm Password'=> 'تأكيد كلمة المرور',
    'Register'=> 'تسجيل',
    'Login'=> 'تسجيل الدخول',
    'Logout'=> 'الخروج',
    'Forgot Your Password?'=> 'نسيت رقمك السري',
    'Remember Me'=> 'تذكرنى',
    
    'Login_success' => 'تسجيل الدخول بنجاح.',
    'Login_fail' => 'تركيبة خاطئة لاسم المستخدم / كلمة المرور.',
    
    'Provider_required' => 'يرجى اختيار واحد من وسائل الاعلام الاجتماعية لتسجيل الدخول.',
    'Provider_unique' => 'يرجى استخدام بيانات اعتماد مختلفة.',
    'Provider_user_required' => 'حدث خطأ ما ، يرجى المحاولة مرة أخرى.',
    'register_user_msg' => 'مستخدم مسجل بالفعل.',
    
    'oldPassword_notMatch'=>'كلمة المرور القديمة لا تتطابق. يرجى إدخال كلمة المرور الصحيحة.',
    'Password_update'=>'تم تحديث كلمة السر بنجاح.',
    'logout_success'=>'تم تسجيل الخروج بنجاح.',
    'password_created'=>'كلمة المرور تم إنشاؤها بنجاح.',    
    'Profile_update'=>'تحديث الملف بنجاح',
];

