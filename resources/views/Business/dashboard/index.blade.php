@extends('layouts.Admin.dashboard')
@section('title', 'Dashboard')
@section('content')
<div class="row">
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-md-center">
                    <i class="icon-user-following icon-lg text-success"></i>
                    <div class="ml-3">
                        <p class="mb-0">Total Users</p>
                        <h6> </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-md-center">
                    <i class="mdi mdi-rocket icon-lg text-warning"></i>
                    <div class="ml-3">
                        <p class="mb-0">Main Categories</p>
                        <h6> </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-md-center">
                    <i class="mdi mdi-diamond icon-lg text-info"></i>
                    <div class="ml-3">
                        <p class="mb-0">Sub-Categories</p>
                        <h6> </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-md-center">
                    <i class="mdi mdi-chart-line-stacked icon-lg text-danger"></i>
                    <div class="ml-3">
                        <p class="mb-0">Total Subscriptions </p>
                        <h6> </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body pb-0">
                <h6 class="card-title">Reviews</h6>
                <div class="row">
                    <div class="col-12">
                        <div class="wrapper border-bottom py-2">
                            <div class="d-flex">
                                <img class="img-sm rounded-circle" src="{{ asset('/admin/images/faces/face4.jpg') }}" alt="" />
                                <div class="wrapper ml-4">
                                    <p class="mb-0">Sarah Graves</p>
                                    <small class="text-muted mb-0">Awesome!!! Highly recommend</small>
                                </div>
                                <div class="rating ml-auto d-flex align-items-center">
                                    <select id="dashboard-rating-2" name="rating">
                                        <option value="1" />1
                                        <option value="2" />2
                                        <option value="3" />3
                                        <option value="4" />4
                                        <option value="5" />5
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="wrapper border-bottom py-2">
                            <div class="d-flex">
                                <img class="img-sm rounded-circle" src="{{ asset('/admin/images/faces/face5.jpg') }}" alt="" />
                                <div class="wrapper ml-4">
                                    <p class="mb-0">David Grey</p>
                                    <small class="text-muted mb-0">Not satisfied with the service.</small>
                                </div>
                                <div class="rating ml-auto d-flex align-items-center">
                                    <select id="dashboard-rating-1" name="rating">
                                        <option value="1" />1
                                        <option value="2" />2
                                        <option value="3" />3
                                        <option value="4" />4
                                        <option value="5" />5
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="wrapper border-bottom py-2">
                            <div class="d-flex">
                                <img class="img-sm rounded-circle" src="{{ asset('/admin/images/faces/face5.jpg') }}" alt="" />
                                <div class="wrapper ml-4">
                                    <p class="mb-0">Burno mars</p>
                                    <small class="text-muted mb-0">Great!! It's the best</small>
                                </div>
                                <div class="rating ml-auto d-flex align-items-center">
                                    <select id="dashboard-rating-3" name="rating">
                                        <option value="1" />1
                                        <option value="2" />2
                                        <option value="3" />3
                                        <option value="4" />4
                                        <option value="5" />5
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="wrapper py-2">
                            <div class="d-flex">
                                <img class="img-sm rounded-circle" src="{{ asset('/admin/images/faces/face4.jpg') }}" alt="" />
                                <div class="wrapper ml-4">
                                    <p class="mb-0">Dobrick</p>
                                    <small class="text-muted mb-0">Not worth the money.</small>
                                </div>
                                <div class="rating ml-auto d-flex align-items-center">
                                    <select id="dashboard-rating-4" name="rating">
                                        <option value="1" />1
                                        <option value="2" />2
                                        <option value="3" />3
                                        <option value="4" />4
                                        <option value="5" />5
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 stretch-card">
        <div class="row flex-grow">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title mb-0">Pending Requests</h6>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-inline-block pt-3">
                                <div class="d-lg-flex">
                                    <h2 class="mb-0">25</h2>
                                    <div class="d-flex align-items-center ml-lg-2"> 
                                        <small class="ml-1 mb-0"></small>
                                    </div>
                                </div>
                                <small class="text-gray">Reuests for products/services from register/anonymous users .</small>
                            </div>
                            <div class="d-inline-block">
                                <div class="bg-success px-3 px-md-4 py-2 rounded">
                                    <i class="mdi mdi-buffer text-white icon-lg"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title mb-0">Business Claims</h6>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-inline-block pt-3">
                                <div class="d-lg-flex">
                                    <h2 class="mb-0">10</h2>
                                    <div class="d-flex align-items-center ml-lg-2"> 
                                        <small class="ml-1 mb-0"></small>
                                    </div>
                                </div>
                                <small class="text-gray">Business claim requests from business owners.</small>
                            </div>
                            <div class="d-inline-block">
                                <div class="bg-warning px-3 px-md-4 py-2 rounded">
                                    <i class="mdi mdi-wallet text-white icon-lg"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">All Users</h4>
                <canvas id="lineChart1" style="max-height: 300px"></canvas>
            </div>
        </div>
    </div>
</div> 
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Total Subscriptions</h4>
                <canvas id="barChart" style="max-height: 300px"></canvas>
            </div>
        </div>
    </div>
</div> 
	<script src="{{ asset('admin/node_modules/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>
	<script src="{{ asset('admin/node_modules/chart.js/dist/Chart.min.js') }}"></script>
	<script src="{{ asset('admin/node_modules/raphael/raphael.min.js') }}"></script>
	<script src="{{ asset('admin/node_modules/morris.js/morris.min.js') }}"></script>
	<script src="{{ asset('admin/node_modules/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
<script>
    $(document).ready(function () {
        new_get_dashboard_data();
        var users,user_year, sales = new Array();
        function new_get_dashboard_data()
        {

            /* dt = document.getElementById('daterange').value;
             yr = document.getElementById('starting_date').value;
             dt = encodeURIComponent(dt);*/
            route = '{!! route('get_dashboard_data') !!}';
            $.ajax({
                url: route,
                type: 'GET',
                async: true,
                dataType: "json",
                success: function (data)
                {
                    if (data.success == '0')
                    {
                       console.log(data.msg)
                    } else
                    {
                     //   users = data.graph_data.users;  
                       $.each(users,function(index,val){
                          user_year[index] = val.year ;
                       });
                       
                        /*if ( !( 'bar' in foo ) ) {
                            foo['bar'] = 42;
                        }*/
                      //  sales = data.graph_data.sales;
                    }
                }
            });

        }

        var multiLineData = {
            labels: ["2014", "2015", "2016", "2017", "2018"],
            datasets: [{
                    label: 'Users',
                    data: [3, 0, 10, 22, 3],
                    borderColor: [
                        '#587ce4'
                    ],
                    borderWidth: 2,
                    fill: true
                },
                {
                    label: 'Business Users',
                    data: [5, 5, 23, 12, 23],
                    borderColor: [
                        '#ede190'
                    ],
                    borderWidth: 2,
                    fill: true
                }
            ]
        };
        var options = {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            },
            legend: {
                display: false
            },
            elements: {
                point: {
                    radius: 0
                }
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            }

        };

        if ($("#lineChart1").length) {
            var lineChartCanvas = $("#lineChart1").get(0).getContext("2d");
            var lineChart = new Chart(lineChartCanvas, {
                type: 'line',
                data: multiLineData,
                options: options
            });
        }

        var data = {
            labels: ["2014", "2015", "2016", "2017", "2018"],
            datasets: [{
                    label: '# of Subscriptions',
                    data: [19, 3, 5, 2, 3],
                    backgroundColor: [
                        /* 'rgba(255, 99, 132, 0.2)',*/
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        /*'rgba(255,99,132,1)',*/
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
        };
        if ($("#barChart").length) {
            var barChartCanvas = $("#barChart").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var barChart = new Chart(barChartCanvas, {
                type: 'bar',
                data: data,
                options: options
            });
        }
    });
</script>
<!--<script src="{ { asset('admin/js/chart.js') }}"></script>-->
@endsection
