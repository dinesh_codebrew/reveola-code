@extends('layouts.Admin.dashboard')
@section('title', 'Dashboard')
@section('content')
  <link rel="stylesheet" href="{{ asset('/admin/node_modules/owl-carousel-2/assets/owl.carousel.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/admin/node_modules/owl-carousel-2/assets/owl.theme.default.min.css') }}" />
<div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div class="container text-center pt-5">
                    <h4 class="mb-3">Start up your Bussiness today</h4><!-- mt-5-->
                    <p class="w-75 mx-auto mb-5">Choose a plan that suits you the best. If you are not fully satisfied, we offer 30-day money-back guarantee no questions asked!!</p>
                    <div class="row pricing-table">
                      
                    @if(count($subscriptions)>0)
                      @foreach($subscriptions as $data)
                      <div class="col-md-4 grid-margin stretch-card pricing-card">
                        <div class="card border border-success pricing-card-body">
                          <div class="text-center pricing-card-head">
                            <h3 class="text-success">{{ $data->sub_name }}</h3>
                            <p>{{ $data->sub_tag_line }}</p>
                            <h1 class="font-weight-normal mb-4">${{ $data->sub_price  }}</h1>
                          </div>
                          <ul class="list-unstyled plan-features">
                             <?php $details=  []; ?> 
                             <?php $details=  explode(',', $data->sub_desc); ?> 
                              @if(count($details)>0)
                              @foreach($details as $detail)
                              <li>{{ $detail }}</li>
                              @endforeach
                              @else
                            <li>{{ $data->sub_desc  }}</li>
                            @endif
                          </ul>
                          <div class="wrapper">
                            <form method='post' action="{{ route('business_subscriptions_post') }}">                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type='hidden' name="id" value="{{ $data->id }}">
                                <button type='submit' class="btn btn-success btn-block">Subscribe</button>
                            </form>
                            
                          </div>
                          <p class="mt-3 mb-0 plan-cost text-success"></p>
                        </div>
                      </div>
                      @endforeach
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
</div>
@endsection

      