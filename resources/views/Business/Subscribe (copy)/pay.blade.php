@extends('layouts.Admin.dashboard')
@if(isset($service))
@section('title', 'Payment')
@else
@section('title', 'Payment')
@endif

@section('content')  
<link rel="stylesheet" href="{{ asset('admin/node_modules/lightgallery/dist/css/lightgallery.min.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
<div class="row flex-grow">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">


                <div class="row">
                    <div class="col-6">
                        <h4 class="card-title">Payment</h4>
                    </div> 
                    <div class="col-6">
                        <p class="page-description"><a style="float: right;" href="{{ route('business_subscriptions') }}" class="btn btn-primary">Cancel</a></p> 
                    </div>
                </div>
                <p class="card-description"></p>

                <form action="{{ route('business_payment_post') }}" method="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <script
                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                        data-key="pk_test_E2jyveEGJxBmi0epjnOJgHUJ"
                        data-amount="999"
                        data-name="John Carter"
                        data-description="Example charge"
                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                        data-locale="auto">
                    </script>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<!--
Array
(
    [_token] => 4YmoY8X5fsX8J76a30BLJFuHZe4sE5feeKmZUOHG
    [stripeToken] => tok_1Ccp6VFkVNOlLMbCxld17rTe
    [stripeTokenType] => card
    [stripeEmail] => test1@gmail.com
    [timezonez] => +05:30
    [timezone] => Asia/Kolkata
)
-->