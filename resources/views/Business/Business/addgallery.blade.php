@extends('layouts.Admin.dashboard')
@if(isset($service))
@section('title', 'Update Business Gallery')
@else
@section('title', 'Add Business Gallery')
@endif

@section('content')  
<link rel="stylesheet" href="{{ asset('admin/node_modules/lightgallery/dist/css/lightgallery.min.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
<div class="row flex-grow">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                
                
                 <div class="row">
                        <div class="col-6">
                  <h4 class="card-title">@if(isset($business)){{'Update' }}@else {{ 'Add New' }} @endif Business Gallery</h4>
                        </div> 
                        <div class="col-6">
                            <p class="page-description"><a style="float: right;" href="{{ route('business_business_edit',$business->id) }}" class="btn btn-primary">Back</a></p> 
                        </div>
                    </div>
                <p class="card-description"></p>
                @if(isset($business))
                <form class="forms-sample" method="post" action="{{route('business_business_addGallery_post')}}" enctype="multipart/form-data" id="form"> 
                    <input type="hidden" name="business_id" value="{{ $business->id }}">
                    @else
                    <form class="forms-sample" method="post" action="{{route('business_business_addGallery_post')}}" enctype="multipart/form-data" id="form">  
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label>Gallery Image</label>
                            <input type="file" name="name" class="file-upload-default" />
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image" />
                                <span class="input-group-btn">
                                    <button class="file-upload-browse btn btn-info" type="button">Browse</button>
                                </span>
                            </div>
                        </div> 
                        <button type="submit"  class="btn btn-success mr-2">Submit</button>
                        <button  type="button"  onClick="this.form.reset()" class="btn btn-light">Cancel</button>
                    </form> 
            </div>

        </div>
    </div>
</div>
<div class="row grid-margin"> 

<!-- gallery table start-->
       <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                  <h4 class="card-title">All Images</h4>          
                        </div>  
                    </div>
                  
                  
                  <div class="row">
                    <div class="table-sorter-wrapper col-lg-12 table-responsive">
                      <table id="sortable-table-2" class="table table-striped">
                        <thead>
                          <tr>  
                                <th>Name</th> 
                                <th>Created At</th>
                                <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(count($gallery)>0) 
                        @foreach($gallery as $image) 
                            <tr>  
                                <td style="width: 50% !important;" class="lightgallery" ><a class="image-tile" href="{{ env('FILE_URL').$image->name }}"><img style="height:100px;width:100px;border-radius: 0px !important" src="{{ env('FILE_URL').$image->name }}" /></a></td>
                                 
                                <td   class="center">{{ date('d-M-Y', strtotime($image->created_at)) }}</td>
                                <td><a title="Delete" onclick="return confirm('Are you sure you want to delete this image?')" href="{{ route('business_bgallery_delete', $image->id ) }}" ><i class="fa fa-trash-o" aria-hidden="true"></i>  </a> | @if($image->status=='active')<a href="{{ route('business_bgallery_block', [$image->id , '1'] ) }}" title="block" onclick="return confirm('Are you sure you want to block this image?')"><i class="fa fa-ban" aria-hidden="true"></i></a>@else<a href="{{ route('business_bgallery_block', [$image->id , '0'] ) }}" title="un-block" onclick="return confirm('Are you sure you want to un-block this image?')"><i style="color:red" class="fa fa-ban" aria-hidden="true"></i></a>@endif
                                    
                                
                                </td> 
                            </tr>
                           @endforeach
                           @endif  
                        </tbody>
                      </table>
                    </div>                     
                  </div>
                </div>
                  <div class="page_custom">{{ $gallery->links() }}</div>
              </div>
            </div> 
       
<!-- gallery table end-->
</div>


<script src="{{ asset('admin/node_modules/lightgallery/dist/js/lightgallery-all.min.js') }}"></script> 
<script> 
    $(".lightgallery").lightGallery({
      download:false,
      autoplayControls:false,
      thumbnail:false,
      share:false,
      animateThumb: false,
      showThumbByDefault: false
    }); 
</script>
<script src="{{ asset('admin/js/file-upload.js') }}"></script>   
@endsection