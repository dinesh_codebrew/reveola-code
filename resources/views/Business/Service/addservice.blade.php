@extends('layouts.Admin.dashboard')
@if(isset($category))
@section('title', 'Update Service')
@else
@section('title', 'New Service')
@endif

@section('content')  
<link rel="stylesheet" href="{{ asset('admin/node_modules/icheck/skins/all.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/dist/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('admin/node_modules/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
<div class="row flex-grow">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
               
                       <div class="row">
                        <div class="col-6">
                 <h4 class="card-title">@if(isset($service)){{'Update' }}@else {{ 'Add New' }} @endif Service</h4> 
                        </div> 
                        <div class="col-6">
                            @if(isset($service))
                            <p class="page-description"><a style="float: right;" href="{{ route('business_service_addGallery_get',$service->id) }}" class="btn btn-primary">Add Gallery Images</a></p> 
                            @endif
                        </div>
                    </div>
                <p class="card-description"></p>
                @if(isset($service))
                <form class="forms-sample" method="post" action="{{route('business_service_update')}}" enctype="multipart/form-data" id="form"> 
                    <input type="hidden" name="id" value="{{ $service->id }}">
                    @else
                    <form class="forms-sample" method="post" action="{{route('business_service_add_post')}}" enctype="multipart/form-data" id="form">  
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group">
                            <label for="exampleInputName1">Business Name</label>
                            <select name="b_id" id="b_id" class="js-example-basic-single" style="width:100%" required='false'>
                                <option value="">Select One Business</option>
                                @foreach($businesses as $key=>$val)
                                <option @if(isset($service->b_id) && $service->b_id==$val->id){{'selected=selected'}}@endif value="{{$val->id}}">{{$val->b_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName1">Name</label>
                            <input name="s_name" value="{{ isset($service->s_name)?$service->s_name : Request::old('s_name') }}" type="text" class="form-control" id="exampleInputName1" placeholder="Service Name" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName2">Service Discription</label>
                            <textarea class="form-control" rows="4" name="s_des">{{ isset($service->s_des)?$service->s_des : Request::old('s_des') }}</textarea> 
                        </div>  
                        <div class="form-group">
                            <label>Service Image</label>
                            <input type="file" name="s_img" class="file-upload-default" />
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image" />
                                <span class="input-group-btn">
                                    <button class="file-upload-browse btn btn-info" type="button">Browse</button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            <select onchange="getfeatures(this.value, {{ isset($service->id)?$service->id: '0' }});" name="cat_id" id="parent_id" class="js-example-basic-single" style="width:100%">
                                <option value="">Select One category</option>
                                @foreach($maincategories as $key=>$val)
                                <option @if(isset($service->cat_id) && $service->cat_id==$val->id){{'selected=selected'}}@endif value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div> 
                        
                        <div class="card-body col-6" id="features" style="padding-left: 0;"> 
                            <?php $existing_key = []; ?>
                            @if(isset($service->features) && !empty($service->features))
                            <h4 class="card-title">Add features</h4>
                            <?php
                            $featuresArr = json_decode($service->features); 
                            ?>     
                            @foreach($featuresArr as $key=>$val)
                            <?php $existing_key[] = $key; ?>
                            <div class="form-group "><label>{{ $key }}</label><div class="input-group"><input name="features[][{{ $key }}]" value="{{ $val }}" type="text" class="form-control"  aria-describedby="colored-addon3"><span title="Remove" class="input-group-addon bg-primary border-primary removefeature" id="colored-addon3"><i class="fa fa-window-close text-white"></i></span></div></div>
                            @endforeach
                            @endif

                            @if(isset($check_newfeature) && count($check_newfeature)>0) 
                            @foreach($check_newfeature as $val) 
                            @if(in_array($val->feature_name,$existing_key)===false)
                            <div class="form-group"><label>{{ $val->feature_name }}</label><div class="input-group"><input name="features[][{{ $val->feature_name }}]"   type="text" class="form-control"  aria-describedby="colored-addon3"><span title="Remove" class="input-group-addon bg-primary border-primary removefeature" id="colored-addon3"><i class="fa fa-window-close text-white"></i></span></div></div>
                            @endif
                            @endforeach
                            @endif
                        </div>
                        <button type="submit"  class="btn btn-success mr-2">Submit</button>
                        <button  type="button"  onClick="this.form.reset()" class="btn btn-light">Cancel</button>
                    </form> 
            </div>

        </div>
    </div>
</div>
<script>
    function getfeatures(id, service_id) {
    if (id != '') {
    $('#features').html('');
    str = "{{ route('business_cat_features_service',['cat_id' ,'service_id']) }}";
    str = str.replace("cat_id", id);
    URL = str.replace("service_id", service_id);
    
    $.ajax({
    url: URL,
            type: 'GET',
            data: {}, // myData: 'This is my data.'
            success: function (data, status, xhr) {
            if (data.statuscode == 200) {
            var features = data.result.features;
            var data = '';
            $('#features').html('<h4 class="card-title">Add features</h4>');
            $.each(features, function (index, val) {
          //  console.log(index + "==" + val);
            data += '<div class="form-group"><label>' + index + '</label><div class="input-group"><input name="features[][' + index + ']" value="' + val + '" type="text" class="form-control"  aria-describedby="colored-addon3"><span title="Remove" class="input-group-addon bg-primary border-primary removefeature" id="colored-addon3"><i class="fa fa-window-close text-white"></i></span></div></div>';
            });
            $('#features').append(data);
            } else {
            console.log(data);
            }

            },
            error: function (jqXhr, textStatus, errorMessage) {

            //  console.log(errorMessage);
            }

    }).done(function () {
    // $(this).addClass("done");
    });
    }
    }
</script>

<script src="{{ asset('admin/dist/bootstrap-tagsinput.min.js') }}"></script>  
<script src="{{ asset('admin/node_modules/select2/dist/js/select2.min.js') }}"></script>  
<script src="{{ asset('admin/js/select2.js') }}"></script> 
<style>
    div.tagsinput span.tag {
        background: #03a9f3;
        border: 0;
        color: #ffffff;
        padding: 6px 14px;
        font-size: .8125rem;
        font-family: inherit;
        line-height: 1;
    }
    div.tagsinput span.tag a {
        color: #ffffff;
    }
    div.tagsinput{
        width: 100%;
        min-height: 20% !important;
        height: auto !important;
    } 
    .bootstrap-tagsinput { 
        border: 0px !important;
        box-shadow: unset !important;
    }
</style>
<script src="{{ asset('admin/js/file-upload.js') }}"></script> 
<script>
    $(document).ready(function () {

    $('#tag_input').on('itemRemoved', function (event) {
    $('#feature_div').html('');
    var new_val = this.value;
    new_val = new_val.split(',');
    jQuery.each(new_val, function (index, item) {
    $('#feature_div').append('<input type="hidden" name="feature[]" value="' + item + '">')
    });
    });
    $('#tag_input').on('itemAdded', function (event) {
    $('#feature_div').html('');
    var new_val = this.value;
    new_val = new_val.split(',');
    jQuery.each(new_val, function (index, item) {
    $('#feature_div').append('<input type="hidden" name="feature[]" value="' + item + '">')
    });
    });
    });
    $(document).on('click', '.removefeature', function () {
    $(this).closest('.form-group').remove();
    });
</script>   
@endsection