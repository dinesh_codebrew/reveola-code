@extends('layouts.Admin.dashboard')
@section('title', 'All Services')
@section('content')
<!-- Data Tables -->  
<div class="row">
          <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                  <h4 class="card-title">All Services</h4>          
                        </div> 
                        <div class="col-6">
                             <p class="page-description"><a style="float: right;" href="{{ route('business_service_add_get')}}" class="btn btn-primary">Add New Service</a></p> 
                        </div>
                    </div>
                  
                  
                  <div class="row">
                    <div class="table-sorter-wrapper col-lg-12 table-responsive">
                      <table id="sortable-table-2" class="table table-striped">
                        <thead>
                          <tr> 
                               <th>ID</th>
                                <th>Name</th>
                                <th>Category Name</th>  
                                <th>Created At</th>
                                <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(count($services)>0) 
                        @foreach($services as $service) 
                            <tr> 
                                <td class="center">{{ $service->id }}</td>
                                <td>{{ $service->s_name }}</td>
                                <td>@if(isset($service->categories->name)){{ $service->categories->name }}@endif</td> 
                                <td class="center">{{ date('d-M-Y', strtotime($service->created_at)) }}</td>
                                <td><a title="Edit" href="{{ route('business_service_edit', $service->id  ) }}" target="blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a> | <a title="Delete" onclick="return confirm('Are you sure you want to delete this service?')" href="{{ route('business_service_delete', $service->id ) }}" ><i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a> | @if($service->status=='active')<a href="javascript:;" title="Active"><i class="fa fa-ban" aria-hidden="true"></i></a>@else<a href="javascript:;" title="Blocked"><i style="color:red" class="fa fa-ban" aria-hidden="true"></i></a>@endif
                                    
                                
                                </td> 
                            </tr>
                           @endforeach
                           @endif  
                        </tbody>
                      </table>
                    </div>                     
                  </div>
                </div>
                  <div class="page_custom">{{ $services->links() }}</div>
              </div>
            </div> 
       
</div>
<!-- Data Tables -->
<style>
    .page_custom ul{
        float: right !important;
        text-align: right;
        margin-right: 3%;
        margin-top: -15px;
    } 
</style>
  <script src="{{ asset('admin/js/jq.tablesort.js') }}"></script>
  <script src="{{ asset('admin/js/tablesorter.js') }}"></script>
<!-- Page-Level Scripts --> 
@endsection
