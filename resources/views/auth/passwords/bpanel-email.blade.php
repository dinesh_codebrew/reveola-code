<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Business User Reset Password</title>
	 <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('admin/node_modules/mdi/css/materialdesignicons.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('admin/node_modules/simple-line-icons/css/simple-line-icons.css') }}" />
  <link rel="stylesheet" href="{{ asset('admin/node_modules/flag-icon-css/css/flag-icon.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('admin/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css') }}" />
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}" />
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('admin/images/favicon.png') }}" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
 </head>
<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper">
		<div class="row">
			  <div class="content-wrapper full-page-wrapper d-flex align-items-center auth register-full-bg">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <h2>Reset Password</h2>
                <h4 class="font-weight-light">Enter your email address and your password will be reset and emailed to you.</h4>
                 <div class="row">
                        <div class="col-lg-12">
							@foreach($errors->all() as $error)
									<div class="alert alert-dismissable alert-danger">
										{{ $error }}
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									</div>
							@endforeach 
							@if (session('status'))
								<div class="alert alert-success">
								{{ session('status') }}
								</div>
							@endif
						</div>
					</div>                 
                <form class="pt-4" role="form" method="POST" action="{{ route('bpanel.password.email') }}"> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                     {{ csrf_field() }}
                    <input  type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" value="{{ old('email') }}" required />
                    <i class="mdi mdi-account"></i>
                  </div>
                  
                  <div class="mt-5">
                    <button class="btn btn-block btn-primary btn-lg font-weight-medium" name="register" type="submit">Send Password Reset Link</button>
                  </div>
                  
                  <div class="mt-2 text-center">
                    <a href="{{ route('business_login_get') }}" class="auth-link text-black">Already have an account? <span class="font-weight-medium">Sign in</span></a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
		</div>
      <!-- row ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{ asset('admin/node_modules/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('admin/node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
  <script src="{{ asset('admin/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('admin/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{ asset('admin/js/off-canvas.js') }}"></script>
  <script src="{{ asset('admin/js/hoverable-collapse.js') }}"></script>
  <script src="{{ asset('admin/js/misc.js') }}"></script>
  <script src="{{ asset('admin/js/settings.js') }}"></script>
  <script src="{{ asset('admin/js/todolist.js') }}"></script>
  <!-- endinject -->
</body> 
</html>

 
