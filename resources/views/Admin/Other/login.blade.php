@extends('layouts.Admin.login')
@section('title', 'login')
@section('content') 
  <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-full-bg">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
				@foreach($errors->all() as $error)
                <div class="alert alert-dismissable alert-danger">
                    {{ $error }}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                </div>
                @endforeach

                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
              <div class="auth-form-dark text-left p-5">
                <h2>Login</h2>
                <h4 class="font-weight-light">Welcome to Reveola Admin Panel</h4>
                
                  <form class="pt-5" method="post" action="{{route('admin_login_post')}}">
					{{ csrf_field() }}
					<input type="hidden" name="timezone" value="" id="timezone">
					<div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input  type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Email" value="@if(count($details)>0){{ $details['ad_email'] }} @else {{ Request::old('email') }} @endif"/>
                    <i class="mdi mdi-account"></i>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control pass" id="exampleInputPassword1" placeholder="Password" value="@if(count($details)>0){{ $details['ad_psw'] }}@endif"/>
                    <i class="mdi mdi-eye" onclick="showPass();"></i>
                  </div>
                  <div class="mt-2 w-100 mx-auto">
                     <div class="form-check form-check-flat" style="float:right; padding-bottom:1%;">
							<label class="form-check-label">
								<input type="checkbox" name="remember" {{ count($details)>0 || old('remember') ? 'checked' : '' }}> {{ __('signup.Remember Me') }}
							</label>
						</div> 
                  </div>
                  <div class="mt-5" style="margin-top: 1rem !important;">
                    <button type="submit" class="btn btn-block btn-warning btn-lg font-weight-medium" name="Login">Login</button>
                  </div>
                  <div class="mt-3 text-center">
                    <a href="{{ route('admin.password.request') }}" class="auth-link text-white">Forgot password?</a>
                  </div>                 
                </form>
              </div>
            </div>
          </div>
          
	    <script src="{{ asset('admin/node_modules/jquery/dist/jquery.min.js') }}"></script>
      <script> 
		  function showPass(){
			  if($('.pass').attr('type')=='password')
				$('.pass').attr('type', 'text');
				else
				$('.pass').attr('type', 'password');
		  } 
      </script>
@endsection
