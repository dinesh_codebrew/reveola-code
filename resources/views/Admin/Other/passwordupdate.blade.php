@extends('layouts.Admin.dashboard')
@section('title', 'Password Update')
@section('content')
<link rel="stylesheet" href="{{ asset('admin/node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/dist/bootstrap-tagsinput.css') }}">
<div class="row flex-grow">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Password Update</h4> 
                <form class="forms-sample" method="post" action="{{route('apassword_update_post')}}"  id="form">  
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label for="exampleInputName1">Old Password</label>
                        <input type="password" placeholder="Old Password" autocomplete="off" class="form-control" required onblur="this.value = removeSpaces(this.value)" name="old_password" autofocus="on" id="old_password">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputName1">New Password</label>
                        <input type="password" placeholder="Password" autocomplete="off" class="form-control" required onblur="this.value = removeSpaces(this.value)" name="password" id="password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputName1">Password Confirmation</label>
                        <input type="password" placeholder="Paswword Confirmation" autocomplete="off" class="form-control" required onblur="this.value = removeSpaces(this.value)" name="password_confirmation" id="password_confirmation">
                    </div>
                    {!! Form::submit('Update Password', ['class' => 'btn btn-success mr-2']) !!}
                    {!! Form::reset('Reset', ['class' => 'btn btn-primary']) !!}


                </form>
            </div>

        </div>
    </div>
</div>


<script src="/admin/js/plugins/validate/jquery.validate.min.js"></script>
<script type="text/javascript">

                            $("#dashboard").addClass("active");

                            $("#form").validate({
                                rules: {
                                    old_password: {
                                        required: true
                                    },
                                    password: {
                                        required: true,
                                    },
                                    password_confirmation: {
                                        equalTo: "#password"
                                    }
                                },
                                messages: {
                                    old_password: {
                                        required: "Old password is required",
                                    },
                                    password: {
                                        required: "New password is required",
                                    },
                                    password_confirmation: {
                                        required: "New Password Confirmation is required",
                                        equalTo: "New Password Confirmation do not match",
                                    }
                                }
                            });

</script>


@stop



