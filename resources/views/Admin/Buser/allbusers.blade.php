@extends('layouts.Admin.dashboard')
@section('title', 'All Categories')
@section('content')
<!-- Data Tables -->  
<div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <h4 class="card-title">All Business Users</h4>          
                    </div> 
                    <div class="col-6">
  <p class="page-description"><a style="float: right;" href="{{ route('admin_b_user_add_get')}}" class="btn btn-primary">Add New Business User</a></p> 
                    </div>
                </div>


                <div class="row">
                    <div class="table-sorter-wrapper col-lg-12 table-responsive">
                        <table id="sortable-table-2" class="table table-striped">
                            <thead>
                                <tr> 
                                    <th>ID</th>
                                    <th>Name</th> 
                                    <th>Email</th> 
                                    <th>Phone</th> 
                                    <th>Status</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($users)>0) 
                                @foreach($users as $user) 
                                <tr> 
                                    <td class="center">{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td> 
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>

                                    <td class="center">{{ $user->status }}</td>
                                    <td> <a title="Edit" href="{{ route('admin_b_user_edit',$user->id) }}" target="blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> | <a title="Delete" onclick="return confirm('Are you sure you want to delete this User?')" href="{{ route('admin_user_delete', $user->id ) }}" ><i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a> | @if($user->status=='active')<a href="{{ route('admin_user_block', [$user->id , '1'] ) }}" title="block" onclick="return confirm('Are you sure you want to block this user?')"><i class="fa fa-ban" aria-hidden="true"></i></a>@else<a href="{{ route('admin_user_block', [$user->id , '0'] ) }}" title="un-block" onclick="return confirm('Are you sure you want to un-block this user?')"><i style="color:red" class="fa fa-ban" aria-hidden="true"></i></a>@endif | <a title="View"  href="{{ route('admin_b_user_view',$user->id  ) }}" target="blank"><i class="fa fa-eye"></i></a>


                                    </td> 
                                </tr>
                                @endforeach
                                @endif  
                            </tbody>
                        </table>
                    </div>                     
                </div>
            </div>
            <div class="page_custom">{{ $users->links() }}</div>
        </div>
    </div> 

</div>
<!-- Data Tables -->
<style>
    .page_custom ul{
        float: right !important;
        text-align: right;
        margin-right: 3%;
        margin-top: -15px;
    } 
</style>
<script src="{{ asset('admin/js/jq.tablesort.js') }}"></script>
<script src="{{ asset('admin/js/tablesorter.js') }}"></script>
<!-- Page-Level Scripts --> 
@endsection
