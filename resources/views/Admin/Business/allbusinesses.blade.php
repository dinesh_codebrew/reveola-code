@extends('layouts.Admin.dashboard')
@section('title', 'All Businesses')
@section('content')
<!-- Data Tables -->  
<div class="row">
          <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                  <h4 class="card-title">All Businesses</h4>          
                        </div> 
                        <div class="col-6">
                             <p class="page-description"><a style="float: right;" href="{{ route('admin_business_add_get')}}" class="btn btn-primary">Add New Business</a></p> 
                        </div>
                    </div>
                    <div class="ajxloader" style="display: none">
                        <img src="{{ asset('/admin/images/ajax-loader.gif') }}"/>
                    </div> 
                  
                  <div class="row">
                    <div class="table-sorter-wrapper col-lg-12 table-responsive">
                      <table id="sortable-table-2" class="table table-striped">
                        <thead>
                          <tr> 
                               <th>ID</th>
                                <th>Name</th>  
                                <th>Category Name</th>  
                                <th>User</th>  
                                <th>Web URL</th>  
                                <!--<th>Created At</th>-->
                                <th>Share Credentials</th>
                                <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(count($businesses)>0) 
                        @foreach($businesses as $business) 
                            <tr> 
                                <td class="center">{{ $business->id }}</td>
                                <td>{{ $business->b_name }}</td>
                                <td><a href="{{ route('admin_category_edit', $business->cat_id  ) }}" target="blank">@if(isset($business->category->id)){{ $business->category->name}}@endif</a></td>
                                
                                <td><a href="{{ route('admin_b_user_edit', $business->user_id  ) }}" target="blank">@if(isset($business->user_id) && $business->user_id!=0){{ $business->user->name}}@endif</a></td>
                                <td class="center"><a href="@if(strstr($business->weburl, 'http')==false){{ 'http://'.$business->weburl }}@else {{ $business->weburl }}@endif" target="blank">{{ $business->weburl }}</a></td>
                                <!--<td class="center">{ { date('d-M-Y', strtotime($business->created_at)) }}</td>-->
                                <td>@if(isset($business->user_id) && $business->user_id!=0)
                                    <a href='javascript:;' onclick="share_credentials({{ $business->id }});">Share</a> 
                                    @endif
                                </td>
                                <td><a title="Edit" href="{{ route('admin_business_edit', $business->id  ) }}" target="blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a> | <a title="Delete" onclick="return confirm('Are you sure you want to delete this business?')" href="{{ route('admin_business_delete', $business->id ) }}" ><i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a> | @if($business->status=='active')<a href="{{ route('admin_business_block', [$business->id , '1'] ) }}" title="block" onclick="return confirm('Are you sure you want to block this business?')"><i class="fa fa-ban" aria-hidden="true"></i></a>@else<a href="{{ route('admin_business_block', [$business->id , '0'] ) }}" title="un-block" onclick="return confirm('Are you sure you want to un-block this business?')"><i style="color:red" class="fa fa-ban" aria-hidden="true"></i></a>@endif 
                                </td> 
                            </tr>
                           @endforeach
                           @endif  
                        </tbody>
                      </table>
                    </div>                     
                  </div>
                </div>
                  <div class="page_custom">{{ $businesses->links() }}</div>
              </div>
            </div> 
  
</div>
<!-- Data Tables -->
<style>
    .page_custom ul{
        float: right !important;
        text-align: right;
        margin-right: 3%;
        margin-top: -15px;
    } 
    .ajxloader {
    position: absolute;
    z-index: 999;
    width: 100%;
    text-align: center;
    top: 275px;
}
</style>
  <script src="{{ asset('admin/js/jq.tablesort.js') }}"></script>
  <script src="{{ asset('admin/js/tablesorter.js') }}"></script>
  <script> 
   function share_credentials(business_id) {
    if (business_id != '') { 
    str = "{{ route('admin_share_credentials', 'business_id') }}"; 
    URL = str.replace("business_id", business_id); 
    $('.ajxloader').css('display','block');
    $.ajax({
    url: URL,
            type: 'GET',
            data: {}, // myData: 'This is my data.'
            success: function (data, status, xhr) {
                $('.ajxloader').css('display','none');
            if (data.statuscode == 200) {  
                $('.content-wrapper').prepend('<div class="alert alert-success">'+data.msg+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            } else { 
                $('.content-wrapper').prepend('<div class="alert alert-dismissable alert-danger">'+data.msg+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            }
            },
            error: function (jqXhr, textStatus, errorMessage) {
             $('.ajxloader').css('display','none');  
              $('.content-wrapper').prepend('<div class="alert alert-dismissable alert-danger">'+errorMessage+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            }

    }).done(function () {
    // $(this).addClass("done");
    });
    }
    }
  </script>
<!-- Page-Level Scripts --> 
@endsection