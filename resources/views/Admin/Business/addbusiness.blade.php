@extends('layouts.Admin.dashboard')
@if(isset($category))
@section('title', 'Update Business')
@else
@section('title', 'New Business')
@endif

@section('content')  
<link rel="stylesheet" href="{{ asset('admin/node_modules/icheck/skins/all.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/dist/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('admin/node_modules/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
<div class="row flex-grow">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
               
                       <div class="row">
                        <div class="col-6">
                 <h4 class="card-title">@if(isset($business)){{'Update' }}@else {{ 'Add New' }} @endif Business</h4> 
                        </div> 
                        <div class="col-6">
                            @if(isset($business))
                            <p class="page-description"><a style="float: right;" href="{{ route('admin_business_addGallery_get',$business->id) }}" class="btn btn-primary">Add Gallery Images</a></p> 
                            @endif
                        </div>
                    </div>
                <p class="card-description"></p>
                @if(isset($business))
                <form class="forms-sample" method="post" action="{{route('admin_business_update')}}" enctype="multipart/form-data" id="form"> 
                    <input type="hidden" name="id" value="{{ $business->id }}">
                    @else
                    <form class="forms-sample" method="post" action="{{route('admin_business_add_post')}}" enctype="multipart/form-data" id="form">  
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="b_address" id="b_address" value="{{ isset($business->b_address)?$business->b_address : Request::old('b_address') }}" />
                            <input type="hidden" name="b_lat" id="b_lat" value="{{ isset($business->b_lat)?$business->b_lat : Request::old('b_lat') }}" />
                            <input type="hidden" name="b_lng" id="b_lng" value="{{ isset($business->b_lng)?$business->b_lng : Request::old('b_lng') }}" />
                        <div class="form-group">
                            <label for="exampleInputName1">Business Name</label>
                            <input name="b_name" value="{{ isset($business->b_name)?$business->b_name : Request::old('b_name') }}" type="text" class="form-control" id="exampleInputName1" placeholder="Business Name" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName2">Business Description</label>
                            <textarea class="form-control" rows="4" name="b_des">{{ isset($business->b_des)?$business->b_des : Request::old('b_des') }}</textarea> 
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName1">Flat No.</label>
                            <input name="b_flat_no" value="{{ isset($business->b_flat_no)?$business->b_flat_no : Request::old('b_flat_no') }}" type="text" class="form-control" id="exampleInputName1" placeholder="Flat Number" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName1">Address</label>
                            <input id="autocomplete" name="b_address1" value="{{ isset($business->b_address)?$business->b_address : Request::old('b_address') }}" type="text" class="form-control"   placeholder="Address" required="required"/> 
                        </div>
                        
                         <div class="form-group">
                            <label for="exampleInputName1">Mobile Number</label>
                            <input name="phone" value="{{ isset($business->phone)?$business->phone: Request::old('phone') }}" type="text" class="form-control" id="exampleInputName1" placeholder="Mobile Number" />
                        </div>    
                         <div class="form-group">
                            <label for="exampleInputName1">Email</label>
                            <input name="email" value="{{ isset($business->email)?$business->email: Request::old('email') }}" type="text" class="form-control" id="exampleInputName1" placeholder="Email" />
                        </div>    
                         <div class="form-group">
                            <label for="exampleInputName1">WebSite</label>
                            <input name="weburl" value="{{ isset($business->weburl)?$business->weburl: Request::old('weburl') }}" type="text" class="form-control" id="exampleInputName1" placeholder="Website" />
                        </div>    
                         <div class="form-group">
                            <label for="exampleInputName1">Charges Description</label>
                            <input name="price" value="{{ isset($business->price)?$business->price: Request::old('price') }}" type="text" class="form-control" id="exampleInputName1" placeholder="Charges Description" />
                        </div>    
                         <div class="form-group">
                            <label for="exampleInputName1">Offer (If Any)</label>
                            <input name="offer" value="{{ isset($business->offer)?$business->offer: Request::old('offer') }}" type="text" class="form-control" id="exampleInputName1" placeholder="offer" />
                        </div>    
                            
                        <div class="form-group">
                            <label>Business Image</label>
                            <input type="file" name="b_pic" class="file-upload-default" />
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image" />
                                <span class="input-group-btn">
                                    <button class="file-upload-browse btn btn-info" type="button">Browse</button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            <!-- onchange="getfeatures(this.value, { { isset($business->id)?$business->id: '0' }});"-->
                            <select  name="cat_id" class="js-example-basic-single" style="width:100%" required="required">
                                <option value="">Select One category</option>
                                @foreach($maincategories as $key=>$val)
                                <option @if(isset($business->cat_id) && $business->cat_id==$val->id){{'selected=selected'}}@endif value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div> 
                        <div class="form-group">
                            <label>Add Business User</label> 
                            <select  name="user_id"  class="js-example-basic-single" style="width:100%" required="required">
                                <option value="">Add one Buiness User</option>
                                @foreach($users as $key=>$val)
                                <option @if(isset($business->user_id) && $business->user_id==$val->id){{'selected=selected'}}@endif value="{{$val->id}}">{{ $val->name.' ('.$val->email.')'}}</option>
                                @endforeach
                            </select>
                        </div> 
                        <div class="card-body col-6" id="features" style="padding-left: 0;"> 
                            <?php $existing_key = []; ?>
                            @if(isset($business->features) && !empty($business->features))
                            <h4 class="card-title">Add features</h4>
                            <?php
                            $featuresArr = json_decode($business->features); 
                            ?>     
                            @foreach($featuresArr as $key=>$val)
                            <?php $existing_key[] = $key; ?>
                            <div class="form-group "><label>{{ $key }}</label><div class="input-group"><input name="features[][{{ $key }}]" value="{{ $val }}" type="text" class="form-control"  aria-describedby="colored-addon3"><span title="Remove" class="input-group-addon bg-primary border-primary removefeature" id="colored-addon3"><i class="fa fa-window-close text-white"></i></span></div></div>
                            @endforeach
                            @endif

                            @if(isset($check_newfeature) && count($check_newfeature)>0) 
                            @foreach($check_newfeature as $val) 
                            @if(in_array($val->feature_name,$existing_key)===false)
                            <div class="form-group"><label>{{ $val->feature_name }}</label><div class="input-group"><input name="features[][{{ $val->feature_name }}]"   type="text" class="form-control"  aria-describedby="colored-addon3"><span title="Remove" class="input-group-addon bg-primary border-primary removefeature" id="colored-addon3"><i class="fa fa-window-close text-white"></i></span></div></div>
                            @endif
                            @endforeach
                            @endif
                        </div>
                        <button type="submit"  class="btn btn-success mr-2">Submit</button>
                        <button  type="button"  onClick="this.form.reset()" class="btn btn-light">Cancel</button>
                    </form> 
            </div>

        </div>
    </div>
</div>
 <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress); 
            var input = document.getElementById('autocomplete');
        google.maps.event.addDomListener(input, 'keydown', function(event) { 
            if (event.keyCode === 13) {  
                event.preventDefault(); 
            }
          });
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
       
         if (!place.geometry) { 
		$('#b_lat').val('');
		$('#b_lng').val(''); 
                
          }

       /* for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }*/

		var lat = place.geometry.location.lat(); 
		var lng = place.geometry.location.lng(); 
		$('#b_lat').val(lat);
		$('#b_lng').val(lng);  
                $('#b_address').val($('#autocomplete').val());
                $('#autocomplete').attr('disabled','disabled');
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
    /*    for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }*/
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      /*function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }*/
 
    </script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('google_place_api_key')}}&libraries=places&callback=initAutocomplete"
   
        async defer></script>
<script>
    function getfeatures(id, service_id) {
    if (id != '') {
    $('#features').html('');
    str = "{{ route('admin_cat_features_service',['cat_id' ,'service_id']) }}";
    str = str.replace("cat_id", id);
    URL = str.replace("service_id", service_id);
    
    $.ajax({
    url: URL,
            type: 'GET',
            data: {}, // myData: 'This is my data.'
            success: function (data, status, xhr) {
            if (data.statuscode == 200) {
            var features = data.result.features;
            var data = '';
            $('#features').html('<h4 class="card-title">Add features</h4>');
            $.each(features, function (index, val) {
          //  console.log(index + "==" + val);
            data += '<div class="form-group"><label>' + index + '</label><div class="input-group"><input name="features[][' + index + ']" value="' + val + '" type="text" class="form-control"  aria-describedby="colored-addon3"><span title="Remove" class="input-group-addon bg-primary border-primary removefeature" id="colored-addon3"><i class="fa fa-window-close text-white"></i></span></div></div>';
            });
            $('#features').append(data);
            } else {
            console.log(data);
            }

            },
            error: function (jqXhr, textStatus, errorMessage) {

            //  console.log(errorMessage);
            }

    }).done(function () {
    // $(this).addClass("done");
    });
    }
    }
</script>

<script src="{{ asset('admin/dist/bootstrap-tagsinput.min.js') }}"></script>  
<script src="{{ asset('admin/node_modules/select2/dist/js/select2.min.js') }}"></script>  
<script src="{{ asset('admin/js/select2.js') }}"></script> 
<style>
    div.tagsinput span.tag {
        background: #03a9f3;
        border: 0;
        color: #ffffff;
        padding: 6px 14px;
        font-size: .8125rem;
        font-family: inherit;
        line-height: 1;
    }
    div.tagsinput span.tag a {
        color: #ffffff;
    }
    div.tagsinput{
        width: 100%;
        min-height: 20% !important;
        height: auto !important;
    } 
    .bootstrap-tagsinput { 
        border: 0px !important;
        box-shadow: unset !important;
    }
</style>
<script src="{{ asset('admin/js/file-upload.js') }}"></script> 
<script>
    $(document).ready(function () {

    $('#tag_input').on('itemRemoved', function (event) {
    $('#feature_div').html('');
    var new_val = this.value;
    new_val = new_val.split(',');
    jQuery.each(new_val, function (index, item) {
    $('#feature_div').append('<input type="hidden" name="feature[]" value="' + item + '">')
    });
    });
    $('#tag_input').on('itemAdded', function (event) {
    $('#feature_div').html('');
    var new_val = this.value;
    new_val = new_val.split(',');
    jQuery.each(new_val, function (index, item) {
    $('#feature_div').append('<input type="hidden" name="feature[]" value="' + item + '">')
    });
    });
    });
    $(document).on('click', '.removefeature', function () {
    $(this).closest('.form-group').remove();
    });
</script>   
@endsection