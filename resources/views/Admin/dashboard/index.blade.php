@extends('layouts.Admin.dashboard')
@section('title', 'Dashboard')
@section('content')
<div class="row">
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body"> 
                <a href="{{ route('admin_user_all') }}">
                    <div class="d-flex align-items-center justify-content-md-center">
                        <i class="icon-user-following icon-lg text-success"></i>
                        <div class="ml-3">

                            <p class="mb-0">Total Users</p>
                            <h6>{{ $stats_data[0]->users_count }}</h6>                      
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <a href="{{ route('admin_category_all') }}">
                    <div class="d-flex align-items-center justify-content-md-center">
                        <i class="mdi mdi-rocket icon-lg text-warning"></i>
                        <div class="ml-3">
                            <p class="mb-0">Main Categories</p>
                            <h6>{{ $stats_data[0]->cat_count }}</h6>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <a href="{{ route('admin_subcategory_all') }}">
                    <div class="d-flex align-items-center justify-content-md-center">
                        <i class="mdi mdi-diamond icon-lg text-info"></i>
                        <div class="ml-3">
                            <p class="mb-0">Sub-Categories</p>
                            <h6>{{ $stats_data[0]->subcat_count }}</h6>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <a href="{{ route('admin_sales_all') }}">
                    <div class="d-flex align-items-center justify-content-md-center">
                        <i class="mdi mdi-chart-line-stacked icon-lg text-danger"></i>
                        <div class="ml-3">
                            <p class="mb-0">Total Subscriptions </p>
                            <h6>{{ $stats_data[0]->orders_count }}</h6>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body pb-0">
                <h6 class="card-title">Reviews</h6>
                <div class="row">
                    @if(isset($reviews) && count($reviews)>0)
                    @foreach($reviews as $review)
                    <div class="col-12">
                        <div class="wrapper border-bottom py-2">
                            <div class="d-flex">

                                @if(isset($review->user->profile_pic) and !empty($review->user->profile_pic)) 
                                <img src="{{ $review->user->profile_pic_url.'/300/300' }}" alt="image" class="img-sm rounded-circle" />@else
                                <img src="{{ asset('admin/images/faces/face10.jpg') }}" alt="image" class="img-sm rounded-circle" />

                                @endif 
                                <div class="wrapper ml-4">
                                    <p class="mb-0">{{ isset($review->user->name)?$review->user->name  :'' }}</p>
                                    <small class="text-muted mb-0">{{ $review->r_des }}</small>
                                </div>
                                <div class="rating ml-auto d-flex align-items-center">
                                    @if($review->rating==3)
                                    <img src="{{ asset('/images/smile.png') }}" alt="image" class="img-sm rounded-circle" />
                                    @elseif($review->rating==2)
                                    <img src="{{ asset('/images/neutral.png') }}" alt="image" class="img-sm rounded-circle" />
                                    @elseif($review->rating==1)
                                    <img src="{{ asset('/images/sad.png') }}" alt="image" class="img-sm rounded-circle" />
                                    @else
                                    {{ 'No Rating Given' }}
                                    @endif    
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 stretch-card">
        <div class="row flex-grow">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title mb-0">Pending Requests</h6>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-inline-block pt-3">
                                <div class="d-lg-flex">
                                    <h2 class="mb-0">25</h2>
                                    <div class="d-flex align-items-center ml-lg-2"> 
                                        <small class="ml-1 mb-0"></small>
                                    </div>
                                </div>
                                <small class="text-gray">Reuests for products/services from register/anonymous users .</small>
                            </div>
                            <div class="d-inline-block">
                                <div class="bg-success px-3 px-md-4 py-2 rounded">
                                    <i class="mdi mdi-buffer text-white icon-lg"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title mb-0">Business Claims</h6>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-inline-block pt-3">
                                <div class="d-lg-flex">
                                    <h2 class="mb-0">10</h2>
                                    <div class="d-flex align-items-center ml-lg-2"> 
                                        <small class="ml-1 mb-0"></small>
                                    </div>
                                </div>
                                <small class="text-gray">Business claim requests from business owners.</small>
                            </div>
                            <div class="d-inline-block">
                                <div class="bg-warning px-3 px-md-4 py-2 rounded">
                                    <i class="mdi mdi-wallet text-white icon-lg"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 grid-margin stretch-card"> 
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="card-title">All Users</h4>        
                    </div> 
                    <div class="col-md-6">
                        <p class="page-description">
                            <select id="starting_date" class="form-control border-primary" style="float: right; width:50%"  onchange="getNewData(this.value);">
                                <option>Select Year</option>
                                @for($i=2015;$i<=2030;$i++)
                                <option @if($i==date('Y')){{'selected="selected"'}}@endif value="{{ $i }}">{{ $i }}</option> 
                                @endfor
                            </select>
                        </p> 
                    </div>
                </div>

                <canvas id="lineChart1" style="max-height: 260px"></canvas>
            </div>
        </div>
    </div>
</div> 
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Total Subscriptions</h4>
                <canvas id="barChart" style="max-height:260px"></canvas>
            </div>
        </div>
    </div>
</div> 
<script src="{{ asset('admin/node_modules/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>
<script src="{{ asset('admin/node_modules/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ asset('admin/node_modules/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('admin/node_modules/morris.js/morris.min.js') }}"></script>
<script src="{{ asset('admin/node_modules/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
<script>
                                        $(document).ready(function () {
                                    new_get_dashboard_data();
                                });
                                var options = {
                                    scales: {
                                        yAxes: [{
                                                ticks: {
                                                    beginAtZero: true
                                                }
                                            }]
                                    },
                                    legend: {
                                        display: false
                                    },
                                    elements: {
                                        point: {
                                            radius: 0
                                        }
                                    },
                                    tooltips: {
                                        mode: 'index',
                                        intersect: false,
                                    },
                                    hover: {
                                        mode: 'nearest',
                                        intersect: true
                                    }

                                };
                                var bar_data = {
                                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov',
                                        'Dec'],
                                    datasets: [{
                                            label: '# of Subscriptions',
                                            data: ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
                                            backgroundColor: [
                                                'rgba(54, 162, 235, 0.2)',
                                                'rgba(255, 206, 86, 0.2)',
                                                'rgba(75, 192, 192, 0.2)',
                                                'rgba(153, 102, 255, 0.2)',
                                                'rgba(255, 159, 64, 0.2)',
                                                'rgba(54, 162, 235, 0.2)',
                                                'rgba(255, 206, 86, 0.2)',
                                                'rgba(75, 192, 192, 0.2)',
                                                'rgba(153, 102, 255, 0.2)',
                                                'rgba(255, 159, 64, 0.2)',
                                                'rgba(54, 162, 235, 0.2)',
                                                'rgba(255, 206, 86, 0.2)'
                                            ],
                                            borderColor: [
                                                'rgba(54, 162, 235, 1)',
                                                'rgba(255, 206, 86, 1)',
                                                'rgba(75, 192, 192, 1)',
                                                'rgba(153, 102, 255, 1)',
                                                'rgba(255, 159, 64, 1)',
                                                'rgba(54, 162, 235, 1)',
                                                'rgba(255, 206, 86, 1)',
                                                'rgba(75, 192, 192, 1)',
                                                'rgba(153, 102, 255, 1)',
                                                'rgba(255, 159, 64, 1)',
                                                'rgba(54, 162, 235, 1)',
                                                'rgba(255, 206, 86, 1)'
                                            ],
                                            borderWidth: 1
                                        }]
                                };
                                var multiLineData = {
                                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov',
                                        'Dec'],
                                    datasets: [{
                                            label: 'Users',
                                            data: ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
                                            borderColor: [
                                                '#587ce4'
                                            ],
                                            borderWidth: 2,
                                            fill: true
                                        },
                                        {
                                            label: 'Business Users',
                                            data: ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
                                            borderColor: [
                                                '#ede190'
                                            ],
                                            borderWidth: 2,
                                            fill: true
                                        }
                                    ]
                                };
                                var lineChartCanvas = $("#lineChart1").get(0).getContext("2d");
                                var barChartCanvas = $("#barChart").get(0).getContext("2d");
                                var lineChart = new Chart(lineChartCanvas, {
                                    type: 'line',
                                    data: multiLineData,
                                    options: options
                                });
                                var barChart = new Chart(barChartCanvas, {
                                    type: 'bar',
                                    data: bar_data,
                                    options: options
                                });
                                function getNewData() {
                                    new_get_dashboard_data();
                                }
                                function new_get_dashboard_data()
                                {    /* dt = document.getElementById('daterange').value;
                                 yr = document.getElementById('starting_date').value;
                                 dt = encodeURIComponent(dt);*/
                                    yr = document.getElementById('starting_date').value;
                                    route = '{!! route("get_dashboard_data") !!}?yr_only=' + yr;
                                    ;
                                    $.ajax({
                                        url: route,
                                        type: 'GET',
                                        async: true,
                                        dataType: "json",
                                        success: function (data)
                                        {
                                            if (data.success == '0') {
                                                console.log(data.msg)
                                            } else
                                            {   //console.log(data.graph_data)
                                                graph_data(data.graph_data);
                                            }
                                        }
                                    });

                                }
                                function graph_data(data) {
                                    multiLineData.datasets.forEach(function (dataset,i) {
                                        if (i == 0)
                                            dataset.data = data.users;
                                        else
                                            dataset.data = data.busers; 
                                    });
                                    lineChart.update();
                                    bar_data.datasets.forEach(function (dataset,i) {
                                            dataset.data = data.orders; 
                                    });
                                    barChart.update();
                                }
</script>
<!--<script src="{ { asset('admin/js/chart.js') }}"></script>-->
@endsection
