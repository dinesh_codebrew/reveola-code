@extends('layouts.Admin.dashboard')
@section('title', 'All Categories')
@section('content')
<!-- Data Tables -->  
<div class="row">
          <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                  <h4 class="card-title">All Subscriptions</h4>          
                        </div> 
                        <div class="col-6">
                              
                        </div>
                    </div>
                  
                  
                  <div class="row">
                    <div class="table-sorter-wrapper col-lg-12 table-responsive">
                      <table id="sortable-table-2" class="table table-striped">
                        <thead>
                          <tr> 
                            <th>ID</th>
                            <th>Plan Name</th> 
                            <th>Price ($)</th> 
                            <th>User</th>  
                            <th>Transaction ID</th> 
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(count($orders)>0) 
                        @foreach($orders as $order) 
                            <tr> 
                                <td class="center">{{ $order->id }}</td>
                                <td>{{ $order->sub_name }}</td> 
                               <td>{{ $order->sub_price }}</td>
                               <td>{{ $order->user_id }}</td> 
                               <td>{{ $order->balance_transaction }}</td> 
                                <td>
                    <a title="Delete" onclick="return confirm('Are you sure you want to delete this Order?')" href="{{ route('admin_sale_delete', $order->id ) }}" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>  | <a title="View"  href="{{ route('admin_sale_view',$order->id  ) }}" target="blank"><i class="fa fa-eye"></i></a>
                                </td> 
                            </tr>
                           @endforeach
                           @endif  
                        </tbody>
                      </table>
                    </div>                     
                  </div>
                </div>
                  <div class="page_custom">{{ $orders->links() }}</div>
              </div>
            </div> 
       
</div>
<!-- Data Tables -->
<style>
    .page_custom ul{
        float: right !important;
        text-align: right;
        margin-right: 3%;
        margin-top: -15px;
    } 
</style>
  <script src="{{ asset('admin/js/jq.tablesort.js') }}"></script>
  <script src="{{ asset('admin/js/tablesorter.js') }}"></script>
<!-- Page-Level Scripts --> 
@endsection