@extends('layouts.Admin.dashboard')
@section('title', 'All Categories')
@section('content')
<!-- Data Tables -->  
<div class="row">
          <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                  <h4 class="card-title">All Subscription Plans</h4>          
                        </div> 
                        <div class="col-6">
                              
                        </div>
                    </div>
                  
                  
                  <div class="row">
                    <div class="table-sorter-wrapper col-lg-12 table-responsive">
                      <table id="sortable-table-2" class="table table-striped">
                        <thead>
                          <tr> 
                               <th>ID</th>
                                <th>Name</th> 
                                <th>Tag line</th> 
                                <th>Price ($)</th> 
                                <th>Time Period (days)</th> 
                                <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(count($subscriptions)>0) 
                        @foreach($subscriptions as $subscription) 
                            <tr> 
                                <td class="center">{{ $subscription->id }}</td>
                                <td>{{ $subscription->sub_name }}</td> 
                               <td>{{ $subscription->sub_tag_line }}</td>
                               <td>{{ $subscription->sub_price }}</td>
                                
                    <td class="center">{{ $subscription->validity }}</td>
                    <td>
                             <!--   <a title="Edit" href="{ { route('admin_subscription_edit', $subscription->id  ) }}" target="blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a> | --><a title="Delete" onclick="return confirm('Are you sure you want to delete this Subscription?')" href="{{ route('admin_subscription_delete', $subscription->id ) }}" ><i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a> | @if($subscription->status=='active')<a href="{{ route('admin_subscription_block', [$subscription->id , '1'] ) }}" title="block" onclick="return confirm('Are you sure you want to block this Subscription?')"><i class="fa fa-ban" aria-hidden="true"></i></a>@else<a href="{{ route('admin_subscription_block', [$subscription->id , '0'] ) }}" title="un-block" onclick="return confirm('Are you sure you want to un-block this category?')"><i style="color:red" class="fa fa-ban" aria-hidden="true"></i></a>@endif
                                    
                                
                                </td> 
                            </tr>
                           @endforeach
                           @endif  
                        </tbody>
                      </table>
                    </div>                     
                  </div>
                </div>
                  <div class="page_custom">{{ $subscriptions->links() }}</div>
              </div>
            </div> 
       
</div>
<!-- Data Tables -->
<style>
    .page_custom ul{
        float: right !important;
        text-align: right;
        margin-right: 3%;
        margin-top: -15px;
    } 
</style>
  <script src="{{ asset('admin/js/jq.tablesort.js') }}"></script>
  <script src="{{ asset('admin/js/tablesorter.js') }}"></script>
<!-- Page-Level Scripts --> 
@endsection