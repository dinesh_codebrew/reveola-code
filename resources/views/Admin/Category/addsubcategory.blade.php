@extends('layouts.Admin.dashboard')
@if(isset($category))
@section('title', 'Update Sub-Category')
@else
@section('title', 'New Sub-Category')
@endif

@section('content') 
<link rel="stylesheet" href="{{ asset('admin/node_modules/icheck/skins/all.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/dist/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{ asset('admin/node_modules/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
  
<div class="row flex-grow">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">@if(isset($category)){{'Update' }}@else {{ 'Add New' }} @endif Sub-Category</h4>
                <p class="card-description"></p>



                @if(isset($category))
                <form class="forms-sample" method="post" action="{{route('admin_subcat_update')}}" enctype="multipart/form-data" id="form"> 
                    <input type="hidden" name="id" value="{{ $category->id }}">
                    @else
                    <form class="forms-sample" method="post" action="{{route('admin_subcat_add_post')}}" enctype="multipart/form-data" id="form">  
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="exampleInputName1">Name</label>
                            <input name="name" value="{{ isset($category->name)?$category->name  : Request::old('name') }}" type="text" class="form-control" id="exampleInputName1" placeholder="Name" max="150" required='required'/>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputName2">Arabic Name</label>
                            <input name="name_ar" value="{{ isset($category->name_ar)?$category->name_ar  :Request::old('name_ar') }}" type="text" class="form-control" id="exampleInputName2" placeholder="Arabic Name" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName3">Sort Order</label>
                            <input name="sort_order" value="{{ isset($category->sort_order)?$category->sort_order :Request::old('sort_order') }}" type="number" min="1" class="form-control" id="exampleInputName3" placeholder="Sort Order" required='required'/>
                        </div>

                        <div class="form-group">
                            <label>Category Image</label>
                            <input type="file" name="cat_image" class="file-upload-default" />
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image" />
                                <span class="input-group-btn">
                                    <button class="file-upload-browse btn btn-info" type="button">Browse</button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Parent Category</label>
                            <select  name="parent_id" id="parent_id" class="js-example-basic-single" style="width:100%" required='required'>
                                <option value="">Select One category</option>
                                    @foreach($maincategories as $key=>$val)
                                    <option @if(isset($category->parent_id) && $category->parent_id==$val->id){{'selected=selected'}}@endif value="{{$val->id}}">{{$val->name}}</option>
                                    @endforeach
                            </select>
                        </div> 
                        <div class="hr-line-dashed"></div> 
                        <button type="submit" oncluick="for_submit();" class="btn btn-success mr-2">Submit</button>
                        <button  type="button"  onClick="this.form.reset()" class="btn btn-light">Cancel</button>
                        <div style="display:none" id="feature_div"> 
                            @if(isset($features) && count($features)>0)
                            <?php $count = 1;
                            $previous_val = '';
                            ?>
                            @foreach($features as $val) 
                            <?php
                            if ($count == 1)
                                $previous_val = $val;
                            else
                                $previous_val .= "," . $val;
                            $count++;
                            ?>
                            <input type="hidden" name="feature[]" value="{{ $val }}">
                            @endforeach 
                            @endif 
                        </div>
                    </form>
                    <hr />
                    <div class="row">
                        <div class="col-lg-12 grid-margin d-flex align-items-stretch">
                            <div class="row flex-grow">
                                <!--tag strats-->
                                <div class="col-12 stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Add Features</h4>
                                            <p class="card-description">Type to add a new feature.</p>
                                            <div id="tags_tagsinput" class="tagsinput" style="width: 100%; min-height: 75%; height: 75%;">
                                                 <input id="tag_input" type="text" value="{{ (isset($previous_val)?$previous_val :'')  }}" data-role="tagsinput" />    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--tag ends-->
                            </div>
                        </div>
                    </div> 
            </div>

        </div>
    </div>
 
<script src="{{ asset('admin/dist/bootstrap-tagsinput.min.js') }}"></script> 
<script src="{{ asset('admin/node_modules/typeahead.js/dist/typeahead.bundle.min.js') }}"></script> 
<script src="{{ asset('admin/node_modules/select2/dist/js/select2.min.js') }}"></script> 
<script src="{{ asset('admin/js/typeahead.js') }}"></script> 
<script src="{{ asset('admin/js/select2.js') }}"></script> 
  
<style>
    div.tagsinput span.tag {
        background: #03a9f3;
        border: 0;
        color: #ffffff;
        padding: 6px 14px;
        font-size: .8125rem;
        font-family: inherit;
        line-height: 1;
    }
    div.tagsinput span.tag a {
        color: #ffffff;
    }
    div.tagsinput{
        width: 100%;
        min-height: 20% !important;
        height: auto !important;
    } 
    .bootstrap-tagsinput { 
        border: 0px !important;
        box-shadow: unset !important;
    }
</style>
<script src="{{ asset('admin/js/file-upload.js') }}"></script> 
<script>
                                    $(document).ready(function () {

                                $('#tag_input').on('itemRemoved', function (event) {
                                    $('#feature_div').html('');
                                    var new_val = this.value;
                                    new_val = new_val.split(',');
                                    jQuery.each(new_val, function (index, item) {
                                        $('#feature_div').append('<input type="hidden" name="feature[]" value="' + item + '">')
                                    });
                                });
                                $('#tag_input').on('itemAdded', function (event) {
                                    $('#feature_div').html('');
                                    var new_val = this.value;
                                    new_val = new_val.split(',');
                                    jQuery.each(new_val, function (index, item) {
                                        $('#feature_div').append('<input type="hidden" name="feature[]" value="' + item + '">')
                                    });
                                });

                            });
</script>   
@endsection
