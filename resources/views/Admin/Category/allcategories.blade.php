@extends('layouts.Admin.dashboard')
@section('title', 'All Categories')
@section('content')
<!-- Data Tables -->  
<div class="row">
          <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                  <h4 class="card-title">All Categories</h4>          
                        </div> 
                        <div class="col-6">
                             <p class="page-description"><a style="float: right;" href="{{ route('admin_category_add_get')}}" class="btn btn-primary">Add New Category</a></p> 
                        </div>
                    </div>
                  
                  
                  <div class="row">
                    <div class="table-sorter-wrapper col-lg-12 table-responsive">
                      <table id="sortable-table-2" class="table table-striped">
                        <thead>
                          <tr> 
                               <th>ID</th>
                                <th>Name</th>
                                <th>Arabic Name</th> 
                                <th>Sort Order</th>
                                <th>Created At</th>
                                <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(count($categories)>0) 
                        @foreach($categories as $category) 
                            <tr> 
                                <td class="center">{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->name_ar }}</td>
                             <!--   <td>{ { $category->parent_id }}</td>-->
                                
                                <td class="center">{{ $category->sort_order }}</td>
                                <td class="center">{{ date('d-M-Y', strtotime($category->created_at)) }}</td>
                                <td><a title="Edit" href="{{ route('admin_category_edit', $category->id  ) }}" target="blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a> | <a title="Delete" onclick="return confirm('Are you sure you want to delete this category?')" href="{{ route('admin_category_delete', $category->id ) }}" ><i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a> | @if($category->status=='active')<a href="{{ route('admin_category_block', [$category->id , '1'] ) }}" title="block" onclick="return confirm('Are you sure you want to block this category?')"><i class="fa fa-ban" aria-hidden="true"></i></a>@else<a href="{{ route('admin_category_block', [$category->id , '0'] ) }}" title="un-block" onclick="return confirm('Are you sure you want to un-block this category?')"><i style="color:red" class="fa fa-ban" aria-hidden="true"></i></a>@endif
                                    
                                
                                </td> 
                            </tr>
                           @endforeach
                           @endif  
                        </tbody>
                      </table>
                    </div>                     
                  </div>
                </div>
                  <div class="page_custom">{{ $categories->links() }}</div>
              </div>
            </div> 
       
</div>
<!-- Data Tables -->
<style>
    .page_custom ul{
        float: right !important;
        text-align: right;
        margin-right: 3%;
        margin-top: -15px;
    } 
</style>
  <script src="{{ asset('admin/js/jq.tablesort.js') }}"></script>
  <script src="{{ asset('admin/js/tablesorter.js') }}"></script>
<!-- Page-Level Scripts --> 
@endsection