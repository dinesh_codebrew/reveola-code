<html>
    <head></head>
    <body>
        <p>Dear {{ $user->name }},</p>
        <p>Please find your credentials given below :</p>
        <table>
            <tr>
                <td>Username </td>
                <td>{{ $user->email}}</td>
            </tr>
            <tr>
                <td>Password </td>
                <td>{{ $user->new_password }}</td>
            </tr>
        </table>
        <p>You can access business panel here  <a href="{{ env("APP_URL") }}/business/login">Login</a>.</p>

        <p>All the best,</p>
        <p>Reveola</p>
    </body>
</html>