 <nav class="sidebar sidebar-offcanvas" id="sidebar">
     <?php 
        $currentControoler  = class_basename(Route::current()->controller); 
        $currentPath = Route::currentRouteName(); //Route::getCurrentRoute()->getActionName();
     ?>
          <ul class="nav">
            <li class="nav-item nav-profile">
              <div class="nav-link">
                <div class="profile-image">
                    @if(isset($admin->profile_pic)&& !empty($admin->profile_pic))
                  <img src="{{ env('FILE_URL').$admin->profile_pic }}" alt="image" />
                  @else
                  <img src="{{ asset('admin/images/faces/face10.jpg') }}" alt="image" />
                  @endif
                 <!-- <span class="online-status online"></span>-->
                  <!--change class online to offline or busy as needed-->
                </div>
                <div class="profile-name">
                  <p class="name">
                   {{ $admin->name }}
                  </p>
                  <p class="designation"> 
                    {{ $admin->job }} 
                  </p>
                </div>
              </div>
            </li>  
            @if(isset($admin->user_type) && $admin->user_type=='business')
           
            <li class="nav-item  @if($currentPath=='business_business_all'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('business_business_all') }}">
                <i class="icon-briefcase menu-icon"></i>
                <span class="menu-title">Manage Businesses</span> 
              </a>
            </li>
            <li class="nav-item  @if($currentPath=='business_product_all' && $currentControoler=='AdminProductController'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('business_product_all') }}">
                <i class="icon-globe menu-icon"></i>
                <span class="menu-title">Manage Products</span> 
              </a>
		</li>
             
            <li class="nav-item  @if($currentPath=='business_service_all'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('business_service_all') }}">
                <i class="fa fa-gavel menu-icon"></i>
                <span class="menu-title">Manage Services</span> 
              </a>
            </li>
            <li class="nav-item @if(in_array($currentPath,['bprofile_update_get','bpassword_update_get','business_subscriptions'])){{ 'active' }} @endif">
              <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
                <i class="fa fa-cogs menu-icon"></i>
                <span class="menu-title">Profile Setting</span> 
              </a>
              <div class="collapse @if(in_array($currentPath,['bprofile_update_get','bpassword_update_get','business_subscriptions'])){{ 'show' }} @endif" id="general-pages">
                <ul class="nav flex-column sub-menu"> 
                  <li  class="nav-item @if($currentPath=='bprofile_update_get'){{ 'active' }} @endif"> <a class="nav-link" href="{{ route('bprofile_update_get') }}"> Update Profile</a></li>
                  <li  class="nav-item @if($currentPath=='bpassword_update_get'){{ 'active' }} @endif"> <a class="nav-link" href="{{ route('bpassword_update_get') }}"> Update Password </a></li> 
                  <li  class="nav-item @if($currentPath=='business_subscriptions'){{ 'active' }} @endif"> <a class="nav-link" href="{{ route('business_subscriptions') }}"> Upgrade Plan </a></li> 
                </ul>
              </div>
            </li> 
            <li class="nav-item">
              <a class="nav-link" href="{{ route('business_logout')}}" onclick="return confirm('Are you sure you want to sign-out?')">
                <i class="fa fa-sign-out menu-icon"></i>
                <span class="menu-title">Sign Out</span>
              </a>
            </li>
            @else
            <li class="nav-item  @if($currentPath=='admin_dashboard'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('admin_dashboard') }}">
                <i class="icon-rocket menu-icon"></i>
                <span class="menu-title">Dashboard</span> 
              </a>
            </li>
            <li class="nav-item @if($currentControoler=='AdminCategoryController'){{ 'active' }} @endif" >
              <a class="nav-link" data-toggle="collapse" href="#sidebar-layouts" aria-expanded="false" aria-controls="sidebar-layouts">
                <i class="icon-layers menu-icon"></i>
                <span class="menu-title">Manage Categories</span> 
              </a>
              <div class="collapse @if($currentControoler=='AdminCategoryController'){{ 'show' }} @endif" id="sidebar-layouts">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item @if($currentPath=='admin_category_all'){{ 'active' }} @endif"> <a class="nav-link" href="{{ route('admin_category_all') }}">All Categories</a></li>
                  <li class="nav-item @if($currentPath=='admin_subcategory_all'){{ 'active' }} @endif"> <a class="nav-link" href="{{ route('admin_subcategory_all')}}">All Sub-Categories</a></li> 
                </ul>
              </div>
            </li> 
            <li class="nav-item  @if($currentPath=='admin_business_all'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('admin_business_all') }}">
                <i class="icon-briefcase menu-icon"></i>
                <span class="menu-title">Manage Businesses</span> 
              </a>
            </li>
            <li class="nav-item  @if($currentPath=='admin_product_all' && $currentControoler=='AdminProductController'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('admin_product_all') }}">
                <i class="icon-globe menu-icon"></i>
                <span class="menu-title">Manage Products</span> 
              </a>
		</li>
             
            <li class="nav-item  @if($currentPath=='admin_service_all'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('admin_service_all') }}">
                <i class="fa fa-gavel menu-icon"></i>
                <span class="menu-title">Manage Services</span> 
              </a>
            </li>
            <li class="nav-item  @if($currentPath=='admin_user_all'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('admin_user_all') }}">
                <i class="icon-user menu-icon"></i>
                <span class="menu-title">Manage Users</span> 
              </a>
            </li>

            <li class="nav-item  @if($currentPath=='admin_b_user_all'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('admin_b_user_all') }}">
                <i class="fa fa-user-circle-o menu-icon"></i>
                <span class="menu-title">Manage Business Users</span> 
              </a>
            </li>      
            <li class="nav-item  @if($currentPath=='admin_subscription_all'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('admin_subscription_all') }}">
                <i class="fa fa-group menu-icon"></i>
                <span class="menu-title">Manage Sub. Plans</span> 
              </a>
            </li>  
            <li class="nav-item  @if($currentPath=='admin_sales_all'){{ 'active' }} @endif">
              <a class="nav-link" href="{{ route('admin_sales_all') }}">
                <i class="fa fa-truck menu-icon"></i>
                <span class="menu-title">Manage Subscriptions</span> 
              </a>
            </li>  
            <li class="nav-item @if($currentControoler == 'AdminController' && $currentPath !='admin_dashboard'){{ 'active' }} @endif">
              <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
                <i class="fa fa-cogs menu-icon"></i>
                <span class="menu-title">Admin Setting</span> 
              </a>
              <div class="collapse @if($currentControoler=='AdminController' && $currentPath !='admin_dashboard'){{ 'show' }} @endif" id="general-pages">
                <ul class="nav flex-column sub-menu"> 
                  <li  class="nav-item @if($currentPath=='aprofile_update_get'){{ 'active' }} @endif"> <a class="nav-link" href="{{ route('aprofile_update_get') }}"> Update Profile</a></li>
                  <li  class="nav-item @if($currentPath=='apassword_update_get'){{ 'active' }} @endif"> <a class="nav-link" href="{{ route('apassword_update_get') }}"> Update Password </a></li> 
                </ul>
              </div>
            </li>          
            <li class="nav-item">
              <a class="nav-link" href="{{ route('admin_logout')}}" onclick="return confirm('Are you sure you want to sign-out?')">
                <i class="fa fa-sign-out menu-icon"></i>
                <span class="menu-title">Sign Out</span>
              </a>
            </li>
             @endif
          </ul>
        </nav>
       
