<html>
    <head>  
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Reveola Admin - @yield('title')</title> 

		<!-- plugins:css -->
	  <link rel="stylesheet" href="{{ asset('admin/node_modules/mdi/css/materialdesignicons.min.css') }}" />
	  <link rel="stylesheet" href="{{ asset('admin/node_modules/simple-line-icons/css/simple-line-icons.css') }}" />
	  <link rel="stylesheet" href="{{ asset('admin/node_modules/flag-icon-css/css/flag-icon.min.css') }}" />
	  <link rel="stylesheet" href="{{ asset('admin/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css') }}" />
	  <!-- endinject -->
	  <!-- plugin css for this page -->
	  <link rel="stylesheet" href="{{ asset('admin/node_modules/font-awesome/css/font-awesome.min.css') }}" />
	  <link rel="stylesheet" href="{{ asset('admin/node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css') }}" />
	  <!-- End plugin css for this page -->
	  <!-- inject:css -->
	  <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}" />
	  <!-- endinject -->
	  <link rel="shortcut icon" href="{{ asset('admin/images/favicon.png') }}" />
	    <script src="{{ asset('admin/node_modules/jquery/dist/jquery.min.js') }}"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body >
       <div class="container-scroller">
            @include('layouts.Admin.sidebar')
		 <div class="container-fluid page-body-wrapper">
			<div class="row row-offcanvas row-offcanvas-right"> 
				@include('layouts.Admin.rightsidebar')
				  <div class="content-wrapper">
					     @foreach($errors->all() as $error)
                        <div class="alert alert-dismissable alert-danger">
                            {!! $error !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        </div>
                        @endforeach
						@if (session('status'))
							<div class="alert alert-success">
								{{ session('status') }}
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							</div>
						@endif 
						@yield('content') 
                </div>
			   @include('layouts.Admin.footer')
			 <!-- partial -->
			</div>
			<!-- row-offcanvas ends -->
		</div> 
			<!-- page-body-wrapper ends -->
	</div>
		<!-- container-scroller -->  
         
	   <!-- plugins:js -->
	
	  <script src="{{ asset('admin/node_modules/popper.js/dist/umd/popper.min.js') }}"></script> 
	  <script src="{{ asset('admin/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	  <script src="{{ asset('admin/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>
	  <!-- endinject -->
	  <!-- Plugin js for this page--> 
	  <!-- End plugin js for this page-->
	  <!-- inject:js -->
	  <script src="{{ asset('admin/js/off-canvas.js') }}"></script>
	  <script src="{{ asset('admin/js/hoverable-collapse.js') }}"></script>
	  <script src="{{ asset('admin/js/misc.js') }}"></script>
	  
		<script src="{{ asset('admin/js/settings.js') }}"></script> 
	  <script src="{{ asset('admin/js/todolist.js') }}"></script>
	  <!-- endinject -->
	  <!-- Custom js for this page-->
	 <script src="{{ asset('admin/js/dashboard.js') }}"></script>
	  <!-- End custom js for this page-->

    </body>
</html>

